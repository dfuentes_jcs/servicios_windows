﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Quartz;
using Quartz.Impl;
using System.Configuration;

using JCSSodimacWService.ServicioWindow;

namespace JCSSodimacWService
{
    public partial class WService : ServiceBase
    {
        private IScheduler sched;

        public WService()
        {
            InitializeComponent();
        }

        public void StartFromDebugger(string[] args)
        {
            OnStart(args);
        }

        protected override void OnStart(string[] args)
        {
            StdSchedulerFactory sf = new StdSchedulerFactory();
            sched = sf.GetScheduler();

            #region CORREO EXCEL
            string tiempoEjecucionProcesoCorreoExcel = ConfigurationManager.AppSettings["tiempoEjecucionProcesoCorreoExcel"].ToString();

            IJobDetail job1         = JobBuilder.Create<ProcesoCorreoExcel>()
                                    .WithIdentity("job1", "group1")
                                    .Build();

            ICronTrigger trigger    = (ICronTrigger)TriggerBuilder.Create()
                                    .WithIdentity("trigger1", "group1")
                                    .WithCronSchedule(tiempoEjecucionProcesoCorreoExcel)
                                    .Build();

            sched.ScheduleJob(job1, trigger);
            #endregion

            #region ENVIO REALIZADO
            string tiempoEjecucionProcesoEnvioRealizado = ConfigurationManager.AppSettings["tiempoEjecucionProcesoEnvioRealizado"].ToString();

            IJobDetail job2         = JobBuilder.Create<ProcesoEnvioRealizado>()
                                    .WithIdentity("job2", "group2")
                                    .Build();

            ICronTrigger trigger2    = (ICronTrigger)TriggerBuilder.Create()
                                    .WithIdentity("trigger2", "group2")
                                    .WithCronSchedule(tiempoEjecucionProcesoEnvioRealizado)
                                    .Build();

            sched.ScheduleJob(job2, trigger2);
            #endregion

            sched.Start();
        }

        protected override void OnStop()
        {
            sched.Shutdown(true);
        }
    }
}
