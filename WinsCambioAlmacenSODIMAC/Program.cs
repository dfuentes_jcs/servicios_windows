﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace JCSSodimacWService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            //ServiceBase[] ServicesToRun;
            //ServicesToRun = new ServiceBase[] 
            //{ 
            //    new WService() 
            //};
            //ServiceBase.Run(ServicesToRun);

            if (System.Diagnostics.Debugger.IsAttached)
            {
                // We are running with a debugger attached, so start
                // the service directly.
                WService service = new WService();
                string[] args = new string[] { "arg1", "arg2" };
                service.StartFromDebugger(args);
            }
            else
            {
                // We are running as a service so start normally.
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new WService()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}





