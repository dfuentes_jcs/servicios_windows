﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.Utiles
{
    public class Enumerables
    {
        public enum EstadoCambioAlmacen : int
        {
            RECIBIDO = 1,
            INFORMADO,
            PENDIENTE,
            REALIZADO,
            ENVIADO
        }
    }
}
