﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Web;
using System.Web.Security;
using System.Net;
using System.Data;
using System.Net.Sockets;
using System.Xml;
using System.Xml.Xsl;
using System.Globalization;
using System.IO;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json;
using System.Dynamic;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.ComponentModel;
using JCSSodimacWService.BLL;
using ClosedXML.Excel;
using NLog;

namespace JCSSodimacWService.Utiles
{
    public static class Util
    {
        public static DateTime ObtenerFechaProceso() 
        {
            DateTime hoy = DateTime.Now;
            DateTime fechaProceso = new DateTime();
            fechaProceso = new DateTime(hoy.Year, hoy.Month, 1);
            return fechaProceso;
        }

        public static IList<dynamic> ToDynamicList(this JArray data)
        {
            var dynamicData = new List<dynamic>();
            var expConverter = new ExpandoObjectConverter();

            foreach (var dataItem in data)
            {
                dynamic obj = JsonConvert.DeserializeObject<ExpandoObject>(dataItem.ToString(), expConverter);
                dynamicData.Add(obj);
            }
            return dynamicData;
        }

        public static DataTable ToDataTables<T>(IList<T> data)
        {
            PropertyDescriptorCollection props = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prp = props[i];
                table.Columns.Add(prp.Name, prp.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }

        public static string MakeValidFileName(string name)
        {
            string invalidChars = System.Text.RegularExpressions.Regex.Escape(new string(System.IO.Path.GetInvalidFileNameChars()));
            string invalidRegStr = string.Format(@"([{0}]*\.+$)|([{0}]+)", invalidChars);

            return System.Text.RegularExpressions.Regex.Replace(name, invalidRegStr, "_");
        }

        public static string GetMD5HashFromFile(string path)
        {
            string userNameDomain       = System.Configuration.ConfigurationManager.AppSettings["userNameDomain"];
            string passwordDomain       = System.Configuration.ConfigurationManager.AppSettings["passwordDomain"];
            string domain               = System.Configuration.ConfigurationManager.AppSettings["domain"];

            Impersonalizacion impersonalizacion = new Impersonalizacion();
            if (impersonalizacion.ImpersonateValidUser(userNameDomain, domain, passwordDomain))
            {
                using (var md5 = MD5.Create())
                {
                    using (var stream = File.OpenRead(path))
                    {
                        return BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", string.Empty);
                    }
                }
                impersonalizacion.UndoImpersonation();
            }

            return string.Empty;
        }

        public static bool GeneraExcel(DataTable dtReporte, string path, string nombreHoja)
        {
            XLWorkbook wb = new XLWorkbook();
            wb.Worksheets.Add(dtReporte, nombreHoja);
            wb.SaveAs(path);

            FileInfo file = new FileInfo(path);
            if (file.Exists) return true;
            else return false;
        }

        public static string GuardarExcel(string pathTemporal, string nombreVariableDirectorio, string nombreArchivo)
        {
            string direccionFinal = string.Empty;

            string userNameDomain       = System.Configuration.ConfigurationManager.AppSettings["userNameDomain"];
            string passwordDomain       = System.Configuration.ConfigurationManager.AppSettings["passwordDomain"];
            string domain               = System.Configuration.ConfigurationManager.AppSettings["domain"];

            string directorioDia        = DateTime.Now.ToString("dd");
            string directorioAnio       = DateTime.Now.ToString("yyyy");
            string directorioMes        = DateTime.Now.ToString("MM");

            try
            {
                VariableConfiguracionBLL variableGlobalBLL = new VariableConfiguracionBLL();
                string directorioTrabajoServer = variableGlobalBLL.ObtenerVariablePorNombre(nombreVariableDirectorio);

                if (File.Exists(pathTemporal))
                {
                    Impersonalizacion impersonalizacion = new Impersonalizacion();
                    if (impersonalizacion.ImpersonateValidUser(userNameDomain, domain, passwordDomain))
                    {
                        direccionFinal = Path.Combine(directorioTrabajoServer, directorioAnio, directorioMes, directorioDia);

                        if (!Directory.Exists(direccionFinal))
                        {
                            Directory.CreateDirectory(direccionFinal);
                        }

                        direccionFinal = Path.Combine(direccionFinal, nombreArchivo);

                        if (File.Exists(direccionFinal))
                        {
                            FileInfo repetido = new FileInfo(direccionFinal);
                            direccionFinal = repetido.DirectoryName + Path.DirectorySeparatorChar + repetido.Name.Replace(repetido.Extension, "_" + DateTime.Now.ToString("HHmmss")) + repetido.Extension;
                        }

                        File.Copy(pathTemporal, direccionFinal);
                        impersonalizacion.UndoImpersonation();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger log = LogManager.GetLogger("Error");
                log.Error(ex.Message.ToString());
                MailTo.aviso_error(ex);
                log.Error("Se ha creado un registro de log en LOG_API_CORREO");
            }

            return direccionFinal;
        }

        public static void BorrarArchivo(string path)
        {
            string userNameDomain       = System.Configuration.ConfigurationManager.AppSettings["userNameDomain"];
            string passwordDomain       = System.Configuration.ConfigurationManager.AppSettings["passwordDomain"];
            string domain               = System.Configuration.ConfigurationManager.AppSettings["domain"];

            Impersonalizacion impersonalizacion = new Impersonalizacion();
            if (impersonalizacion.ImpersonateValidUser(userNameDomain, domain, passwordDomain))
            {
                File.Delete(path);
                impersonalizacion.UndoImpersonation();
            }
        }

        public static bool ValidarCodigoContenedor(string id)
        {
            char[] codigo   = id.ToCharArray();

            if (codigo.Length < 11) return false;
            else
            {
                char digito     = codigo[10];
                int[] digitos   = new int[10];
                int total       = 0;
                int entero      = 0;
                int verificador = 0;
    
                // VALIDA Y TRANSFORMA A NUMERICO LA COMPAÑIA BIC
                if (!Char.IsDigit(codigo[0])) {
                    digitos[0] = ConversionCodigo(codigo[0]);
                }
                else { return false; }

                if (!Char.IsDigit(codigo[1])) {
                    digitos[1] = ConversionCodigo(codigo[1]);
                }
                else { return false; }

                if (!Char.IsDigit(codigo[2])) {
                    digitos[2] = ConversionCodigo(codigo[2]);
                }
                else { return false; }

                // VALIDA Y TRANSFORMA A NUMERICO IDENTIFICADOR DE CONTENEDOR
                if (!Char.IsDigit(codigo[3])) {
                    if (codigo[3].ToString().ToUpper() == "U" || codigo[3].ToString().ToUpper() == "J" || codigo[3].ToString().ToUpper() == "Z") {
                        digitos[3] = ConversionCodigo(codigo[3]);
                    }
                    else { return false; }
                }
                else { return false; }

                // VALIDA LOS DIGITOS DEL CONTENEDOR
                for (int i = 4; i < 10; i++) {
                    if (!Char.IsDigit(codigo[i])) { return false; }
                    else {
                        digitos[i] = int.Parse(codigo[i].ToString());
                    }
                }

                // OBTIENE EL TOTAL ACUMULADO DE TODOS LOS DIGITOS
                for (int i = 0; i < 10; i++) {
                    total += digitos[i] * Convert.ToInt32(Math.Pow(2, i));
                }

                // OBTIENE LA PARTE ENTERA DE LA DIVISION
                double d = total / 11;
                entero = Convert.ToInt32(Math.Floor(d));

                // CALCULA EL DIGITO VERIFICADOR DEL CODIGO INGRESADO
                verificador = total - (entero * 11);
    
                if (verificador == 10) { verificador = 0; }

                // COMPARA EL DIGITO CALCULADO CON EL INGRESADO POR EL USUARIO
                if (verificador == int.Parse(digito.ToString())) { return true; }
                else { return false; }
            }
        }

        public static int ConversionCodigo(char letra) 
        {
            char letra_auxiliar = letra.ToString().ToUpper().First();

            switch (letra_auxiliar) {
                case 'A':
                    return 10;
                    break;
                case 'B':
                    return 12;
                    break;
                case 'C':
                    return 13;
                    break;
                case 'D':
                    return 14;
                    break;
                case 'E':
                    return 15;
                    break;
                case 'F':
                    return 16;
                    break;
                case 'G':
                    return 17;
                    break;
                case 'H':
                    return 18;
                    break;
                case 'I':
                    return 19;
                    break;
                case 'J':
                    return 20;
                    break;
                case 'K':
                    return 21;
                    break;
                case 'L':
                    return 23;
                    break;
                case 'M':
                    return 24;
                    break;
                case 'N':
                    return 25;
                    break;
                case 'O':
                    return 26;
                    break;
                case 'P':
                    return 27;
                    break;
                case 'Q':
                    return 28;
                    break;
                case 'R':
                    return 29;
                    break;
                case 'S':
                    return 30;
                    break;
                case 'T':
                    return 31;
                    break;
                case 'U':
                    return 32;
                    break;
                case 'V':
                    return 34;
                    break;
                case 'W':
                    return 35;
                    break;
                case 'X':
                    return 36;
                    break;
                case 'Y':
                    return 37;
                    break;
                case 'Z':
                    return 38;
                    break;
                default:
                    return 0;
                    break;
            }
        }

        public static bool ValidaBL(string valor)
        {
            if (valor.Trim().Length < 9) return false;
            else return true;
        }

        public static int ValidarRut(string rut)
        {
            rut = rut.Replace("-", "");
            rut = rut.Replace(".", "");

            if (rut.Length > 10)
            {
                return 4;
            }

            if (rut.Substring(rut.Length - 1, 1).ToUpper() != "K")
            {
                if (!ValidaNumeroLong(rut))
                {
                    return 4;
                }
            }

            if (digitoVerificador(int.Parse(rut.Substring(0, rut.Length - 1))) != rut.Substring(rut.Length - 1, 1).ToUpper())
            {
                return 4;
            }

            return 0;
        }

        public static bool ValidaNumeroLong(string valor)
        {
            long valortry;
            if (!long.TryParse(valor, out valortry))
            {

                return false;
            }

            return true;
        }

        public static string digitoVerificador(int rut)
        {
            int Digito;
            int Contador;
            int Multiplo;
            int Acumulador;
            string RutDigito;

            Contador = 2;
            Acumulador = 0;

            while (rut != 0)
            {
                Multiplo = (rut % 10) * Contador;
                Acumulador = Acumulador + Multiplo;
                rut = rut / 10;
                Contador = Contador + 1;
                if (Contador == 8)
                {
                    Contador = 2;
                }
            }

            Digito = 11 - (Acumulador % 11);
            RutDigito = Digito.ToString().Trim();
            if (Digito == 10)
            {
                RutDigito = "K";
            }
            if (Digito == 11)
            {
                RutDigito = "0";
            }
            return (RutDigito);
        }

        public static bool ValidaAgente(string nombreAgente)
        {
            List<CAMBIO_ALMACEN_AGENTE> listaAgentes = new AgenteBLL().ObtenerAgentes();
            if (listaAgentes.Any(a => a.nombre.Trim().ToUpper() == nombreAgente.Trim().ToUpper())) return true;
            else return false;
        }

        public static bool ValidaFecha(string fecha)
        {
            try
            {
                Convert.ToDateTime(fecha);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
