﻿using JCSSodimacWService.BLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.Utiles
{
    public class MailTo
    {
        public static Boolean ReporteResumenSodimac(string pathReporte)
        {
            string asunto = "Reporte Cambio de Almacén SODIMAC";
            string mensaje = string.Format(@"Buenos días<br><br>
                                    Se adjunta reporte resumen con los cambios de almacén por realizar
                                    <br>
                                    <br>
                                    Les saluda atte.<br>
                                    Departamento de Informática<br>
                                    Agencia de Aduana Juan Carlos Stephens");

            try
            {
                VariableConfiguracionBLL variableGlobalBLL = new VariableConfiguracionBLL();
                string respuesta = new Email.MailClient().EnviarEmail(asunto,
                                    mensaje,
                                    variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.resumen.desde"),
                                    variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.resumen.para"),
                                    variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.resumen.cc"),
                                    variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.error.cco"),
                                    pathReporte + ";",
                                    true,
                                    "NORMAL",
                                    true,
                                    "WINS SODIMAC: REPORTE CAMBIO DE ALMACÉN SODIMAC POR REALIZAR");

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static Boolean ArchivoFormatoIncorrecto(string path)
        {
            string asunto = "Registros sin procesar de planilla Cambio Almacén SODIMAC";
            string mensaje = string.Format(@"Buenos días<br><br>
                                    Los registros de la planilla adjunta no se lograron procesar, el detalle del error se encuentran en la última columna.
                                    <br>
                                    <br>
                                    Les saluda atte.<br>
                                    Departamento de Informática<br>
                                    Agencia de Aduana Juan Carlos Stephens");

            try
            {
                VariableConfiguracionBLL variableGlobalBLL = new VariableConfiguracionBLL();
                string respuesta = new Email.MailClient().EnviarEmail(asunto,
                                    mensaje,
                                    variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.formato.incorrecto.desde"),
                                    variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.formato.incorrecto.para"),
                                    variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.formato.incorrecto.cc"),
                                    variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.error.cco"),
                                    path + ";",
                                    true,
                                    "NORMAL",
                                    true,
                                    "WINS SODIMAC: REPORTE CAMBIO DE ALMACÉN SODIMAC FORMATO INCORRECTO");

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static Boolean ReporteCambiosAlmacenRealizados(string pathReporte)
        {
            string asunto = "Reporte Cambios de Almacén REALIZADOS";
            string mensaje = string.Format(@"Buenos días<br><br>
                                    Se adjunta reporte con los cambios de almacén ya realizados
                                    <br>
                                    <br>
                                    Les saluda atte.<br>
                                    Departamento de Informática<br>
                                    Agencia de Aduana Juan Carlos Stephens");

            try
            {
                VariableConfiguracionBLL variableGlobalBLL = new VariableConfiguracionBLL();
                string respuesta = new Email.MailClient().EnviarEmail(asunto,
                                    mensaje,
                                    variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.realizado.desde"),
                                    variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.realizado.para"),
                                    variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.realizado.cc"),
                                    variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.error.cco"),
                                    pathReporte + ";",
                                    true,
                                    "NORMAL",
                                    true,
                                    "WINS SODIMAC: REPORTE CAMBIO DE ALMACÉN SODIMAC REALIZADOS");

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static Boolean aviso_error(Exception ex)
        {
            string mensaje_corto = ex.Message;
            string mensaje_largo = ex.ToString();
            string mensaje_causa = Convert.ToString(ex.InnerException);
            string source = ex.Source;
            string targetsite = Convert.ToString(ex.TargetSite);
            string traza = ex.StackTrace;

            StringBuilder estilo = new StringBuilder();

            estilo.Append("<style type=\"text/css\">.grislinea {background-color: #C1C1C1;}");
            estilo.Append(".titulotablaleft {font-family: Arial, Helvetica, sans-serif;font-size: 11px;font-weight: bold;color: #4975A0;text-decoration: none;background-color: #D9E3ED;text-align: left;vertical-align: middle;height: 20px;padding-left: 10px;}");
            estilo.Append(".celdamantenedor {font-family: Arial, Helvetica, sans-serif;font-size: 11px;font-weight: normal;color: #000000;text-decoration: none;background-color: #EFEFEF;height: 25px;text-align: left;vertical-align: middle;padding-left: 10px;}");
            estilo.Append(".celda1mantenedorleft {font-family: Arial, Helvetica, sans-serif;font-size: 11px;font-weight: normal;color: #000000;	text-decoration: none;background-color: #FFFFFF;height: 25px;text-align: left;vertical-align: middle;padding-left: 10px;}");
            estilo.Append("table {border-collapse:collapse};td {border:1px solid #C1C1C1};");
            estilo.Append("</style>");

            StringBuilder mensaje = new StringBuilder();
            mensaje.Append("<head>" + estilo + "</head>");
            mensaje.Append("<h4>Sr Administrador:</h4></br>");

            mensaje.Append("<p>Se ha generado un log de error con fecha " + DateTime.Now.ToString("dd/MM/yyyy HH:mm") + ".</p>");
            mensaje.Append("<p> A continuación se presenta un detalle del log de error: </p>");
            mensaje.Append("<table width=\"80%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">");
            mensaje.Append("<tr><td class=\"grislinea\"><table width=\"100%\" border=\"1\" cellspacing=\"1\" cellpadding=\"0\" bordercolor=\"#C1C1C1\">");
            mensaje.Append("<tr><td colspan=\"2\" class=\"titulotablaleft\">Encabezado</td></tr>");

            mensaje.Append("<tr><td class=\"celdamantenedor\" width=\"10%\">Causa:</td>");
            mensaje.Append("<td class=\"celda1mantenedorleft\" width=\"20%\">" + mensaje_causa + "</td></tr>");
            mensaje.Append("<tr><td class=\"celdamantenedor\" width=\"10%\">Mensaje:</td>");
            mensaje.Append("<td class=\"celda1mantenedorleft\" width=\"20%\">" + mensaje_corto + "</td></tr>");
            mensaje.Append("<tr><td class=\"celdamantenedor\" width=\"10%\">Mensaje Completo:</td>");
            mensaje.Append("<td class=\"celda1mantenedorleft\" width=\"20%\">" + mensaje_largo + "</td></tr>");

            if (traza != string.Empty)
            {
                mensaje.Append("<tr><td class=\"celdamantenedor\" >Traza:</td>");
                mensaje.Append("<td class=\"celda1mantenedorleft\" >" + traza.Replace(",", "</BR>") + "</td></tr>");
            }

            mensaje.Append("</table></td></tr></table></br>");
            mensaje.Append("<p>Atte.</p></br>");
            mensaje.Append("Departamento de Informática<br>");
            mensaje.Append("Agencia de Aduana Juan Carlos Stephens");

            string asunto = "Error WINS SODIMAC: Reporte de cambio de almacén " + DateTime.Now.ToString("dd/MM/yyyy");

            try
            {
                VariableConfiguracionBLL variableGlobalBLL = new VariableConfiguracionBLL();
                string respuesta = new Email.MailClient().EnviarEmailSinCC(asunto,
                                    mensaje.ToString(),
                                    variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.error.desde"),
                                    variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.error.para"),
                                    variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.error.cco"),
                                    string.Empty,
                                    true,
                                    "NORMAL",
                                    true,
                                    "WINS SODIMAC: REPORTE CAMBIO DE ALMACÉN SODIMAC");
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
