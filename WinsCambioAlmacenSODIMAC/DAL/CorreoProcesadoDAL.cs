﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.DAL
{
    public class CorreoProcesadoDAL
    {
        public List<string> ListaCorreoProcesado(DateTime fechaProceso)
        {
            DateTime fechaDesde = fechaProceso.AddMonths(-1);
            DateTime fechaHasta = fechaProceso.AddMonths(1);
            using (JCSDBEntities db = new JCSDBEntities())
            {
                var query = from t in db.CAMBIO_ALMACEN_CORREO_PROCESADO
                            where t.esta_activo == true
                             && DbFunctions.TruncateTime(t.fecha_proceso) >= DbFunctions.TruncateTime(fechaDesde)
                             && DbFunctions.TruncateTime(t.fecha_proceso) <= DbFunctions.TruncateTime(fechaHasta)
                            select t;
                return query.Select(s=>s.correo_id).ToList();
            }
        }

        public void CrearCorreoProcesado(CAMBIO_ALMACEN_CORREO_PROCESADO correoProcesado)
        {
            using (JCSDBEntities db = new JCSDBEntities())
            {
                db.CAMBIO_ALMACEN_CORREO_PROCESADO.Add(correoProcesado);
                db.SaveChanges();
            }
        }
    }
}
