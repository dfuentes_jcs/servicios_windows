﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.DAL
{
    public class PlanillaClienteDetalleDAL
    {
        public bool PlanillaClienteDetalleListaCrear(List<CAMBIO_ALMACEN_PLANILLA_CLIENTE> listaDetalle)
        {
            using (var db = new JCSDBEntities())
            {
                db.CAMBIO_ALMACEN_PLANILLA_CLIENTE.AddRange(listaDetalle);
                db.SaveChanges();

                return true;
            }
        }

        public List<CAMBIO_ALMACEN_PLANILLA_CLIENTE> ObtenerListaClienteDetalleProcesado()
        {
            using (var db = new JCSDBEntities())
            {
                var query = from c in db.CAMBIO_ALMACEN_PLANILLA_CLIENTE
                            where c.esta_activo && c.CAMBIO_ALMACEN_PLANILLA_CLIENTE_CABECERA.esta_activo
                            select c;

                return query.ToList();
            }
        }
    }
}
