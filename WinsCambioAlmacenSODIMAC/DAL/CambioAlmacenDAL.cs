﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.Drawing.Charts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace JCSSodimacWService.DAL
{
    public class CambioAlmacenDAL
    {
        public bool CambioAlmacenListaCrear(List<CAMBIO_ALMACEN> listaCambioAlmacen)
        {
            using (var db = new JCSDBEntities())
            {
                db.CAMBIO_ALMACEN.AddRange(listaCambioAlmacen);
                db.SaveChanges();

                return true;
            }
        }

        public bool CambioAlmacenCrear(CAMBIO_ALMACEN cambioAlmacen)
        {
            using (var db = new JCSDBEntities())
            {
                db.CAMBIO_ALMACEN.Add(cambioAlmacen);
                db.SaveChanges();

                return true;
            }
        }

        public bool EditarEstadoCambioAlmacen(int cambioAlmacenId, int estadoId, bool esEnviado)
        {
            using (var db = new JCSDBEntities())
            {
                var query = from p in db.CAMBIO_ALMACEN
                            where p.cambio_almacen_id == cambioAlmacenId
                            select p;

                CAMBIO_ALMACEN cambioAlmacen = query.FirstOrDefault();

                if (cambioAlmacen != null)
                {
                    cambioAlmacen.cambio_almacen_estado_id = estadoId;
                    if (esEnviado)
                    {
                        cambioAlmacen.fecha_enviado = DateTime.Now;
                        cambioAlmacen.enviado = true;
                    }
                }

                db.SaveChanges();
                return true;
            }
        }

        public List<CAMBIO_ALMACEN_PLANILLA_CLIENTE> ObtenerListaRealizadosSinEnviar(int estadoRealizadoId)
        {
            using (var db = new JCSDBEntities())
            {
                var query = from c in db.CAMBIO_ALMACEN_PLANILLA_CLIENTE.Include(x => x.CAMBIO_ALMACEN)
                            where c.esta_activo && c.CAMBIO_ALMACEN.Any(ca => ca.cambio_almacen_estado_id == estadoRealizadoId 
                            && ca.enviado == false && ca.esta_activo)
                            select c;

                return query.ToList();
            }
        }

        public List<CAMBIO_ALMACEN> ObtenerListaCambioAlmacenSinInformar(int estadoRecibidoId)
        {
            using (var db = new JCSDBEntities())
            {
                var query = from c in db.CAMBIO_ALMACEN
                            where c.esta_activo && c.cambio_almacen_estado_id == estadoRecibidoId 
                            select c;

                return query.ToList();
            }
        }
    }
}
