﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.DAL
{
    public class PlanillaClienteCabeceraDAL
    {
        public bool PlanillaClienteCabeceraCrear(CAMBIO_ALMACEN_PLANILLA_CLIENTE_CABECERA cabecera)
        {
            using (var db = new JCSDBEntities())
            {
                db.CAMBIO_ALMACEN_PLANILLA_CLIENTE_CABECERA.Add(cabecera);
                db.SaveChanges();

                return true;
            }
        }

        public List<string> ObtenerListaArchivosCargados()
        {
            using (var db = new JCSDBEntities())
            {
                var query = from c in db.CAMBIO_ALMACEN_PLANILLA_CLIENTE_CABECERA
                            where c.esta_activo && c.ubicacion != string.Empty
                            select c.ubicacion;

                return query.ToList();
            }
        }
    }
}
