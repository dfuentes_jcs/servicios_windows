﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.DAL
{
    public class CompaniaDAL
    {
        public CAMBIO_ALMACEN_COMPANIA ObtenerCompaniaPorNombre(string nombre)
        {
            using (var db = new JCSDBEntities())
            {
                var query = from l in db.CAMBIO_ALMACEN_COMPANIA
                            where l.esta_activo == true &&
                            l.nombre.Trim().ToUpper() == nombre.Trim().ToUpper()
                            select l;

                return query.FirstOrDefault();
            }
        }

        public List<CAMBIO_ALMACEN_COMPANIA> ObtenerCompanias()
        {
            using (var db = new JCSDBEntities())
            {
                var query = from l in db.CAMBIO_ALMACEN_COMPANIA
                            where l.esta_activo == true
                            select l;

                return query.ToList();
            }
        }
    }
}
