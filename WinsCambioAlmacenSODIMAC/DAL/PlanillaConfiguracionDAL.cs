﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.DAL
{
    public class PlanillaConfiguracionDAL
    {
        public List<CAMBIO_ALMACEN_PLANILLA_CONFIGURACION> ObtenerListaPlanillaConfiguracion()
        {
            using (var db = new JCSDBEntities())
            {
                var query = from l in db.CAMBIO_ALMACEN_PLANILLA_CONFIGURACION
                            where l.esta_activo == true 
                            orderby l.columna
                            select l;

                return query.ToList();
            }
        }
    }
}
