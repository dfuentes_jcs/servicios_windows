﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.DAL
{
    public class AgenteDAL
    {
        public CAMBIO_ALMACEN_AGENTE ObtenerAgentePorNombre(string nombre)
        {
            using (var db = new JCSDBEntities())
            {
                var query = from l in db.CAMBIO_ALMACEN_AGENTE
                            where l.esta_activo == true &&
                            l.nombre.Trim().ToUpper() == nombre.Trim().ToUpper()
                            select l;

                return query.FirstOrDefault();
            }
        }

        public List<CAMBIO_ALMACEN_AGENTE> ObtenerAgentes()
        {
            using (var db = new JCSDBEntities())
            {
                var query = from l in db.CAMBIO_ALMACEN_AGENTE
                            where l.esta_activo == true
                            select l;

                return query.ToList();
            }
        }
    }
}
