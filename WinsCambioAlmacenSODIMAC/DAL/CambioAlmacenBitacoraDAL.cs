﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.DAL
{
    class CambioAlmacenBitacoraDAL
    {
        public bool PlanillaClienteBitacoraCrear(CAMBIO_ALMACEN_BITACORA bitacora)
        {
            using (var db = new JCSDBEntities())
            {
                db.CAMBIO_ALMACEN_BITACORA.Add(bitacora);
                db.SaveChanges();

                return true;
            }
        }
    }
}
