﻿using ClosedXML.Excel;
using JCSSodimacWService.BLL;
using JCSSodimacWService.BO;
using JCSSodimacWService.Utiles;
using NLog;
using Quartz;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.ServicioWindow
{
    class ProcesoEnvioRealizado : IJob
    {
        readonly Logger log                 = LogManager.GetCurrentClassLogger();
        readonly private Semaforo semaforo  = Semaforo.getInstance("ProcesoEnvioRealizado");
        readonly string repositorioTemporal = System.Configuration.ConfigurationManager.AppSettings["repositorioTemporal"];

        public void Execute(IJobExecutionContext context)
        {
            if (!semaforo.EstaProcesando)
            {
                semaforo.EstaProcesando = true;

                try
                {
                    log.Debug("---------COMIENZO SERVICIO ENVIO REALIZADO------------");
                    CambioAlmacenBLL cambioAlmacenBLL                               = new CambioAlmacenBLL();
                    List<CAMBIO_ALMACEN_PLANILLA_CLIENTE> listaRegistrosRealizados  = cambioAlmacenBLL.ObtenerListaRealizadosSinEnviar();

                    if (listaRegistrosRealizados.Count > 0)
                    {
                        log.Info("Generando Reporte Cambios Realizados");
                        string formato              = new VariableConfiguracionBLL().ObtenerVariablePorNombre("cambio.almacen.formato.reportes");
                        string nombreArchivo        = string.Format("{0}{1}.{2}", "CambioAlmacenRealizado_", DateTime.Now.ToString("yyyy-MM-dd"), formato);
                        string pathReporteTemporal  = Path.Combine(repositorioTemporal, nombreArchivo);
                        string nombreHoja           = "REALIZADO";

                        List<ReporteCambiosRealizadosBO> listaRegistrosBO = new List<ReporteCambiosRealizadosBO>();
                        
                        foreach(CAMBIO_ALMACEN_PLANILLA_CLIENTE item in listaRegistrosRealizados)
                        {
                            listaRegistrosBO.Add(new ReporteCambiosRealizadosBO
                            {
                                Extraportuario      = item.rut_extraportuario,
                                Contenedor          = item.contenedor,
                                BL                  = item.bl,
                                POD                 = item.pod,
                                TamTipoContenedor   = item.tamano_tipo_contenedor,
                                TipoCarga           = item.tipo_carga,
                                ETA                 = item.fecha_eta,
                                Nave                = item.nave,
                                Agente              = item.agente,
                                EstadoContenedor    = item.estado_contenedor,
                                Manifiesto          = item.manifiesto
                            });
                        }

                        DataTable dtReporte                                 = Util.ToDataTables(listaRegistrosBO);
                        dtReporte.Columns["TamTipoContenedor"].ColumnName   = "Tam./Tipo Contenedor";
                        dtReporte.Columns["TipoCarga"].ColumnName           = "Tipo Carga";
                        dtReporte.Columns["EstadoContenedor"].ColumnName    = "Estado Contenedor";
                        dtReporte.AcceptChanges();

                        if (Util.GeneraExcel(dtReporte, pathReporteTemporal, nombreHoja))
                        {
                            string pathReporteFinal = Util.GuardarExcel(pathReporteTemporal, "cambio.almacen.directorio.cliente.salida", nombreArchivo);

                            if (!string.IsNullOrEmpty(pathReporteFinal))
                            {
                                log.Info("Enviando Reporte Cambios Realizados");
                                if (MailTo.ReporteCambiosAlmacenRealizados(pathReporteFinal))
                                {
                                    log.Info("Reporte Enviado");
                                    foreach(CAMBIO_ALMACEN_PLANILLA_CLIENTE cambioAlmacenCliente in listaRegistrosRealizados)
                                    {
                                        CAMBIO_ALMACEN cambioAlmacen            = cambioAlmacenCliente.CAMBIO_ALMACEN.FirstOrDefault();
                                        cambioAlmacenBLL.EditarEstadoCambioAlmacen(cambioAlmacen.cambio_almacen_id, (int)Enumerables.EstadoCambioAlmacen.ENVIADO);
                                        cambioAlmacen.cambio_almacen_estado_id  = (int)Enumerables.EstadoCambioAlmacen.ENVIADO; 
                                        cambioAlmacenBLL.CambioAlmacenBitacoraCrear(cambioAlmacen); 
                                    }

                                    File.Delete(pathReporteTemporal);
                                }
                                else
                                {
                                    log.Error("Reporte no pudo ser enviado");
                                }
                            }
                            else
                            {
                                log.Error("Reporte no pudo copiado a la ruta final");
                            }
                        }
                        else
                        {
                            log.Error("Reporte no pudo ser generado");
                        }
                    }

                    log.Debug("---------FINALIZO SERVICIO ENVIO REALIZADO------------");
                }
                catch (Exception ex)
                {
                    Logger log = LogManager.GetLogger("Error");
                    log.Error(ex.Message.ToString());
                    MailTo.aviso_error(ex);
                    log.Error("Se ha creado un registro de log en LOG_API_CORREO");
                }
                finally
                {
                    if (semaforo.EstaProcesando)
                    {
                        Semaforo.setInstance("ProcesoEnvioRealizado");
                        if (semaforo.CantidadArchivosConError > 0)
                            Semaforo.setInstance("ProcesoEnvioRealizado");
                    }
                }
            }
        }
    }
}
