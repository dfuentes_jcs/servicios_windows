﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using JCSSodimacWService.BLL;
using JCSSodimacWService.BO;
using JCSSodimacWService.Utiles;
using MailKit;
using MailKit.Net.Imap;
using MailKit.Search;
using MimeKit;
using NLog;
using OfficeOpenXml;
using Quartz;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;

namespace JCSSodimacWService.ServicioWindow
{
    class ProcesoCorreoExcel : IJob
    {
        readonly Logger log = LogManager.GetCurrentClassLogger();
        readonly private Semaforo semaforo = Semaforo.getInstance("ProcesoCorreoExcel");
        readonly string repositorioTemporal = System.Configuration.ConfigurationManager.AppSettings["repositorioTemporal"];
        List<CAMBIO_ALMACEN_PLANILLA_CLIENTE> listaRegistrosProcesados = new List<CAMBIO_ALMACEN_PLANILLA_CLIENTE>();

        public void Execute(IJobExecutionContext context)
        {
            if (!semaforo.EstaProcesando)
            {
                semaforo.EstaProcesando = true;

                try
                {
                    log.Debug("---------COMIENZO SERVICIO CORREO EXCEL------------");

                    DateTime fechaProceso = Util.ObtenerFechaProceso();
                    List<string> seenUids = new CorreoProcesadoBLL().ListaCorreoProcesado(fechaProceso);
                    log.Info("Total de correos procesados a la fecha: " + seenUids.Count);
                    List<MensajeCorreo> listaCorreosNoProcesados = ObtenerCorreosImap(seenUids);
                    log.Info("Total de correos por procesar: " + listaCorreosNoProcesados.Count);

                    List<CAMBIO_ALMACEN> listaCambiosAlmacen = new List<CAMBIO_ALMACEN>();

                    if (listaCorreosNoProcesados.Count > 0)
                    {
                        listaRegistrosProcesados = new PlanillaClienteDetalleBLL().ObtenerListaClienteDetalleProcesado();
                    }

                    foreach (MensajeCorreo CorreosNoProcesados in listaCorreosNoProcesados)
                    {
                        MimeMessage mensaje     = CorreosNoProcesados.Mensaje;
                        string mensajeId        = CorreosNoProcesados.MensajeId;
                        bool archivoIntegrado   = false;

                        log.Info(string.Format("Inicio proceso correo ID: {0}; Asunto: {1}", mensajeId, mensaje.Subject));

                        IEnumerable<MimeEntity> listadoAdjuntos = mensaje.Attachments;
                        DirectoryInfo dir                       = new DirectoryInfo(repositorioTemporal);

                        if (listadoAdjuntos != null)
                        {
                            foreach (var adjunto in listadoAdjuntos)
                            {
                                //Validamos que exista un excel como adjunto en el correo.
                                if (adjunto.ContentType.MimeType.ToLower().Equals("application/vnd.ms-excel") ||
                                    adjunto.ContentType.MimeType.ToLower().Equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") ||
                                    ((MimePart)adjunto).FileName.Contains(".xls"))
                                {
                                    MimePart item               = (MimePart)adjunto;
                                    String[] splitNombreArchivo = item.FileName.Split('.');
                                    string nombreArchivoExcel   = Guid.NewGuid().ToString() + "." + splitNombreArchivo[splitNombreArchivo.Length - 1];
                                    string pathTemporal         = string.Empty;

                                    using (var stream = File.Create(Path.Combine(@dir.FullName, nombreArchivoExcel)))
                                    {
                                        item.ContentObject.DecodeTo(stream);
                                        stream.Close();
                                        pathTemporal = Path.Combine(@dir.FullName, nombreArchivoExcel);
                                    }

                                    string pathFinal    = Util.GuardarExcel(pathTemporal, "cambio.almacen.directorio.cliente.entrada", nombreArchivoExcel);
                                    int cantidadFilas   = 0;

                                    if (!string.IsNullOrEmpty(pathFinal))
                                    {
                                        log.Info("Archivo copiado a la ruta: " + pathFinal);
                                        log.Info("Leyendo archivo: " + pathTemporal);
                                        DataTable dtErrores = new DataTable();
                                        List<CAMBIO_ALMACEN_PLANILLA_CLIENTE> listaDetalle = LeerExcel(pathTemporal, out dtErrores, out cantidadFilas);

                                        if (listaDetalle.Count > 0)
                                        {
                                            if (!ExisteArchivo(pathFinal))
                                            {
                                                log.Info(string.Format("Se encontraron {0} registros nuevos", listaDetalle.Count));
                                                CAMBIO_ALMACEN_PLANILLA_CLIENTE_CABECERA cabecera   = new CAMBIO_ALMACEN_PLANILLA_CLIENTE_CABECERA();
                                                cabecera.esta_activo                                = true;
                                                cabecera.fecha_creacion                             = DateTime.Now;
                                                cabecera.ubicacion                                  = pathFinal;
                                                new PlanillaClienteCabeceraBLL().PlanillaClienteCabeceraCrear(cabecera);

                                                listaDetalle.ForEach(d => d.cambio_almacen_planilla_cliente_cabecera_id = cabecera.cambio_almacen_planilla_cliente_cabecera_id);
                                                new PlanillaClienteDetalleBLL().PlanillaClienteDetalleListaCrear(listaDetalle);
                                                listaRegistrosProcesados.AddRange(listaDetalle);

                                                listaCambiosAlmacen.AddRange(ObtenerListaCambioAlmacen(listaDetalle, CorreosNoProcesados.FechaRecibido));
                                                File.Delete(pathTemporal);
                                                archivoIntegrado = true;
                                            }
                                            else
                                            {
                                                string resp = "El archivo ya ha sido procesado anteriormente: " + pathTemporal;
                                                log.Error(resp);
                                                Util.BorrarArchivo(pathFinal);
                                            }
                                        }
                                        else if (dtErrores.Rows.Count == 0)
                                        {
                                            string resp = "El archivo no tiene el formato correcto: " + pathTemporal;

                                            if (cantidadFilas > 1)
                                            {
                                                MailTo.ArchivoFormatoIncorrecto(pathFinal);
                                            }
                                            else
                                            {
                                                resp = "El archivo no tiene registros: " + pathTemporal;
                                            }

                                            log.Error(resp);
                                            Util.BorrarArchivo(pathFinal);
                                        }

                                        if (dtErrores.Rows.Count > 0)
                                        {
                                            log.Info(string.Format("Se encontraron {0} registros con errores", dtErrores.Rows.Count));
                                            nombreArchivoExcel = item.FileName;
                                            pathTemporal = Path.Combine(repositorioTemporal, nombreArchivoExcel);
                                            Util.GeneraExcel(dtErrores, pathTemporal, "ERRORES");
                                            string pathExcelErrores    = Util.GuardarExcel(pathTemporal, "cambio.almacen.directorio.cliente.salida", nombreArchivoExcel);
                                            MailTo.ArchivoFormatoIncorrecto(pathExcelErrores);
                                            log.Info("Se ha enviado reporte de errores al cliente: " + pathExcelErrores);
                                            File.Delete(pathTemporal);
                                        }
                                    }
                                    else
                                    {
                                        log.Error("El archivo no pudo ser copiado a la ruta final: " + pathFinal);
                                    }
                                }
                                else
                                {
                                    log.Info("El tipo de archivo " + adjunto.ContentType.MimeType.ToLower() + " no es aceptado");
                                }
                            }
                        }
                        else
                        {
                            log.Info(string.Format("El correo ID: {0} no tenia archivos adjuntos", mensajeId));
                        }

                        //Registramos el correo en la tabla CorreoProcesado para que no vuelva a ser procesado
                        log.Info("Ingresar correo procesado Id: " + mensajeId);
                        CorreoProcesadoBLL correoProcesadoBLL           = new CorreoProcesadoBLL();
                        CAMBIO_ALMACEN_CORREO_PROCESADO correoProcesado = new CAMBIO_ALMACEN_CORREO_PROCESADO();
                        correoProcesado.correo_id                       = mensajeId;
                        correoProcesado.fecha_proceso                   = DateTime.Now;
                        correoProcesado.esta_activo                     = true;
                        correoProcesadoBLL.CrearCorreoProcesado(correoProcesado);
                    }

                    List<ResumenCambioAlmacenBO> listaResumen = ObtenerListaResumen(listaCambiosAlmacen);

                    if (listaResumen.Count > 0)
                    {
                        log.Info("Generando Reporte Resumen");
                        string formato              = new VariableConfiguracionBLL().ObtenerVariablePorNombre("cambio.almacen.formato.reportes");
                        string nombreArchivo        = string.Format("{0}{1}.{2}", "ResumenCambioAlmacen_", DateTime.Now.ToString("yyyyMMddHHmm"), formato);
                        string pathReporteTemporal  = Path.Combine(repositorioTemporal, nombreArchivo);

                        DataTable dtResumen         = Util.ToDataTables(listaResumen);
                        string nombreHoja           = "RESUMEN CAMBIOS DE ALMACEN";

                        if (Util.GeneraExcel(dtResumen, pathReporteTemporal, nombreHoja))
                        {
                            string pathResumenFinal = Util.GuardarExcel(pathReporteTemporal, "cambio.almacen.directorio.resumen", nombreArchivo);

                            if (!string.IsNullOrEmpty(pathResumenFinal))
                            {
                                log.Info("Enviando Reporte Resumen");
                                if (MailTo.ReporteResumenSodimac(pathResumenFinal))
                                {
                                    log.Info("Reporte Enviado");
                                    ActualizarCambiosAlmacen(listaCambiosAlmacen);

                                    File.Delete(pathReporteTemporal);
                                }
                                else
                                {
                                    log.Error("Reporte no pudo ser enviado");
                                }
                            }
                            else
                            {
                                log.Error("Reporte no pudo ser copiado a la ruta final");
                            }
                        }
                        else
                        {
                            log.Error("Reporte no pudo ser generado");
                        }
                    }

                    log.Debug("---------FINALIZO SERVICIO CORREO EXCEL------------");
                }
                catch (Exception ex)
                {
                    //Logger log = LogManager.GetLogger("Error");
                    log.Error(ex.Message.ToString());
                    MailTo.aviso_error(ex);
                    log.Error("Se ha creado un registro de log en LOG_API_CORREO");
                }
                finally
                {
                    if (semaforo.EstaProcesando)
                    {
                        Semaforo.setInstance("ProcesoCorreoExcel");
                        if (semaforo.CantidadArchivosConError > 0)
                            Semaforo.setInstance("ProcesoCorreoExcel");
                    }
                }
            }
        }

        public List<MensajeCorreo> ObtenerCorreosImap(List<string> seenUids)
        {
            VariableConfiguracionBLL variableGlobalBLL = new VariableConfiguracionBLL();

            string hostname = variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.host");
            int port        = int.Parse(variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.puerto"));
            bool useSsl     = Convert.ToBoolean(variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.ssl"));
            string username = variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.username");
            string password = variableGlobalBLL.ObtenerVariablePorNombre("cambio.almacen.email.password");

            List<MensajeCorreo> listaMensajes = new List<MensajeCorreo>();

            using (var client = new ImapClient())
            {
                using (var cancel = new CancellationTokenSource())
                {
                    client.Connect(hostname, port, useSsl, cancel.Token);
                    client.AuthenticationMechanisms.Remove("XOAUTH");
                    client.Authenticate(username, password, cancel.Token);

                    var inbox = client.Inbox;
                    inbox.Open(FolderAccess.ReadOnly, cancel.Token);

                    DateTime fechaProceso               = Util.ObtenerFechaProceso();
                    DateSearchQuery query               = SearchQuery.DeliveredAfter(DateTime.Parse(fechaProceso.ToString("yyyy-MM-dd")));
                    int total                           = 0;
                    List<MailKit.UniqueId> listaCorreos = inbox.Search(query, cancel.Token).ToList();

                    foreach (var uid in listaCorreos)
                    {
                        if (!seenUids.Contains(uid.ToString()))
                        {
                            MensajeCorreo mensajeCorreo = new MensajeCorreo();
                            MimeMessage message         = inbox.GetMessage(uid, cancel.Token);
                            mensajeCorreo.Mensaje       = message;
                            mensajeCorreo.MensajeId     = uid.ToString();
                            mensajeCorreo.FechaRecibido = message.Date.LocalDateTime;
                            listaMensajes.Add(mensajeCorreo);
                            total++;
                        }
                    }
                    client.Disconnect(true, cancel.Token);
                }
            }
            return listaMensajes;
        }

        public List<CAMBIO_ALMACEN_PLANILLA_CLIENTE> LeerExcel(string path, out DataTable dtErrores, out int cantidadFilas)
        {
            PlanillaConfiguracionBLL planillaConfiguracionBLL               = new PlanillaConfiguracionBLL();
            List<CAMBIO_ALMACEN_PLANILLA_CONFIGURACION> listaTipoPlanilla   = planillaConfiguracionBLL.ObtenerListaPlanillaConfiguracion();
            List<CAMBIO_ALMACEN_PLANILLA_CLIENTE> listaRegistrosExcel       = new List<CAMBIO_ALMACEN_PLANILLA_CLIENTE>();
            int cantidadCol                                                 = listaTipoPlanilla.Where(x => x.es_obligatorio).ToList().Count;
            cantidadFilas                                                   = 0;
            List<string> columnasObligatorias                               = listaTipoPlanilla.Where(x => x.es_obligatorio).Select(n => n.nombre).ToList();
            dtErrores                                                       = new DataTable();

            try
            {
                if (File.Exists(path))
                {
                    using (FileStream file = File.OpenRead(path))
                    {
                        if (file.Length > 0)
                        {
                            FileInfo existingFile = new FileInfo(path);
                            using (ExcelPackage package = new ExcelPackage(existingFile))
                            {
                                ExcelWorksheet worksheet    = package.Workbook.Worksheets[1];

                                // leer las celdas
                                int rows                    = worksheet.Dimension.End.Row;
                                int cols                    = worksheet.Dimension.End.Column;
                                int obligatorioLeido        = 0;
                                cantidadFilas               = rows;
                                List<string> columnasLeidas = new List<string>();
                                dtErrores                   = ObtenerDataTableErrores(worksheet);
                                string mensajeError         = string.Empty;

                                if (cols >= cantidadCol)
                                {
                                    for (int row = 2; row <= rows; row++)
                                    {
                                        CAMBIO_ALMACEN_PLANILLA_CLIENTE planilla    = new CAMBIO_ALMACEN_PLANILLA_CLIENTE();
                                        planilla.esta_activo                        = true;
                                        obligatorioLeido                            = 0;
                                        mensajeError                                = string.Empty;

                                        DataRow rowError                            = dtErrores.NewRow();

                                        for (int col = 1; col <= cols; col++)
                                        {
                                            string nombreColumna = (worksheet.Cells[1, col].Value != null)? worksheet.Cells[1, col].Value.ToString() : string.Empty;
                                            if (!string.IsNullOrEmpty(nombreColumna))
                                            {
                                                nombreColumna                                       = nombreColumna.Trim().ToUpper();
                                                CAMBIO_ALMACEN_PLANILLA_CONFIGURACION tipoPlanilla  = listaTipoPlanilla.Where(x => x.nombre.Trim().ToUpper() == nombreColumna).FirstOrDefault();

                                                if (tipoPlanilla != null)
                                                {
                                                    PropertyInfo propertyInfo   = planilla.GetType().GetProperty(tipoPlanilla.campo_db);
                                                    string valor                = ExtraerDato(worksheet, row, col);
                                                    rowError[nombreColumna]     = valor;

                                                    string validacion = ValidacionTipo(valor, tipoPlanilla);

                                                    if (validacion == string.Empty)
                                                    {
                                                        if (propertyInfo.PropertyType.IsGenericType && propertyInfo.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
                                                        {
                                                            propertyInfo.SetValue(planilla, Convert.ChangeType(valor.ToUpper(), Nullable.GetUnderlyingType(propertyInfo.PropertyType)), null);
                                                        }
                                                        else
                                                        {
                                                            propertyInfo.SetValue(planilla, Convert.ChangeType(valor.ToUpper(), propertyInfo.PropertyType), null);
                                                        }
                                                    }
                                                    else
                                                    {
                                                        mensajeError += (string.IsNullOrEmpty(mensajeError))? validacion : "; " + validacion;
                                                    }

                                                    if (tipoPlanilla.es_obligatorio && validacion == string.Empty)
                                                    {
                                                        obligatorioLeido++;
                                                        columnasLeidas.Add(nombreColumna);
                                                    }
                                                }
                                            }
                                            else break;
                                        }

                                        if (obligatorioLeido == cantidadCol)
                                        {
                                            if (!ExisteRegistro(listaRegistrosExcel, planilla))
                                            {
                                                listaRegistrosExcel.Add(planilla);
                                            }
                                        }
                                        else
                                        {
                                            string columnasSinProcesar = string.Empty;
                                            columnasObligatorias.Where(c => !columnasLeidas.Contains(c.Trim().ToUpper())).ForEach(columna => columnasSinProcesar += (string.IsNullOrEmpty(columnasSinProcesar))? columna : ", " + columna);
                                            rowError["ERROR"] += (!string.IsNullOrEmpty(columnasSinProcesar))? string.Format("Las columnas: {0} son obligatorias", columnasSinProcesar) : string.Empty;
                                            log.Info(string.Format("El registro del contenedor {0} no posee las columnas obligatorias ({1}) y no fué procesado", planilla.contenedor, columnasSinProcesar));
                                        }

                                        if (!string.IsNullOrEmpty(mensajeError))
                                        {
                                            log.Info(string.Format("El registro del contenedor {0} no fué procesado al tener los siguientes errores: {1}", planilla.contenedor, mensajeError));
                                            rowError["ERROR"] +=  (string.IsNullOrEmpty(rowError["ERROR"].ToString()))? mensajeError : "; " + mensajeError;
                                        }

                                        if (!string.IsNullOrEmpty(rowError["Error"].ToString()))
                                        {
                                            dtErrores.Rows.Add(rowError);
                                        }
                                    }

                                    log.Info(string.Format("Se leyeron correctamente {0} registros del excel", (rows - 1)));
                                }

                                package.Dispose();
                            }
                        }

                        file.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                //Logger log = LogManager.GetLogger("Error");
                log.Error(ex.Message.ToString());
                MailTo.aviso_error(ex);
                log.Error("Se ha creado un registro de log en LOG_API_CORREO");
            }
            return listaRegistrosExcel;
        }

        public string ValidacionTipo(string valor, CAMBIO_ALMACEN_PLANILLA_CONFIGURACION tipoPlanilla)
        {
            if (string.IsNullOrEmpty(valor))
            {
                return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(tipoPlanilla.tipo_validacion.ToLower()) + " en blanco";
            }
            else
            {
                switch (tipoPlanilla.tipo_validacion)
                {
                    case "CONTENEDOR":
                        {
                            if (Util.ValidarCodigoContenedor(valor)) return string.Empty;
                            else return "Código de Contenedor inválido";
                            break;
                        }
                    case "BL":
                        {
                            if (Util.ValidaBL(valor)) return string.Empty;
                            else return "N° de BL inválido";
                            break;
                        }
                    case "RUT":
                        {
                            if (Util.ValidarRut(valor) == 0) return string.Empty;
                            else return "Rut inválido";
                            break;
                        }
                    case "AGENTE":
                        {
                            if (Util.ValidaAgente(valor)) return string.Empty;
                            else return "Agente no corresponde a Juan Carlos Stephens";
                            break;
                        }
                    case "FECHA":
                        {
                            if (Util.ValidaFecha(valor)) return string.Empty;
                            else return "La Fecha ETA es inválida";
                            break;
                        }
                    default:
                        return string.Empty;
                        break;
                }
            }
        }

        public string ExtraerDato(ExcelWorksheet worksheet, int fila, int columna)
        {
            string str_value = string.Empty;
            if (worksheet.Cells[fila, columna].Value != null)
            {
                str_value = worksheet.Cells[fila, columna].Value.ToString().Trim();
            }

            return str_value;
        }

        public List<CAMBIO_ALMACEN> ObtenerListaCambioAlmacen(List<CAMBIO_ALMACEN_PLANILLA_CLIENTE> listaDetalle, DateTime fechaRecibido)
        {
            List<CAMBIO_ALMACEN> listaCambioAlmacen         = new List<CAMBIO_ALMACEN>();
            List<CAMBIO_ALMACEN_AGENTE> listaAgentes        = new AgenteBLL().ObtenerAgentes();
            List<CAMBIO_ALMACEN_COMPANIA> listaCompanias    = new CompaniaBLL().ObtenerCompanias();

            foreach (CAMBIO_ALMACEN_PLANILLA_CLIENTE detalle in listaDetalle)
            {
                CAMBIO_ALMACEN_AGENTE agente                = listaAgentes.Where(a => a.nombre.Trim().ToUpper() == detalle.agente.Trim().ToUpper()).FirstOrDefault();
                CAMBIO_ALMACEN_COMPANIA compania            = listaCompanias.Where(a => a.rut.Trim().ToUpper() == detalle.rut_extraportuario.Trim().ToUpper()).FirstOrDefault();

                if (agente != null && compania != null)
                {
                    CAMBIO_ALMACEN cambioAlmacen                        = new CAMBIO_ALMACEN();
                    cambioAlmacen.cambio_almacen_agente_id              = agente.cambio_almacen_agente_id;
                    cambioAlmacen.cambio_almacen_compania_id            = compania.cambio_almacen_compania_id;
                    cambioAlmacen.cambio_almacen_estado_id              = (int)Enumerables.EstadoCambioAlmacen.RECIBIDO;
                    cambioAlmacen.cambio_almacen_planilla_cliente_id    = detalle.cambio_almacen_planilla_cliente_id;
                    cambioAlmacen.contenedor                            = detalle.contenedor;
                    cambioAlmacen.bl                                    = detalle.bl;
                    cambioAlmacen.fecha_eta                             = Convert.ToDateTime(detalle.fecha_eta);
                    cambioAlmacen.nave                                  = detalle.nave;
                    cambioAlmacen.enviado                               = false;
                    cambioAlmacen.esta_activo                           = true;
                    cambioAlmacen.fecha_creacion                        = fechaRecibido;

                    listaCambioAlmacen.Add(cambioAlmacen);
                }
            }

            return listaCambioAlmacen;
        }

        public List<ResumenCambioAlmacenBO> ObtenerListaResumen(List<CAMBIO_ALMACEN> listaCambiosAlmacen)
        {
            List<ResumenCambioAlmacenBO> listaResumen           = new List<ResumenCambioAlmacenBO>();

            if (listaCambiosAlmacen.Count > 0)
            {
                List<CAMBIO_ALMACEN_COMPANIA> listaCompanias    = new CompaniaBLL().ObtenerCompanias();
                CambioAlmacenBLL cambioAlmacenBLL               = new CambioAlmacenBLL();
                log.Info("Guardando Registros de Cambio de Almacén");

                foreach (CAMBIO_ALMACEN cambioAlmacen in listaCambiosAlmacen)
                {
                    if (cambioAlmacenBLL.CambioAlmacenCrear(cambioAlmacen))
                    {
                        listaResumen.Add(new ResumenCambioAlmacenBO
                        {
                            Extraportuario  = listaCompanias.Where(x => x.cambio_almacen_compania_id == cambioAlmacen.cambio_almacen_compania_id).FirstOrDefault().nombre,
                            BL              = cambioAlmacen.bl,
                            Contenedor      = cambioAlmacen.contenedor,
                            Nave            = cambioAlmacen.nave,
                            ETA             = cambioAlmacen.fecha_eta.Value
                        });
                    }
                }
            }

            //listaResumen.AddRange(ObtenerListaResumenSinInformar());
            //listaResumen = listaResumen.Distinct().ToList();

            return listaResumen.OrderBy(x => x.ETA).ToList();
        }

        public List<ResumenCambioAlmacenBO> ObtenerListaResumenSinInformar()
        {
            List<ResumenCambioAlmacenBO> listaResumen           = new List<ResumenCambioAlmacenBO>();
            log.Info("Buscando Registros de Cambio de Almacén sin informar");
            List<CAMBIO_ALMACEN> listaSinInformar               = new CambioAlmacenBLL().ObtenerListaCambioAlmacenSinInformar();

            if (listaSinInformar.Count > 0)
            {
                List<CAMBIO_ALMACEN_COMPANIA> listaCompanias    = new CompaniaBLL().ObtenerCompanias();
                CambioAlmacenBLL cambioAlmacenBLL               = new CambioAlmacenBLL();

                foreach (CAMBIO_ALMACEN cambioAlmacen in listaSinInformar)
                {
                    listaResumen.Add(new ResumenCambioAlmacenBO
                    {
                        Extraportuario  = listaCompanias.Where(x => x.cambio_almacen_compania_id == cambioAlmacen.cambio_almacen_compania_id).FirstOrDefault().nombre,
                        BL              = cambioAlmacen.bl,
                        Contenedor      = cambioAlmacen.contenedor,
                        Nave            = cambioAlmacen.nave,
                        ETA             = cambioAlmacen.fecha_eta.Value
                    });
                }
            }

            return listaResumen.OrderBy(x => x.ETA).ToList();
        }

        public void ActualizarCambiosAlmacen(List<CAMBIO_ALMACEN> listaCambiosAlmacen)
        {
            CambioAlmacenBLL cambioAlmacenBLL = new CambioAlmacenBLL();
            log.Info("Actualizando estado y bitacoras de registros");
            listaCambiosAlmacen.ForEach(d =>
            {
                cambioAlmacenBLL.EditarEstadoCambioAlmacen(d.cambio_almacen_id, (int)Enumerables.EstadoCambioAlmacen.INFORMADO);
                d.cambio_almacen_estado_id = (int)Enumerables.EstadoCambioAlmacen.INFORMADO;
                cambioAlmacenBLL.CambioAlmacenBitacoraCrear(d);

                cambioAlmacenBLL.EditarEstadoCambioAlmacen(d.cambio_almacen_id, (int)Enumerables.EstadoCambioAlmacen.PENDIENTE);
                d.cambio_almacen_estado_id = (int)Enumerables.EstadoCambioAlmacen.PENDIENTE;
                cambioAlmacenBLL.CambioAlmacenBitacoraCrear(d);
            });
        }

        public bool ExisteArchivo(string path)
        {
            string md5                              = Util.GetMD5HashFromFile(path);
            List<string> listaArchivosProcesados    = new PlanillaClienteCabeceraBLL().ObtenerListaArchivosCargados();

            foreach(string ubicacion in listaArchivosProcesados)
            {
                string md5Procesado = Util.GetMD5HashFromFile(ubicacion);
                if (md5 == md5Procesado)
                {
                    return true;
                }
            }

            return false;
        }

        public bool ExisteRegistro(List<CAMBIO_ALMACEN_PLANILLA_CLIENTE> listaExcelActual, CAMBIO_ALMACEN_PLANILLA_CLIENTE registro)
        {
            if (listaExcelActual.Where(c =>
                                            c.contenedor.Trim().ToUpper() == registro.contenedor.Trim().ToUpper() &&
                                            c.nave.Trim().ToUpper() == registro.nave.Trim().ToUpper() &&
                                            c.bl.Trim().ToUpper() == registro.bl.Trim().ToUpper() &&
                                            c.fecha_eta.Trim().ToUpper() == registro.fecha_eta.Trim().ToUpper()
                                        ).Count() > 0)
            {
                log.Info("El siguiente contenedor con Bl, nave y fecha ETA ya se encuentra en el archivo actual: " + registro.contenedor.Trim().ToUpper());
                return true;
            }
            else if (listaRegistrosProcesados.Where(c =>
                                            c.contenedor.Trim().ToUpper() == registro.contenedor.Trim().ToUpper() &&
                                            c.nave.Trim().ToUpper() == registro.nave.Trim().ToUpper() &&
                                            c.bl.Trim().ToUpper() == registro.bl.Trim().ToUpper() &&
                                            c.fecha_eta.Trim().ToUpper() == registro.fecha_eta.Trim().ToUpper()
                                        ).Count() > 0)
            {
                log.Info("El siguiente contenedor con Bl, nave y fecha ETA ya se encuentra en el sistema: " + registro.contenedor.Trim().ToUpper());
                return true;
            }

            return false;
        }

        public DataTable ObtenerDataTableErrores(ExcelWorksheet worksheet)
        {
            DataTable dt    = new DataTable();
            int cols        = worksheet.Dimension.End.Column;

            for (int i = 1; i <= cols; i++)
            {
                string nombreColumna = (worksheet.Cells[1, i].Value != null)? worksheet.Cells[1, i].Value.ToString() : string.Empty;

                if (!string.IsNullOrEmpty(nombreColumna))
                {
                    nombreColumna = nombreColumna.Trim().ToUpper();
                    dt.Columns.Add(nombreColumna);
                }
                else break;
            }

            dt.Columns.Add("ERROR");
            return dt;
        }
    }
}