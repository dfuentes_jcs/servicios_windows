﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.BO
{
    public class ResumenCambioAlmacenBO
    {
        public string Extraportuario { get; set; }
        public string Contenedor { get; set; }
        public string BL { get; set; }
        public string Nave { get; set; }
        public DateTime ETA { get; set; }
    }
}
