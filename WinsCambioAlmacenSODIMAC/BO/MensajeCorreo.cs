﻿using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.BO
{
    public class MensajeCorreo
    {
        public MimeMessage Mensaje { get; set; }
        public string MensajeId { get; set; }
        public DateTime FechaRecibido { get; set; }
    }
}
