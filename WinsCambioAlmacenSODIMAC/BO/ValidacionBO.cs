﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.BO
{
    public class ValidacionBO
    {
        public int Fila { get; set; }
        public int Columna { get; set; }
        public string NombreColumna { get; set; }
        public string Error { get; set; }
    }
}
