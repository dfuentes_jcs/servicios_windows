﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.BO
{
    public class ReporteCambiosRealizadosBO
    {
        public string Extraportuario { get; set; }
        public string Contenedor { get; set; }
        public string BL { get; set; }
        public string POD { get; set; }
        public string TamTipoContenedor { get; set; }
        public string TipoCarga { get; set; }
        public string ETA { get; set; }
        public string Nave { get; set; }
        public string Agente { get; set; }
        public string EstadoContenedor { get; set; }
        public string Manifiesto { get; set; }
    }
}
