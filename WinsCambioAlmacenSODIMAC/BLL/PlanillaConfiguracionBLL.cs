﻿using JCSSodimacWService.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.BLL
{
    public class PlanillaConfiguracionBLL
    {
        public List<CAMBIO_ALMACEN_PLANILLA_CONFIGURACION> ObtenerListaPlanillaConfiguracion()
        {
            return new PlanillaConfiguracionDAL().ObtenerListaPlanillaConfiguracion();
        }
    }
}
