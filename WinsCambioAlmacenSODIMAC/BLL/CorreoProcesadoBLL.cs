﻿using JCSSodimacWService.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.BLL
{
    public class CorreoProcesadoBLL
    {
        public List<string> ListaCorreoProcesado(DateTime fechaProceso)
        {
            List<string> lista = new CorreoProcesadoDAL().ListaCorreoProcesado(fechaProceso);
            return lista;
        }


        public void CrearCorreoProcesado(CAMBIO_ALMACEN_CORREO_PROCESADO correoProcesado) {

            new CorreoProcesadoDAL().CrearCorreoProcesado(correoProcesado);
        }
    }
}
