﻿using JCSSodimacWService.DAL;
using JCSSodimacWService.Utiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.BLL
{
    public class CambioAlmacenBLL
    {
        public bool CambioAlmacenListaCrear(List<CAMBIO_ALMACEN> listaCambioAlmacen)
        {
            return new CambioAlmacenDAL().CambioAlmacenListaCrear(listaCambioAlmacen);
        }

        public bool CambioAlmacenCrear(CAMBIO_ALMACEN cambioAlmacen)
        {
            if (new CambioAlmacenDAL().CambioAlmacenCrear(cambioAlmacen))
            {
                return CambioAlmacenBitacoraCrear(cambioAlmacen);
            }
            else return false;
        }

        public bool CambioAlmacenBitacoraCrear(CAMBIO_ALMACEN cambioAlmacen)
        {
            CAMBIO_ALMACEN_BITACORA bitacora    = new CAMBIO_ALMACEN_BITACORA();
            bitacora.cambio_almacen_estado_id   = cambioAlmacen.cambio_almacen_estado_id;
            bitacora.cambio_almacen_id          = cambioAlmacen.cambio_almacen_id;
            bitacora.esta_activo                = true;
            bitacora.fecha_creacion             = (cambioAlmacen.cambio_almacen_estado_id == (int)Enumerables.EstadoCambioAlmacen.RECIBIDO)? cambioAlmacen.fecha_creacion : DateTime.Now;
            bitacora.usuario_id                 = Constante.USUARIO_TAREA_PROGRAMADA_ID;
            return new CambioAlmacenBitacoraDAL().PlanillaClienteBitacoraCrear(bitacora);
        }

        public bool EditarEstadoCambioAlmacen(int cambioAlmacenId, int estadoId)
        {
            return new CambioAlmacenDAL().EditarEstadoCambioAlmacen(cambioAlmacenId, estadoId, (estadoId == (int)Enumerables.EstadoCambioAlmacen.ENVIADO)? true : false);
        }

        public List<CAMBIO_ALMACEN_PLANILLA_CLIENTE> ObtenerListaRealizadosSinEnviar()
        {
            return new CambioAlmacenDAL().ObtenerListaRealizadosSinEnviar((int)Enumerables.EstadoCambioAlmacen.REALIZADO);
        }

        public List<CAMBIO_ALMACEN> ObtenerListaCambioAlmacenSinInformar()
        {
            return new CambioAlmacenDAL().ObtenerListaCambioAlmacenSinInformar((int)Enumerables.EstadoCambioAlmacen.RECIBIDO);
        }
    }
}
