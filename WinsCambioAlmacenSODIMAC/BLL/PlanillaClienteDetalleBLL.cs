﻿using JCSSodimacWService.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.BLL
{
    public class PlanillaClienteDetalleBLL
    {
        public bool PlanillaClienteDetalleListaCrear(List<CAMBIO_ALMACEN_PLANILLA_CLIENTE> listaDetalle)
        {
            return new PlanillaClienteDetalleDAL().PlanillaClienteDetalleListaCrear(listaDetalle);
        }

        public List<CAMBIO_ALMACEN_PLANILLA_CLIENTE> ObtenerListaClienteDetalleProcesado()
        {
            return new PlanillaClienteDetalleDAL().ObtenerListaClienteDetalleProcesado();
        }
    }
}
