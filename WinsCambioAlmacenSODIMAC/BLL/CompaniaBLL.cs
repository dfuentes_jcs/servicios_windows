﻿using JCSSodimacWService.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.BLL
{
    public class CompaniaBLL
    {
        public CAMBIO_ALMACEN_COMPANIA ObtenerCompaniaPorNombre(string nombre)
        {
            return new CompaniaDAL().ObtenerCompaniaPorNombre(nombre);
        }

        public List<CAMBIO_ALMACEN_COMPANIA> ObtenerCompanias()
        {
            return new CompaniaDAL().ObtenerCompanias();
        }
    }
}
