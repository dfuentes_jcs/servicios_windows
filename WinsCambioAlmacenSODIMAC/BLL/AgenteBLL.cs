﻿using JCSSodimacWService.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.BLL
{
    public class AgenteBLL
    {
        public CAMBIO_ALMACEN_AGENTE ObtenerAgentePorNombre(string nombre)
        {
            CAMBIO_ALMACEN_AGENTE agente = new AgenteDAL().ObtenerAgentePorNombre(nombre);
            return agente;
        }

        public List<CAMBIO_ALMACEN_AGENTE> ObtenerAgentes()
        {
            return new AgenteDAL().ObtenerAgentes();
        }
    }
}
