﻿using JCSSodimacWService.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSSodimacWService.BLL
{
    public class PlanillaClienteCabeceraBLL
    {
        public bool PlanillaClienteCabeceraCrear(CAMBIO_ALMACEN_PLANILLA_CLIENTE_CABECERA cabecera)
        {
            return new PlanillaClienteCabeceraDAL().PlanillaClienteCabeceraCrear(cabecera);
        }

        public List<string> ObtenerListaArchivosCargados()
        {
            return new PlanillaClienteCabeceraDAL().ObtenerListaArchivosCargados();
        }
    }
}
