﻿using ClosedXML.Excel;
using JCSControlPuertoWService.BLL;
using JCSControlPuertoWService.BO;
using JCSControlPuertoWService.Utiles;
using Newtonsoft.Json;
using NLog;
using Quartz;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSControlPuertoWService.ServicioWindow
{
    class ProcesoReporteDespachosSinSolicitar : IJob
    {
        readonly Logger log                 = LogManager.GetCurrentClassLogger();
        readonly private Semaforo semaforo  = Semaforo.getInstance("ProcesoReporteDespachosSinSolicitar");
        readonly string repositorioTemporal = System.Configuration.ConfigurationManager.AppSettings["repositorioTemporal"];

        public void Execute(IJobExecutionContext context)
        {
            if (!semaforo.EstaProcesando)
            {
                semaforo.EstaProcesando = true;

                try
                {
                    log.Debug("---------COMIENZO SERVICIO REPORTE DESPACHOS SIN SOLICITAR------------");
                    ControlPuertoBLL controlPuertoBLL                   = new ControlPuertoBLL();
                    VariableConfiguracionBLL variableConfiguracionBLL   = new VariableConfiguracionBLL();
                    DateTime hoy                                        = DateTime.Now;
                    XLWorkbook wb                                       = new XLWorkbook();
                    DataTable dtEjecutivo                               = new DataTable();

                    string aduanasExcluidas                             = variableConfiguracionBLL.ObtenerVariablePorNombre("control.puerto.reporte.sin.solicitar.aduanas.excluidas");
                    List<ReporteDespachosBO> listaRegistrosBO           = controlPuertoBLL.ObtenerListaReporteDespachosSinSolicitar(aduanasExcluidas.Split(';').ToList());
                    List<string> listaEjecutivos                        = listaRegistrosBO.Select(r => r.Ejecutivo).Distinct().OrderBy(e => e).ToList();
                    List<USUARIO> listaUsuarios                         = controlPuertoBLL.ObtenerListaUsuarios().Where(u => !string.IsNullOrEmpty(u.nombre_ejecutivo_sigad)).ToList();
                    string rutaResumen                                  = variableConfiguracionBLL.ObtenerVariablePorNombre("control.puerto.reporte.sin.solicitar.directorio.resumen");
                    string formato                                      = variableConfiguracionBLL.ObtenerVariablePorNombre("control.puerto.formato.reporte");
                    string directorioReporte                            = variableConfiguracionBLL.ObtenerVariablePorNombre("control.puerto.reporte.sin.solicitar.directorio.reporte");
                    string correoDesarrollo                             = variableConfiguracionBLL.ObtenerVariablePorNombre("correo.log.error.desarrollo");
                    string correoEjecutivo                              = string.Empty;
                    string pathReporteTemporal                          = string.Empty;
                    string json                                         = string.Empty;
                    string nombreArchivo                                = string.Empty;
                    string nombreHoja                                   = string.Empty;
                    string pathReporteFinal                             = string.Empty;
                    bool enviado                                        = false;

                    if (listaRegistrosBO.Count > 0)
                    {
                        foreach (string ejecutivo in listaEjecutivos)
                        {
                            try
                            {
                                log.Info("Generando Reporte Despachos sin solicitar " + ejecutivo);

                                List<ReporteDespachosBO> listaPendientes = listaRegistrosBO.Where(d => d.Ejecutivo == ejecutivo).OrderBy(d => d.FechaCreacion).ToList();
                                correoEjecutivo         = (listaUsuarios.Where(u => u.nombre_ejecutivo_sigad.ToLower().Trim() == ejecutivo.ToLower().Trim()).Count() > 0)? 
                                                        listaUsuarios.Where(u => u.nombre_ejecutivo_sigad.ToLower().Trim() == ejecutivo.ToLower().Trim()).Select(u => u.email).FirstOrDefault() : 
                                                        correoDesarrollo;
                                json                    = JsonConvert.SerializeObject(listaPendientes);
                                dtEjecutivo             = JsonConvert.DeserializeObject<DataTable>(json);

                                nombreArchivo           = string.Format("{0}_{1}_{2}.{3}", "SinSolicitar", ejecutivo.Replace(" ","_"), DateTime.Now.ToString("yyyy-MM-dd-HHmm"), formato);
                                pathReporteTemporal     = Path.Combine(repositorioTemporal, nombreArchivo);
                                nombreHoja              = "PENDIENTES";

                                Util.EliminarColumnasReporte(dtEjecutivo);
                                dtEjecutivo.Columns.Remove("Contenedor");
                                dtEjecutivo.Columns.Remove("Cantidad_Bultos_DIN");
                                dtEjecutivo.Columns.Remove("Cantidad_Contenedores");
                                dtEjecutivo.Columns.Remove("FechaRetiroParcial");
                                dtEjecutivo.Columns.Remove("Fecha_Retiro_Parcial");

                                for (int i=0; i < dtEjecutivo.Columns.Count; i++)
                                {
                                    dtEjecutivo.Columns[i].ColumnName = dtEjecutivo.Columns[i].ColumnName.Replace('_', ' ').ToUpper();
                                }

                                dtEjecutivo.AcceptChanges();

                                wb.Worksheets.Add(dtEjecutivo, ejecutivo.ToUpper());
                                pathReporteTemporal  = Path.Combine(repositorioTemporal, nombreArchivo);
                                
                                if (Util.GeneraExcel(dtEjecutivo, pathReporteTemporal, nombreHoja))
                                {
                                    pathReporteFinal = Util.GuardarArchivo(pathReporteTemporal, nombreArchivo, directorioReporte);

                                    if (!string.IsNullOrEmpty(pathReporteFinal) && !string.IsNullOrEmpty(correoEjecutivo))
                                    {
                                        log.Info("Enviando Reporte Despachos Sin Solicitar " + ejecutivo);
                                        if (MailTo.ReporteDespachosSinSolicitar(pathReporteFinal, correoEjecutivo, ejecutivo))
                                        {
                                            log.Info("Reporte Enviado " + ejecutivo);
                                            File.Delete(pathReporteTemporal);
                                        }
                                        else
                                        {
                                            log.Error("Reporte no pudo ser enviado " + ejecutivo);
                                        }
                                    }
                                    else
                                    {
                                        log.Error("Reporte no pudo ser copiado a la ruta final " + ejecutivo);
                                    }
                                }
                            }
                            catch (Exception ex) { enviado = MailTo.aviso_error(ex, ejecutivo); }
                        }

                        rutaResumen += Path.Combine(string.Format("\\{0}{1}.{2}", "ReporteResumenSinSolicitar_", DateTime.Now.ToString("yyyyMMdd_HHmmss"), formato));

                        wb.SaveAs(rutaResumen);
                        if (MailTo.ReporteDespachosSinSolicitarResumen(rutaResumen))
                        {
                            log.Info("Reporte resumen enviado");
                        }
                    }

                    log.Debug("---------FINALIZO SERVICIO REPORTE DESPACHOS SIN SOLICITAR------------");
                }
                catch (Exception ex)
                {
                    Logger log = LogManager.GetLogger("Error");
                    log.Error(ex.Message.ToString());
                    MailTo.aviso_error(ex, string.Empty);
                    log.Error("Se ha creado un registro de log en LOG_API_CORREO");
                }
                finally
                {
                    if (semaforo.EstaProcesando)
                    {
                        Semaforo.setInstance("ProcesoReporteDespachosSinSolicitar");
                        if (semaforo.CantidadArchivosConError > 0)
                            Semaforo.setInstance("ProcesoReporteDespachosSinSolicitar");
                    }
                }
            }
        }
    }
}
