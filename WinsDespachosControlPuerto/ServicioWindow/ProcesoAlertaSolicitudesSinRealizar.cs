﻿using ClosedXML.Excel;
using JCSControlPuertoWService.BLL;
using JCSControlPuertoWService.BO;
using JCSControlPuertoWService.Utiles;
using Newtonsoft.Json;
using NLog;
using Quartz;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSControlPuertoWService.ServicioWindow
{
    class ProcesoAlertaSolicitudesSinRealizar : IJob
    {
        readonly Logger log                 = LogManager.GetCurrentClassLogger();
        readonly private Semaforo semaforo  = Semaforo.getInstance("ProcesoAlertaSolicitudesSinRealizar");
        readonly string repositorioTemporal = System.Configuration.ConfigurationManager.AppSettings["repositorioTemporal"];

        public void Execute(IJobExecutionContext context)
        {
            if (!semaforo.EstaProcesando)
            {
                semaforo.EstaProcesando = true;

                try
                {
                    log.Debug("---------COMIENZO SERVICIO REPORTE SOLICITUDES SIN REALIZAR------------");
                    ControlPuertoBLL controlPuertoBLL                   = new ControlPuertoBLL();
                    VariableConfiguracionBLL variableConfiguracionBLL   = new VariableConfiguracionBLL();

                    List<ReporteDespachosBO> listaRegistrosBO           = controlPuertoBLL.ObtenerListaReporteSolicitudesSinRealizar();
                    List<CONTROL_PUERTO_USUARIO_CORREO_TIPO> operativos = controlPuertoBLL.ObtenerUsuariosOperativos();
                    List<ADUANA> listaAduanas                           = controlPuertoBLL.ObtenerAduanas();

                    if (listaRegistrosBO.Count > 0)
                    {
                        List<int> listaAduanasId                = listaRegistrosBO.Select(d => d.AduanaId).Distinct().ToList();
                        List<ReporteDespachosBO> listaAduanaBO  = new List<ReporteDespachosBO>();

                        string formato              = variableConfiguracionBLL.ObtenerVariablePorNombre("control.puerto.formato.reporte");
                        string correoDesarrollo     = variableConfiguracionBLL.ObtenerVariablePorNombre("correo.log.error.desarrollo");
                        string directorioReporte    = variableConfiguracionBLL.ObtenerVariablePorNombre("control.puerto.directorio.reporte");
                        string nombreArchivo        = string.Empty; 
                        string pathReporteTemporal  = string.Empty;
                        string nombreHoja           = string.Empty;
                        string nombreAduana         = string.Empty;

                        foreach (int aduanaId in listaAduanasId)
                        {
                            listaAduanaBO                       = listaRegistrosBO.Where(d => d.AduanaId == aduanaId).ToList();
                            nombreAduana                        = listaAduanas.Where(a => a.id == aduanaId).FirstOrDefault().nombre;
                            List<string> listaCorreosOperativos = operativos.Where(o => o.id_aduana != null && o.id_aduana == aduanaId).Select(o => o.USUARIO.email).Distinct().ToList();
                            List<string> listaCorreosCopia      = controlPuertoBLL.ObtenerDestinatariosPorReporteYAduana(aduanaId, (int)Enumerables.Reportes.OPERACIONES_SIN_REALIZAR);

                            if (listaCorreosOperativos == null || listaCorreosOperativos.Count == 0) listaCorreosOperativos.Add(correoDesarrollo);

                            log.Info("Generando Reporte sin realizar, aduana " + nombreAduana);
                            nombreHoja           = "OPERACIONES SIN REALIZAR";
                            nombreArchivo        = string.Format("{0}_{1}_{2}.{3}", "SolicitudesSinRealizar", nombreAduana, DateTime.Now.ToString("yyyy-MM-dd-HHmm"), formato);
                            pathReporteTemporal  = Path.Combine(repositorioTemporal, nombreArchivo);

                            //DataTable dtReporte         = Util.ToDataTables(listaRegistrosBO);
                            string json                 = JsonConvert.SerializeObject(listaAduanaBO);
                            DataTable dtReporte         = JsonConvert.DeserializeObject<DataTable>(json);

                            Util.EliminarColumnasReporte(dtReporte);
                            dtReporte.Columns.Remove("Contenedor");
                            dtReporte.Columns.Remove("Cantidad_Bultos_DIN");
                            dtReporte.Columns.Remove("Cantidad_Contenedores");
                            dtReporte.Columns.Remove("FechaRetiroParcial");
                            dtReporte.Columns.Remove("Fecha_Retiro_Parcial");

                            for (int i=0; i < dtReporte.Columns.Count; i++)
                            {
                                dtReporte.Columns[i].ColumnName = dtReporte.Columns[i].ColumnName.Replace('_', ' ').ToUpper();
                            }

                            dtReporte.AcceptChanges();

                            if (Util.GeneraExcel(dtReporte, pathReporteTemporal, nombreHoja))
                            {
                                string pathReporteFinal = Util.GuardarArchivo(pathReporteTemporal, nombreArchivo, directorioReporte);

                                if (!string.IsNullOrEmpty(pathReporteFinal))
                                {
                                    log.Info("Enviando Reporte sin realizar, aduana " + nombreAduana);
                                    if (MailTo.ReporteSolicitudesSinRealizar(pathReporteFinal, listaCorreosOperativos, listaCorreosCopia, nombreAduana))
                                    {
                                        log.Info("Reporte sin realizar, aduana " + nombreAduana + " Enviado");
                                        File.Delete(pathReporteTemporal);
                                    }
                                    else
                                    {
                                        log.Error("Reporte sin realizar, aduana " + nombreAduana + " no pudo ser enviado");
                                    }
                                }
                                else
                                {
                                    log.Error("Reporte sin realizar, aduana " + nombreAduana + " no pudo ser copiado a la ruta final");
                                }
                            }
                            else
                            {
                                log.Error("Reporte sin realizar, aduana " + nombreAduana + " no pudo ser generado");
                            }
                        }
                    }

                    log.Debug("---------FINALIZO SERVICIO REPORTE SOLICITUDES SIN REALIZAR------------");
                }
                catch (Exception ex)
                {
                    Logger log = LogManager.GetLogger("Error");
                    log.Error(ex.Message.ToString());
                    MailTo.aviso_error(ex, string.Empty);
                    log.Error("Se ha creado un registro de log en LOG_API_CORREO");
                }
                finally
                {
                    if (semaforo.EstaProcesando)
                    {
                        Semaforo.setInstance("ProcesoAlertaSolicitudesSinRealizar");
                        if (semaforo.CantidadArchivosConError > 0)
                            Semaforo.setInstance("ProcesoAlertaSolicitudesSinRealizar");
                    }
                }
            }
        }
    }
}
