﻿using ClosedXML.Excel;
using JCSControlPuertoWService.BLL;
using JCSControlPuertoWService.BO;
using JCSControlPuertoWService.Utiles;
using Newtonsoft.Json;
using NLog;
using Quartz;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSControlPuertoWService.ServicioWindow
{
    class ProcesoReporteDespachos : IJob
    {
        readonly Logger log                 = LogManager.GetCurrentClassLogger();
        readonly private Semaforo semaforo  = Semaforo.getInstance("ProcesoReporteDespachos");
        readonly string repositorioTemporal = System.Configuration.ConfigurationManager.AppSettings["repositorioTemporal"];

        public void Execute(IJobExecutionContext context)
        {
            if (!semaforo.EstaProcesando)
            {
                semaforo.EstaProcesando = true;

                try
                {
                    log.Debug("---------COMIENZO SERVICIO REPORTE DESPACHOS------------");
                    ControlPuertoBLL controlPuertoBLL                   = new ControlPuertoBLL();
                    VariableConfiguracionBLL variableConfiguracionBLL   = new VariableConfiguracionBLL();
                    ws_aduana.aduana wsAduana                           = new ws_aduana.aduana();
                    DateTime fechaDesde                                 = DateTime.Now.AddDays(-90);
                    DateTime fechaHasta                                 = DateTime.Now.AddDays(60);

                    try
                    {
                        if (!Constantes.ModoDesarrollo)
                        {
                            string procesaDespachosRetirados    = wsAduana.ActualizarDespachosSinRastreoConRetiro(
                                                                    DateTime.Now.AddDays(-1).ToString("yyyy-MM-dd"), 
                                                                    DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"));
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger log = LogManager.GetLogger("Error");
                        log.Error(ex.Message.ToString());
                        //MailTo.aviso_error(ex, string.Empty);
                        log.Error("Se ha creado un registro de log en LOG_API_CORREO");
                    }

                    List<ReporteDespachosBO> listaRegistrosBO       = new List<ReporteDespachosBO>();
                    //List<ReporteContenedorBO> listaContenedorBO     = new List<ReporteContenedorBO>();
                    DataTable dtContenedor                          = new DataTable();

                    Parallel.Invoke(
                    () =>
                    {
                        listaRegistrosBO    = controlPuertoBLL.ObtenerListaReporteDepachos(fechaDesde, fechaHasta);
                    },
                    () => 
                    {
                        //listaContenedorBO   = controlPuertoBLL.ObtenerListaReporteContenedor(fechaDesde, fechaHasta);
                        dtContenedor        = controlPuertoBLL.ObtenerReporteContenedor(fechaDesde, fechaHasta);
                    });

                    if (listaRegistrosBO.Count > 0 || dtContenedor.Rows.Count > 0)
                    {
                        log.Info("Generando Reporte Despachos");
                        string formato              = variableConfiguracionBLL.ObtenerVariablePorNombre("control.puerto.formato.reporte");
                        string nombreArchivo        = string.Format("{0}{1}.{2}", "ControlPuertoDespachos_", DateTime.Now.ToString("yyyy-MM-dd-HHmm"), formato);
                        string pathReporteTemporal  = Path.Combine(repositorioTemporal, nombreArchivo);

                        //DataTable dtReporte         = Util.ToDataTables(listaRegistrosBO);
                        string json                 = JsonConvert.SerializeObject(listaRegistrosBO);
                        DataTable dtReporte         = JsonConvert.DeserializeObject<DataTable>(json);

                        //json                        = JsonConvert.SerializeObject(listaContenedorBO);
                        //DataTable dtContenedor      = JsonConvert.DeserializeObject<DataTable>(json);

                        Util.EliminarColumnasReporte(dtReporte);
                        dtReporte.Columns.Remove("Contenedor");
                        dtReporte.Columns.Remove("Cantidad_Bultos_DIN");
                        dtReporte.Columns.Remove("Cantidad_Contenedores");
                        dtReporte.Columns.Remove("Fecha_Retiro_Parcial");

                        for (int i=0; i < dtReporte.Columns.Count; i++)
                        {
                            //if (dtContenedor.Columns[i].ColumnName.Contains("Fecha_")) dtContenedor.Columns[i].DataType = typeof(DateTime);
                            dtReporte.Columns[i].ColumnName = dtReporte.Columns[i].ColumnName.Replace('_', ' ').ToUpper();
                        }

                        dtReporte.AcceptChanges();

                        //dtContenedor.Columns.Remove("FechaETA");
                        //dtContenedor.Columns.Remove("FechaRastreoParcial");
                        //dtContenedor.Columns.Remove("EsImo");
                        //dtContenedor.Columns.Remove("FechaRetiroProgramada");
                        //dtContenedor.Columns.Remove("FechaSolicitudInscripcion");
                        //dtContenedor.Columns.Remove("FechaNotificacionCarga");

                        dtContenedor.Columns.Remove("ESTADO_ID");
                        dtContenedor.Columns.Remove("ROWNUMBER");
                        dtContenedor.Columns.Remove("ID_USUARIO_EJECUTIVO");
                        dtContenedor.Columns.Remove("ID_CLIENTE");
                        dtContenedor.Columns.Remove("CORR_CLIENTE");

                        for (int i=0; i < dtContenedor.Columns.Count; i++)
                        {
                            dtContenedor.Columns[i].ColumnName = dtContenedor.Columns[i].ColumnName.Replace('_', ' ');
                            if (dtContenedor.Columns[i].ColumnName.Contains("FECHA")) dtContenedor.Columns[i].DataType = typeof(DateTime);
                        }

                        dtContenedor.AcceptChanges();

                        XLWorkbook wb = new XLWorkbook();
                        wb.Worksheets.Add(dtReporte, "DESPACHOS");
                        wb.Worksheets.Add(dtContenedor, "CONTENEDORES");
                        wb.SaveAs(pathReporteTemporal);

                        string directorioReporte = variableConfiguracionBLL.ObtenerVariablePorNombre("control.puerto.directorio.reporte");
                        string pathReporteFinal = Util.GuardarArchivo(pathReporteTemporal, nombreArchivo, directorioReporte);

                        if (!string.IsNullOrEmpty(pathReporteFinal))
                        {
                            log.Info("Enviando Reporte Despachos");
                            if (MailTo.ReporteDespachos(pathReporteFinal))
                            {
                                log.Info("Reporte Enviado");
                                File.Delete(pathReporteTemporal);
                            }
                            else
                            {
                                log.Error("Reporte no pudo ser enviado");
                            }
                        }
                        else
                        {
                            log.Error("Reporte no pudo ser copiado a la ruta final");
                        }
                    }

                    log.Debug("---------FINALIZO SERVICIO REPORTE DESPACHOS------------");
                }
                catch (Exception ex)
                {
                    Logger log = LogManager.GetLogger("Error");
                    log.Error(ex.Message.ToString());
                    MailTo.aviso_error(ex, string.Empty);
                    log.Error("Se ha creado un registro de log en LOG_API_CORREO");
                }
                finally
                {
                    if (semaforo.EstaProcesando)
                    {
                        Semaforo.setInstance("ProcesoReporteDespachos");
                        if (semaforo.CantidadArchivosConError > 0)
                            Semaforo.setInstance("ProcesoReporteDespachos");
                    }
                }
            }
        }
    }
}
