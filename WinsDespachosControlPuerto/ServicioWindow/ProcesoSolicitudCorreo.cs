﻿using ClosedXML.Excel;
using DocumentFormat.OpenXml.Spreadsheet;
using JCSControlPuertoWService.BLL;
using JCSControlPuertoWService.BO;
using JCSControlPuertoWService.Utiles;
using MailKit;
using MailKit.Net.Imap;
using MailKit.Search;
using MimeKit;
using NLog;
using OfficeOpenXml;
using Quartz;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Management;
using System.Reflection;
using System.ServiceModel.Configuration;
using System.Threading;
using System.Web.Services.Description;
using System.Web.UI.WebControls;
using MimePart = MimeKit.MimePart;

namespace JCSControlPuertoWService.ServicioWindow
{
    class ProcesoSolicitudCorreo : IJob
    {
        readonly Logger log = LogManager.GetCurrentClassLogger();
        readonly private Semaforo semaforo = Semaforo.getInstance("ProcesoSolicitudCorreo");
        static string repositorioTemporal = System.Configuration.ConfigurationManager.AppSettings["repositorioTemporal"];

        static VariableConfiguracionBLL variableGlobalBLL  = new VariableConfiguracionBLL();
        static ControlPuertoBLL controlPuertoBLL           = new ControlPuertoBLL();
        static CorreoProcesadoBLL correoProcesadoBLL       = new CorreoProcesadoBLL();
        static AlmacenBLL almacenBLL                       = new AlmacenBLL();

        readonly string patronDespacho              = variableGlobalBLL.ObtenerVariablePorNombre("control.puerto.pattern.despacho");
        readonly string directorioTrabajoServer     = variableGlobalBLL.ObtenerVariablePorNombre("control.puerto.directorio.entrada");
        readonly string patronContenedor            = variableGlobalBLL.ObtenerVariablePorNombre("control.puerto.pattern.contenedor");

        static DirectoryInfo dir                    = new DirectoryInfo(repositorioTemporal);

        public void Execute(IJobExecutionContext context)
        {
            if (!semaforo.EstaProcesando)
            {
                semaforo.EstaProcesando = true;

                try
                {
                    log.Debug("---------COMIENZO SERVICIO SOLICITUD CORREO------------");

                    DateTime fechaProceso                               = Util.ObtenerFechaProceso();
                    List<string> seenUids                               = correoProcesadoBLL.ListaCorreoProcesado(fechaProceso);
                    log.Info("Total de correos procesados a la fecha: " + seenUids.Count);
                    List<MensajeCorreo> listaCorreosNoProcesados        = ObtenerCorreosImap(seenUids);
                    log.Info("Total de correos por procesar: " + listaCorreosNoProcesados.Count);
                    List<int> listaDespachos                            = new List<int>();
                    List<string> listaNaves                             = new List<string>();
                    List<CONTROL_PUERTO_ALMACEN> listaAlmacenes         = new List<CONTROL_PUERTO_ALMACEN>();
                    List<CONTROL_PUERTO_CORREO_TIPO> listaTipoCorreo    = new List<CONTROL_PUERTO_CORREO_TIPO>();

                    if (listaCorreosNoProcesados.Count > 0)
                    {
                        listaDespachos  = controlPuertoBLL.ObtenerListaDespachosExistentes();
                        listaNaves      = controlPuertoBLL.ObtenerListaNaves();
                        listaAlmacenes  = almacenBLL.ObtenerListaAlmacenes();
                        listaTipoCorreo = controlPuertoBLL.ObtenerListaTipoCorreo();
                    }

                    foreach (MensajeCorreo correoNoProcesado in listaCorreosNoProcesados)
                    {
                        MimeMessage mensaje     = correoNoProcesado.Mensaje;
                        string mensajeId        = correoNoProcesado.MensajeId;
                        string asunto           = mensaje.Subject;

                        log.Info(string.Format("Inicio proceso correo ID: {0}; Asunto: {1}", mensajeId, asunto));

                        IEnumerable<MimeEntity> listadoAdjuntos                     = mensaje.Attachments;
                        List<CONTROL_PUERTO_CORREO_PROCESADO_ADJUNTO> listaAdjuntos = new List<CONTROL_PUERTO_CORREO_PROCESADO_ADJUNTO>();

                        if (listadoAdjuntos != null)
                        {
                            foreach (var adjunto in listadoAdjuntos)
                            {
                                //Validamos que exista un excel como adjunto en el correo.
                                if (adjunto.ContentType.MimeType.ToLower().Equals("application/pdf") ||
                                    ((MimePart)adjunto).FileName.Contains(".pdf"))
                                {
                                    MimePart item               = (MimePart)adjunto;
                                    String[] splitNombreArchivo = item.FileName.Split('.');
                                    string nombreArchivoBL      = item.FileName; //Guid.NewGuid().ToString() + "." + splitNombreArchivo[splitNombreArchivo.Length - 1];
                                    string pathTemporal         = string.Empty;

                                    CONTROL_PUERTO_CORREO_PROCESADO_ADJUNTO controlPuertoAdjunto    = new CONTROL_PUERTO_CORREO_PROCESADO_ADJUNTO();
                                    controlPuertoAdjunto.content_type                               = adjunto.ContentType.MimeType.ToLower();
                                    controlPuertoAdjunto.creation_date                              = DateTime.Now;
                                    controlPuertoAdjunto.esta_activo                                = true;
                                    controlPuertoAdjunto.extension                                  = item.ContentType.MediaSubtype.ToLower();
                                    controlPuertoAdjunto.file_name                                  = item.FileName;
                                    controlPuertoAdjunto.modification_date                          = DateTime.Now;

                                    using (var stream = File.Create(Path.Combine(@dir.FullName, nombreArchivoBL)))
                                    {
                                        item.ContentObject.DecodeTo(stream);
                                        controlPuertoAdjunto.size = stream.Length;
                                        stream.Close();
                                        pathTemporal = Path.Combine(@dir.FullName, nombreArchivoBL);
                                        controlPuertoAdjunto.md5 = Util.GetMD5HashFromFile(pathTemporal);
                                    }

                                    listaAdjuntos.Add(controlPuertoAdjunto);
                                }
                                else
                                {
                                    log.Info("El tipo de archivo " + adjunto.ContentType.MimeType.ToLower() + " no es aceptado");
                                }
                            }
                        }
                        else
                        {
                            log.Info(string.Format("El correo ID: {0} no tenia archivos adjuntos", mensajeId));
                        }

                        int tipoCorreoId = 0;

                        foreach (CONTROL_PUERTO_CORREO_TIPO tipoCorreo in listaTipoCorreo)
                        {
                            if (Util.CheckAsunto(asunto, tipoCorreo.patron))
                            {
                                tipoCorreoId = tipoCorreo.control_puerto_correo_tipo_id;
                            }
                        }

                        string para = string.Empty;
                        string copia = string.Empty;

                        mensaje.To.Mailboxes.ForEach(t => para += (para == string.Empty)? t.Address : string.Format(";{0}", t.Address));
                        mensaje.Cc.Mailboxes.ForEach(c => copia += (copia == string.Empty)? c.Address : string.Format(";{0}", c.Address));

                        if (tipoCorreoId == (int)Enumerables.TipoCorreo.INSCRIPCION_DIRECTA) 
                        {
                            log.Info(string.Format("El correo ID: {0} cumple el patrón de inscripción directa", mensajeId));
                            ProcesaInscripcion(correoNoProcesado, para, copia, listaAdjuntos, listaDespachos, listaAlmacenes, listaNaves, listaTipoCorreo.Where(t => t.control_puerto_correo_tipo_id == tipoCorreoId).FirstOrDefault());
                        }
                        else if (tipoCorreoId == (int)Enumerables.TipoCorreo.CAMBIO_ALMACEN)
                        {
                            log.Info(string.Format("El correo ID: {0} cumple el patrón de cambio de almacén", mensajeId));
                            ProcesaCambioAlmacen(correoNoProcesado, para, copia, listaAdjuntos, listaDespachos, listaAlmacenes, listaTipoCorreo.Where(t => t.control_puerto_correo_tipo_id == tipoCorreoId).FirstOrDefault());
                        }
                        else
                        {
                            log.Info(string.Format("El correo ID: {0} no cumple ningún patrón conocido", mensajeId));

                            //Registramos el correo en la tabla CorreoProcesado para que no vuelva a ser procesado
                            log.Info("Ingresar correo procesado Id: " + mensajeId);

                            CONTROL_PUERTO_CORREO_PROCESADO correoProcesado = new CONTROL_PUERTO_CORREO_PROCESADO();
                            correoProcesado.correo_id                       = mensajeId;
                            correoProcesado.fecha_proceso                   = DateTime.Now;
                            correoProcesado.esta_activo                     = true;
                            correoProcesado.para                            = para;
                            correoProcesado.copia                           = copia;
                            correoProcesado.asunto                          = (mensaje.Subject.Length > 250)? mensaje.Subject.Substring(0, 250) : mensaje.Subject;
                            correoProcesado.cuerpo                          = (mensaje.HtmlBody.Length > 8000)? mensaje.HtmlBody.Substring(0, 8000) : mensaje.HtmlBody;
                            correoProcesado.desde                           = mensaje.From.Mailboxes.First().Address;
                            correoProcesado.cumple_patron                   = false;

                            bool correoIngresado = correoProcesadoBLL.CrearCorreoProcesado(correoProcesado);

                            if (correoIngresado)
                            {
                                if (listaAdjuntos.Count > 0)
                                {
                                    log.Info(string.Format("Ingresando {0} adjuntos", listaAdjuntos.Count));
                                    listaAdjuntos.ForEach(a => 
                                    {
                                        a.control_puerto_correo_procesado_id = correoProcesado.control_puerto_correo_procesado_id; 
                                    });
                                    controlPuertoBLL.InsertarAdjuntosControlPuerto(listaAdjuntos);

                                    string pathTemporal = string.Empty;
                                    string pathFinal = string.Empty;

                                    foreach (CONTROL_PUERTO_CORREO_PROCESADO_ADJUNTO adjunto in listaAdjuntos)
                                    {
                                        pathTemporal = Path.Combine(@dir.FullName, adjunto.file_name);
                                        pathFinal    = Util.GuardarAdjunto(pathTemporal, adjunto.file_name, directorioTrabajoServer, correoProcesado.correo_id);

                                        if (!string.IsNullOrEmpty(pathFinal))
                                        {
                                            log.Info("Archivo copiado a la ruta: " + pathFinal);
                                            File.Delete(pathTemporal);
                                        }
                                        else
                                        {
                                            log.Error("El archivo no pudo ser copiado a la ruta final: " + pathFinal);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    log.Debug("---------FINALIZO SERVICIO SOLICITUD CORREO------------");
                }
                catch (Exception ex)
                {
                    //Logger log = LogManager.GetLogger("Error");
                    log.Error(ex.Message.ToString());
                    MailTo.aviso_error(ex);
                    log.Error("Se ha creado un registro de log en LOG_API_CORREO");
                }
                finally
                {
                    if (semaforo.EstaProcesando)
                    {
                        Semaforo.setInstance("ProcesoSolicitudCorreo");
                        if (semaforo.CantidadArchivosConError > 0)
                            Semaforo.setInstance("ProcesoSolicitudCorreo");
                    }
                }
            }
        }

        public void ProcesaInscripcion(MensajeCorreo correo, string para, string copia, List<CONTROL_PUERTO_CORREO_PROCESADO_ADJUNTO> listaAdjuntos, List<int> listaDespachos, List<CONTROL_PUERTO_ALMACEN> listaAlmacenes, List<string> listaNaves, CONTROL_PUERTO_CORREO_TIPO tipoCorreo)
        {
            List<CONTROL_PUERTO_DOCUMENTO> listaDocumentos      = new List<CONTROL_PUERTO_DOCUMENTO>();
            List<CONTROL_PUERTO_CONTENEDOR> listaContenedores   = new List<CONTROL_PUERTO_CONTENEDOR>();

            MimeMessage mensaje     = correo.Mensaje;
            string mensajeId        = correo.MensajeId;
            string asunto           = correo.Mensaje.Subject;
            bool cumplePatron       = true;

            string nave             = Util.ExtraerNave(asunto, listaNaves);
            int almacenId           = Util.ExtraerAlmacen(asunto, listaAlmacenes);
            int despachoId          = Util.ExtraerDespachoEnListaDespachos(asunto, listaDespachos, patronDespacho);

            CONTROL_EJECUTIVO controlEjecutivo = (despachoId > 0)? controlPuertoBLL.ObtenerControlEjecutivoPorDespacho(despachoId) : new CONTROL_EJECUTIVO();

            if (string.IsNullOrEmpty(nave) && controlEjecutivo != null && !string.IsNullOrEmpty(controlEjecutivo.NAVE))
            {
                nave = controlEjecutivo.NAVE;
            }

            //Registramos el correo en la tabla CorreoProcesado para que no vuelva a ser procesado
            log.Info("Ingresar correo procesado Id: " + mensajeId);

            CONTROL_PUERTO_CORREO_PROCESADO correoProcesado = new CONTROL_PUERTO_CORREO_PROCESADO();
            correoProcesado.correo_id                       = mensajeId;
            correoProcesado.fecha_proceso                   = DateTime.Now;
            correoProcesado.esta_activo                     = true;
            correoProcesado.para                            = para;
            correoProcesado.copia                           = copia;
            correoProcesado.asunto                          = (mensaje.Subject.Length > 250)? mensaje.Subject.Substring(0, 250) : mensaje.Subject;
            correoProcesado.cuerpo                          = (mensaje.HtmlBody.Length > 8000)? mensaje.HtmlBody.Substring(0, 8000) : mensaje.HtmlBody;
            correoProcesado.desde                           = mensaje.From.Mailboxes.First().Address;
            correoProcesado.id_despacho                     = despachoId;
            correoProcesado.nave                            = nave;
            correoProcesado.control_puerto_correo_tipo_id   = (int)Enumerables.TipoCorreo.INSCRIPCION_DIRECTA;
            correoProcesado.control_puerto_almacen_id       = ((almacenId > 0)? almacenId : (int?)null);
            correoProcesado.cumple_patron                   = cumplePatron;

            bool correoIngresado = correoProcesadoBLL.CrearCorreoProcesado(correoProcesado);

            if (correoIngresado)
            {
                if (listaAdjuntos.Count > 0)
                {
                    log.Info(string.Format("Ingresando {0} adjuntos", listaAdjuntos.Count));
                    listaAdjuntos.ForEach(a => 
                    {
                        a.control_puerto_correo_procesado_id = correoProcesado.control_puerto_correo_procesado_id; 
                    });
                    controlPuertoBLL.InsertarAdjuntosControlPuerto(listaAdjuntos);
                }
            }

            if (despachoId > 0)
            {
                log.Info(string.Format("Se detectó el despacho {0} en el asunto del email", despachoId));

                string pathTemporal = string.Empty;
                string pathFinal    = string.Empty;

                foreach(CONTROL_PUERTO_CORREO_PROCESADO_ADJUNTO adjunto in listaAdjuntos)
                { 
                    CONTROL_PUERTO_DOCUMENTO documento  = new CONTROL_PUERTO_DOCUMENTO();
                    documento.esta_activo               = true;
                    documento.fecha_creacion            = DateTime.Now;
                    documento.nombre_archivo            = Path.Combine(despachoId.ToString(), adjunto.file_name);
                    documento.numero_documento          = adjunto.file_name.Split('.')[0];
                    documento.id_despacho               = despachoId;
                    documento.control_puerto_correo_procesado_adjunto_id = adjunto.control_puerto_correo_procesado_adjunto_id;

                    listaDocumentos.Add(documento);

                    pathTemporal = Path.Combine(@dir.FullName, adjunto.file_name);
                    pathFinal    = Util.GuardarAdjunto(pathTemporal, adjunto.file_name, directorioTrabajoServer, despachoId.ToString());
                                
                    if (!string.IsNullOrEmpty(pathFinal))
                    {
                        log.Info("Archivo copiado a la ruta: " + pathFinal);
                        File.Delete(pathTemporal);
                    }
                    else
                    {
                        log.Error("El archivo no pudo ser copiado a la ruta final: " + pathFinal);
                    }
                }

                if (listaDocumentos.Count > 0)
                {
                    log.Info(string.Format("Ingresando {0} documentos", listaDocumentos.Count));
                    controlPuertoBLL.InsertarDocumentosControlPuerto(listaDocumentos);
                }

                List<string> contenedores = Util.ObtenerContenedoresCorreo(mensaje.Subject, mensaje.HtmlBody, patronContenedor);

                foreach (string numeroContenedor in contenedores)
                {
                    CONTROL_PUERTO_CONTENEDOR contenedor            = new CONTROL_PUERTO_CONTENEDOR();
                    contenedor.esta_activo                          = true;
                    contenedor.fecha_creacion                       = DateTime.Now;
                    contenedor.id_despacho                          = despachoId;
                    contenedor.numero_contenedor                    = numeroContenedor;
                    contenedor.control_puerto_correo_procesado_id   = correoProcesado.control_puerto_correo_procesado_id; 

                    listaContenedores.Add(contenedor);
                }

                if (listaContenedores.Count > 0)
                {
                    log.Info(string.Format("Ingresando {0} contenedores", listaContenedores.Count));
                    controlPuertoBLL.InsertarContenedoresControlPuerto(listaContenedores);
                }

                if (cumplePatron)
                {
                    if (controlEjecutivo.CONTROL_PUERTO_ESTADO_ID == null || (controlEjecutivo.CONTROL_PUERTO_ESTADO_ID != null && 
                        controlEjecutivo.CONTROL_PUERTO_ESTADO_ID != (int)Enumerables.ControlPuertoEstado.INSCRITO &&
                        controlEjecutivo.CONTROL_PUERTO_ESTADO_ID != (int)Enumerables.ControlPuertoEstado.RETIRADO))
                    {
                        string almacen = ((almacenId > 0)? listaAlmacenes.Where(a => a.control_puerto_almacen_id == almacenId).FirstOrDefault().nombre : string.Empty);
                        bool respuesta = controlPuertoBLL.EditarDespachoControlEjecutivo(despachoId, (int)Enumerables.ControlPuertoEstado.INSCRIPCION_SOLICITADA, almacen);

                        if (respuesta)
                        {
                            log.Info("Actualizado el estado del despacho");
                            if (controlEjecutivo != null)
                            {
                                if (controlPuertoBLL.ControlPuertoBitacoraCrear(controlEjecutivo, almacenId, 0))
                                {
                                    log.Info("Ingresada la bitacora del despacho");
                                }
                            }
                            if (MailTo.EnviarCorreoControlPuerto(despachoId, (int)Enumerables.ControlPuertoEstado.INSCRIPCION_SOLICITADA, true, string.Empty, tipoCorreo))
                            {
                                log.Info("Enviado el email de cambio de estado");
                            }
                        }
                        else
                        {
                            log.Error("Estado del despacho no pudo ser modificado");
                        }
                    }
                    else
                    {
                        log.Debug("Estado del despacho no permite modificar");
                    }
                }
            }
        }

        public void ProcesaCambioAlmacen(MensajeCorreo correo, string para, string copia, List<CONTROL_PUERTO_CORREO_PROCESADO_ADJUNTO> listaAdjuntos, List<int> listaDespachos, List<CONTROL_PUERTO_ALMACEN> listaAlmacenes, CONTROL_PUERTO_CORREO_TIPO tipoCorreo)
        {
            try
            {
                log.Debug("---------COMIENZO SERVICIO CORREO CAMBIO ALMACÉN------------");

                VariableConfiguracionBLL variableGlobalBLL  = new VariableConfiguracionBLL();
                ControlPuertoBLL controlPuertoBLL           = new ControlPuertoBLL();
                CorreoProcesadoBLL correoProcesadoBLL       = new CorreoProcesadoBLL();
                AlmacenBLL almacenBLL                       = new AlmacenBLL();

                MimeMessage mensaje         = correo.Mensaje;
                string mensajeId            = correo.MensajeId;
                string asunto               = mensaje.Subject;
                List<int> despachos         = new List<int>();
                List<int> despachosCuerpo   = new List<int>();

                log.Info(string.Format("Inicio proceso cambio de almacén, asunto: {0}", asunto));

                int almacenId       = Util.ExtraerAlmacen(asunto, listaAlmacenes);
                despachosCuerpo     = Util.ExtraerListaDespachos(mensaje.HtmlBody, listaDespachos, patronDespacho);
                despachos.AddRange(despachosCuerpo);
                despachos           = despachos.Distinct().ToList();

                //Registramos el correo en la tabla CorreoProcesado para que no vuelva a ser procesado
                log.Info("Ingresar correo procesado Id: " + mensajeId);

                CONTROL_PUERTO_CORREO_PROCESADO correoProcesado = new CONTROL_PUERTO_CORREO_PROCESADO();
                correoProcesado.correo_id                       = mensajeId;
                correoProcesado.fecha_proceso                   = DateTime.Now;
                correoProcesado.esta_activo                     = true;
                correoProcesado.para                            = para;
                correoProcesado.copia                           = copia;
                correoProcesado.asunto                          = (mensaje.Subject.Length > 250)? mensaje.Subject.Substring(0, 250) : mensaje.Subject;
                correoProcesado.cuerpo                          = (mensaje.HtmlBody.Length > 8000)? mensaje.HtmlBody.Substring(0, 8000) : mensaje.HtmlBody;
                correoProcesado.desde                           = mensaje.From.Mailboxes.First().Address;
                correoProcesado.control_puerto_correo_tipo_id   = (int)Enumerables.TipoCorreo.CAMBIO_ALMACEN;
                correoProcesado.control_puerto_almacen_id       = ((almacenId > 0)? almacenId : (int?)null);
                correoProcesado.cumple_patron                   = false;
                correoProcesado.id_despacho                     = (despachos.Count == 1)? despachos[0] : (int?)null;

                bool correoIngresado = correoProcesadoBLL.CrearCorreoProcesado(correoProcesado);

                if (correoIngresado)
                {
                    if (listaAdjuntos.Count > 0)
                    {
                        log.Info(string.Format("Ingresando {0} adjuntos", listaAdjuntos.Count));
                        listaAdjuntos.ForEach(a => 
                        {
                            a.control_puerto_correo_procesado_id = correoProcesado.control_puerto_correo_procesado_id; 
                        });
                        controlPuertoBLL.InsertarAdjuntosControlPuerto(listaAdjuntos);

                        string pathTemporal = string.Empty;
                        string pathFinal    = string.Empty;

                        foreach(CONTROL_PUERTO_CORREO_PROCESADO_ADJUNTO adjunto in listaAdjuntos)
                        { 
                            pathTemporal = Path.Combine(@dir.FullName, adjunto.file_name);
                            pathFinal    = Util.GuardarAdjunto(pathTemporal, adjunto.file_name, directorioTrabajoServer, correoProcesado.control_puerto_correo_procesado_id.ToString());
                                
                            if (!string.IsNullOrEmpty(pathFinal))
                            {
                                log.Info("Archivo copiado a la ruta: " + pathFinal);
                                File.Delete(pathTemporal);
                            }
                            else
                            {
                                log.Error("El archivo no pudo ser copiado a la ruta final: " + pathFinal);
                            }
                        }
                    }

                    if (despachos.Count > 0)
                    {
                        log.Info(string.Format("Se detectaron {0} despachos en el email ({1})", despachosCuerpo.Count, despachosCuerpo.Select(a => a.ToString()).Aggregate((i, j) => i + "," + j)));

                        if (almacenId > 0)
                        {
                            log.Info(string.Format("Se detectó el almacén ({0})", listaAlmacenes.Where(a => a.control_puerto_almacen_id == almacenId).FirstOrDefault().nombre));

                            foreach (int despachoId in despachos)
                            {
                                CONTROL_EJECUTIVO controlEjecutivo = (despachoId > 0)? controlPuertoBLL.ObtenerControlEjecutivoPorDespacho(despachoId) : new CONTROL_EJECUTIVO();

                                CONTROL_PUERTO_CORREO_PROCESADO_DESPACHO despachoCorreoProcesado    = new CONTROL_PUERTO_CORREO_PROCESADO_DESPACHO();
                                despachoCorreoProcesado.control_puerto_correo_procesado_id          = correoProcesado.control_puerto_correo_procesado_id;
                                despachoCorreoProcesado.id_despacho                                 = despachoId;
                                controlPuertoBLL.InsertarDespachoCorreoProcesadoControlPuerto(despachoCorreoProcesado);

                                if (controlEjecutivo != null && controlEjecutivo.CONTROL_PUERTO_ESTADO_ID != (int)Enumerables.ControlPuertoEstado.RETIRADO)
                                {
                                    string almacen = ((almacenId > 0)? listaAlmacenes.Where(a => a.control_puerto_almacen_id == almacenId).FirstOrDefault().nombre : string.Empty);
                                    bool respuesta = controlPuertoBLL.EditarDespachoControlEjecutivo(despachoId, controlEjecutivo.CONTROL_PUERTO_ESTADO_ID.Value, almacen);

                                    if (respuesta)
                                    {
                                        if (controlEjecutivo != null)
                                        {
                                            CONTROL_PUERTO_BITACORA bitacora = (controlEjecutivo.CONTROL_PUERTO_BITACORA != null)? controlEjecutivo.CONTROL_PUERTO_BITACORA.Where(x => x.control_puerto_almacen_id_nuevo != null && x.esta_activo).OrderByDescending(f => f.fecha_creacion).FirstOrDefault() : null;
                                            int almacenAntiguoId = 0;
                                            int.TryParse((bitacora != null)? bitacora.control_puerto_almacen_id_nuevo.ToString() : "0", out almacenAntiguoId);

                                            controlEjecutivo.CONTROL_PUERTO_ESTADO_ID = (int)Enumerables.ControlPuertoEstado.CAMBIO_ALMACEN_SOLICITADO;

                                            if (controlPuertoBLL.ControlPuertoBitacoraCrear(controlEjecutivo, almacenAntiguoId, almacenId))
                                            {
                                                log.Info("Ingresada la bitacora del despacho");
                                            }
                                        }
                                        if (MailTo.EnviarCorreoControlPuerto(despachoId, (int)Enumerables.ControlPuertoEstado.CAMBIO_ALMACEN_SOLICITADO, true, almacen, tipoCorreo))
                                        {
                                            log.Info("Enviado el email de cambio de almacén");
                                        }
                                    }
                                    else
                                    {
                                        log.Error("Almacén del despacho no pudo ser modificado");
                                    }
                                }
                                else
                                {
                                    log.Debug("Estado del despacho no permite modificar");
                                }
                            }
                        }
                        else
                        {
                            log.Info("No se detectó almacén en el asunto del email");
                        }
                    }
                    else
                    {
                        log.Info("No se detectaron despachos en el email");
                    }
                }

                log.Debug("---------FINALIZO SERVICIO CORREO CAMBIO ALMACÉN------------");
            }
            catch (Exception ex)
            {
                //Logger log = LogManager.GetLogger("Error");
                log.Error(ex.Message.ToString());
                MailTo.aviso_error(ex);
                log.Error("Se ha creado un registro de log en LOG_API_CORREO");
            }
        }

        public List<MensajeCorreo> ObtenerCorreosImap(List<string> seenUids)
        {
            VariableConfiguracionBLL variableGlobalBLL = new VariableConfiguracionBLL();

            string hostname = variableGlobalBLL.ObtenerVariablePorNombre("control.puerto.email.host");
            int port        = int.Parse(variableGlobalBLL.ObtenerVariablePorNombre("control.puerto.email.puerto"));
            bool useSsl     = Convert.ToBoolean(variableGlobalBLL.ObtenerVariablePorNombre("control.puerto.email.ssl"));
            string username = variableGlobalBLL.ObtenerVariablePorNombre("control.puerto.email.username");
            string password = variableGlobalBLL.ObtenerVariablePorNombre("control.puerto.email.password");

            List<MensajeCorreo> listaMensajes = new List<MensajeCorreo>();

            using (var client = new ImapClient())
            {
                using (var cancel = new CancellationTokenSource())
                {
                    client.Connect(hostname, port, useSsl, cancel.Token);
                    client.AuthenticationMechanisms.Remove("XOAUTH");
                    client.Authenticate(username, password, cancel.Token);

                    var inbox = client.Inbox;
                    inbox.Open(FolderAccess.ReadOnly, cancel.Token);

                    DateTime fechaProceso               = Util.ObtenerFechaProceso();
                    DateSearchQuery query               = SearchQuery.DeliveredAfter(DateTime.Parse(fechaProceso.ToString("yyyy-MM-dd")));
                    int total                           = 0;
                    List<UniqueId> listaCorreos         = inbox.Search(query, cancel.Token).ToList();

                    foreach (var uid in listaCorreos)
                    {
                        if (!seenUids.Contains(uid.ToString()))
                        {
                            MensajeCorreo mensajeCorreo = new MensajeCorreo();
                            MimeMessage message         = inbox.GetMessage(uid, cancel.Token);
                            mensajeCorreo.Mensaje       = message;
                            mensajeCorreo.MensajeId     = uid.ToString();
                            mensajeCorreo.FechaRecibido = message.Date.LocalDateTime;
                            listaMensajes.Add(mensajeCorreo);
                            total++;
                        }
                    }
                    client.Disconnect(true, cancel.Token);
                }
            }
            return listaMensajes;
        }
    }
}