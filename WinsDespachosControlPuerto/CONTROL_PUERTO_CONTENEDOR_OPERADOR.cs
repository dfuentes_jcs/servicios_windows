//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JCSControlPuertoWService
{
    using System;
    using System.Collections.Generic;
    
    public partial class CONTROL_PUERTO_CONTENEDOR_OPERADOR
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CONTROL_PUERTO_CONTENEDOR_OPERADOR()
        {
            this.CONTROL_PUERTO_CONTENEDOR = new HashSet<CONTROL_PUERTO_CONTENEDOR>();
        }
    
        public int control_puerto_contenedor_operador_id { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public bool esta_activo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONTROL_PUERTO_CONTENEDOR> CONTROL_PUERTO_CONTENEDOR { get; set; }
    }
}
