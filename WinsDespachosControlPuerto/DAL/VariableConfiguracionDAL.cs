﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSControlPuertoWService.DAL
{
    class VariableConfiguracionDAL
    {
        public VARIABLE_CONFIGURACION ObtenerVariablePorNombre(string nombre)
        {
            using (var db = new JCSDBEntities())
            {
                var query = from l in db.VARIABLE_CONFIGURACION
                            where l.esta_activo == true &&
                            l.nombre.Trim().ToUpper() == nombre.Trim().ToUpper()
                            select l;

                return query.FirstOrDefault();
            }
        }
    }
}
