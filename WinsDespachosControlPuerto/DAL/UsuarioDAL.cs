﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSControlPuertoWService.DAL
{
    public class UsuarioDAL
    {
        public string ObtenerEmailUsuario(int usuarioId)
        {
            using (var db = new JCSDBEntities())
            {
                var query = from u in db.USUARIO
                            where u.id == usuarioId
                            select u.email;

                return query.FirstOrDefault();
            }
        }
    }
}
