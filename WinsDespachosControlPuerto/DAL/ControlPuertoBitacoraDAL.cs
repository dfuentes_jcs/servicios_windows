﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSControlPuertoWService.DAL
{
    class ControlPuertoBitacoraDAL
    {
        public bool ControlPuertoBitacoraCrear(CONTROL_PUERTO_BITACORA bitacora)
        {
            using (var db = new JCSDBEntities())
            {
                db.CONTROL_PUERTO_BITACORA.Add(bitacora);
                db.SaveChanges();

                return true;
            }
        }
    }
}
