﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSControlPuertoWService.DAL
{
    public class AlmacenDAL
    {
        public List<CONTROL_PUERTO_ALMACEN> ObtenerListaAlmacenes()
        {
            using (var db = new JCSDBEntities())
            {
                var query = from c in db.CONTROL_PUERTO_ALMACEN
                            where c.esta_activo
                            select c;

                return query.ToList();
            }
        }
    }
}
