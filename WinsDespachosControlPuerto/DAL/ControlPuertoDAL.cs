﻿using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using JCSControlPuertoWService.Utiles;
using JCSControlPuertoWService.BO;
using System.Data.SqlClient;
using System.Data;
using System.Data.Entity.SqlServer;
using System.Data.Entity.Core.Objects;

namespace JCSControlPuertoWService.DAL
{
    public class ControlPuertoDAL
    {
        public CONTROL_EJECUTIVO ObtenerControlEjecutivoPorDespacho(int idDespacho)
        {
            using (var db = new JCSDBEntities())
            {
                var query = from p in db.CONTROL_EJECUTIVO.Include(b => b.CONTROL_PUERTO_BITACORA)
                            where p.ID_DESPACHO == idDespacho &&
                            p.ESTADO_REGISTRO
                            select p;

                return query.FirstOrDefault();
            }
        }

        public bool EditarDespachoControlEjecutivo(int idDespacho, int estadoId, string almacen)
        {
            using (var db = new JCSDBEntities())
            {
                var query = from p in db.CONTROL_EJECUTIVO
                            where p.ID_DESPACHO == idDespacho &&
                            p.ESTADO_REGISTRO
                            select p;

                CONTROL_EJECUTIVO controlEjecutivo = query.FirstOrDefault();

                if (controlEjecutivo != null)
                {
                    controlEjecutivo.CONTROL_PUERTO_ESTADO_ID = estadoId;
                    controlEjecutivo.ALMACEN = almacen;
                }

                db.SaveChanges();
                return true;
            }
        }

        public List<int> ObtenerListaDespachosExistentes()
        {
            using (var db = new JCSDBEntities())
            {
                var query = from c in db.CONTROL_EJECUTIVO
                            where c.ESTADO_REGISTRO && !Constantes.ESTADOS_DESPACHOS_EXCLUIR.Contains(c.ESTADO)
                            select c.ID_DESPACHO;

                return query.ToList();
            }
        }

        public List<string> ObtenerListaNaves()
        {
            using (var db = new JCSDBEntities())
            {
                var query = from c in db.NAVE
                            where c.vigente
                            select c.nombre.ToUpper().Trim();

                return query.Distinct().ToList();
            }
        }

        public bool InsertarAdjuntosControlPuerto(List<CONTROL_PUERTO_CORREO_PROCESADO_ADJUNTO> listaAdjuntos)
        {
            using (var db = new JCSDBEntities())
            {
                db.CONTROL_PUERTO_CORREO_PROCESADO_ADJUNTO.AddRange(listaAdjuntos);
                db.SaveChanges();
            }

            return true;
        }

        public bool InsertarDocumentosControlPuerto(List<CONTROL_PUERTO_DOCUMENTO> listaDocumentos)
        {
            using (var db = new JCSDBEntities())
            {
                db.CONTROL_PUERTO_DOCUMENTO.AddRange(listaDocumentos);
                db.SaveChanges();
            }

            return true;
        }

        public bool InsertarContenedoresControlPuerto(List<CONTROL_PUERTO_CONTENEDOR> listaContenedores)
        {
            using (var db = new JCSDBEntities())
            {
                db.CONTROL_PUERTO_CONTENEDOR.AddRange(listaContenedores);
                db.SaveChanges();
            }

            return true;
        }

        public bool InsertarDespachoCorreoProcesadoControlPuerto(CONTROL_PUERTO_CORREO_PROCESADO_DESPACHO despachoCorreoProcesado)
        {
            using (var db = new JCSDBEntities())
            {
                db.CONTROL_PUERTO_CORREO_PROCESADO_DESPACHO.Add(despachoCorreoProcesado);
                db.SaveChanges();
            }

            return true;
        }

        public List<CONTROL_PUERTO_ESTADO> ObtenerEstadosControlPuerto()
        {
            using (var db = new JCSDBEntities())
            {
                var query = from c in db.CONTROL_PUERTO_ESTADO
                            //where c.esta_activo
                            select c;

                return query.ToList();
            }
        }

        public DataSet ObtenerDetalleDespachoSP(int despachoId)
        {
            using (var db = new JCSDBEntities())
            {
                DataSet dsDespacho = new DataSet();
                SqlConnection conn = new SqlConnection(new JCSDBEntities().Database.Connection.ConnectionString);

                try
                {
                    SqlCommand dcmd = new SqlCommand("SP_CONTROL_PUERTO_OBTENER_DETALLE", conn);

                    SqlParameter prms = new SqlParameter("@ID_DESPACHO", SqlDbType.Int);
                    prms.Value = despachoId;
                    dcmd.Parameters.Add(prms);

                    dcmd.CommandType = CommandType.StoredProcedure;

                    conn.Open();
                    DataTable dt = new DataTable();
                    string query = dcmd.ToString();

                    SqlDataAdapter da = new SqlDataAdapter(dcmd);
                    da.Fill(dsDespacho);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }

                return dsDespacho;
            }
        }

        public DataTable ObtenerCorreosPorAduana(int aduanaId)
        {
            using (var db = new JCSDBEntities())
            {
                DataSet dsDespacho = new DataSet();
                SqlConnection conn = new SqlConnection(new JCSDBEntities().Database.Connection.ConnectionString);

                try
                {
                    SqlCommand dcmd = new SqlCommand("SP_CONTROL_PUERTO_OBTENER_DESTINATARIO_ADUANA", conn);

                    SqlParameter prms = new SqlParameter("@ID_ADUANA", SqlDbType.Int);
                    prms.Value = aduanaId;
                    dcmd.Parameters.Add(prms);

                    dcmd.CommandType = CommandType.StoredProcedure;

                    conn.Open();
                    DataTable dt = new DataTable();
                    string query = dcmd.ToString();

                    SqlDataAdapter da = new SqlDataAdapter(dcmd);
                    da.Fill(dsDespacho);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }

                return (dsDespacho != null && dsDespacho.Tables.Count > 0)? dsDespacho.Tables[0] : null;
            }
        }

        public DataTable ObtenerReporteContenedorSP(DateTime fechaDesde, DateTime fechaHasta)
        {
            using (var db = new JCSDBEntities())
            {
                DataSet dsDespacho = new DataSet();
                SqlConnection conn = new SqlConnection(new JCSDBEntities().Database.Connection.ConnectionString);

                try
                {
                    SqlCommand dcmd = new SqlCommand("SP_CONTROL_PUERTO_LISTADO_GENERAL_CONTENEDOR_PAGINADO", conn);

                    SqlParameter prms = new SqlParameter("@EJECUTIVO", SqlDbType.VarChar);
                    prms.Value = string.Empty;
                    dcmd.Parameters.Add(prms);

                    prms = new SqlParameter("@ID_DESPACHO", SqlDbType.Int);
                    prms.Value = 0;
                    dcmd.Parameters.Add(prms);

                    prms = new SqlParameter("@ID_CLIENTE", SqlDbType.Int);
                    prms.Value = 0;
                    dcmd.Parameters.Add(prms);

                    prms = new SqlParameter("@CORR_CLIENTE", SqlDbType.SmallInt);
                    prms.Value = 0;
                    dcmd.Parameters.Add(prms);

                    prms = new SqlParameter("@ID_ESTADO", SqlDbType.Int);
                    prms.Value = 0;
                    dcmd.Parameters.Add(prms);

                    prms = new SqlParameter("@SIN_RETIRO", SqlDbType.Bit);
                    prms.Value = 1;
                    dcmd.Parameters.Add(prms);

                    prms = new SqlParameter("@CODIGO_ADUANA", SqlDbType.VarChar);
                    prms.Value = string.Empty;
                    dcmd.Parameters.Add(prms);

                    prms = new SqlParameter("@TIPO_FECHA", SqlDbType.VarChar);
                    prms.Value = "Pago";
                    dcmd.Parameters.Add(prms);

                    prms = new SqlParameter("@FECHA_INICIO", SqlDbType.DateTime);
                    prms.Value = fechaDesde.ToString("dd-MM-yyyy HH:mm:ss");
                    dcmd.Parameters.Add(prms);

                    prms = new SqlParameter("@FECHA_TERMINO", SqlDbType.DateTime);
                    prms.Value = fechaHasta.ToString("dd-MM-yyyy HH:mm:ss");
                    dcmd.Parameters.Add(prms);

                    prms = new SqlParameter("@SIN_FECHA", SqlDbType.Bit);
                    prms.Value = 0;
                    dcmd.Parameters.Add(prms);

                    prms = new SqlParameter("@REFERENCIA", SqlDbType.VarChar);
                    prms.Value = string.Empty;
                    dcmd.Parameters.Add(prms);

                    prms = new SqlParameter("@SIN_REFERENCIA", SqlDbType.Bit);
                    prms.Value = 0;
                    dcmd.Parameters.Add(prms);

                    prms = new SqlParameter("@NAVE", SqlDbType.VarChar);
                    prms.Value = string.Empty;
                    dcmd.Parameters.Add(prms);

                    prms = new SqlParameter("@SIN_NAVE", SqlDbType.Bit);
                    prms.Value = 0;
                    dcmd.Parameters.Add(prms);

                    prms = new SqlParameter("@PageSize", SqlDbType.Int);
                    prms.Value = int.MaxValue;
                    dcmd.Parameters.Add(prms);

                    prms = new SqlParameter("@PageNumber", SqlDbType.Int);
                    prms.Value = 0;
                    dcmd.Parameters.Add(prms);

                    dcmd.CommandType = CommandType.StoredProcedure;

                    conn.Open();
                    DataTable dt = new DataTable();
                    string query = dcmd.ToString();

                    SqlDataAdapter da = new SqlDataAdapter(dcmd);
                    da.Fill(dsDespacho);
                }
                catch (Exception ex)
                {
                    throw;
                }
                finally
                {
                    conn.Close();
                }

                return (dsDespacho != null && dsDespacho.Tables.Count > 0)? dsDespacho.Tables[0] : null;
            }
        }

        public List<ReporteDespachosBO> ObtenerListaReporteDepachos(List<int> listaEstadosId, string tipoOperacion, DateTime fechaDesde, DateTime fechaHasta)
        {
            using (var db = new JCSDBEntities())
            {
                var query = from c in db.CONTROL_EJECUTIVO
                            join a in db.ADUANA on c.ADUANA equals a.id_sigad
                            where c.ESTADO_REGISTRO && c.CONTROL_PUERTO_ESTADO_ID.HasValue 
                            && listaEstadosId.Contains(c.CONTROL_PUERTO_ESTADO_ID.Value)
                            && !Constantes.ESTADOS_DESPACHOS_EXCLUIR.Contains(c.ESTADO)
                            && c.OPERACION == tipoOperacion
                            && DbFunctions.TruncateTime(c.FECHA_CREACION) >= DbFunctions.TruncateTime(fechaDesde)
                            && DbFunctions.TruncateTime(c.FECHA_CREACION) <= DbFunctions.TruncateTime(fechaHasta)
                            select new ReporteDespachosBO
                            {
                                Despacho                    = c.ID_DESPACHO,
                                Tipo_Operacion              = c.OPERACION,
                                Razon_Social                = c.RAZON_SOCIAL,
                                Referencia                  = (c.REFERENCIA != null)? c.REFERENCIA : string.Empty,
                                Ejecutivo                   = c.EJECUTIVO,
                                Aduana                      = a.nombre,
                                Nave                        = (c.NAVE != null)? c.NAVE : string.Empty,
                                Destino                     = (c.DESTINO != null)? c.DESTINO : string.Empty,
                                FechaETA                    = c.FECHA_ETA,
                                FechaCreacion               = c.FECHA_CREACION.Value,
                                FechaAceptacion             = c.FECHA_ACEPTACION,
                                FechaPago                   = c.FECHA_PAGO,
                                CDA                         = (c.CDA != null)? c.CDA : string.Empty,
                                SAG                         = (c.SAG != null)? c.SAG : string.Empty,
                                //Almacen = (c.ALMACEN != null)? c.ALMACEN : string.Empty,
                                //EstadoId = (c.CONTROL_PUERTO_ESTADO_ID.HasValue)? c.CONTROL_PUERTO_ESTADO_ID.Value : 0,
                                Nombre_Estado               = (c.CONTROL_PUERTO_ESTADO != null)? c.CONTROL_PUERTO_ESTADO.nombre : string.Empty,
                                Tipo_Carga                  = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.esta_activo))? 
                                                            c.CONTROL_PUERTO_SOLICITUD.Where(s => s.esta_activo).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().CONTROL_PUERTO_SOLICITUD_CARGA_TIPO.nombre : string.Empty,
                                Transporte                  = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.esta_activo && !string.IsNullOrEmpty(b.transporte)))? 
                                                            c.CONTROL_PUERTO_SOLICITUD.Where(s => s.esta_activo && !string.IsNullOrEmpty(s.transporte)).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().transporte : string.Empty,
                                FechaNotificacionCarga      = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.NOTIFICACION_CARGA))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.NOTIFICACION_CARGA_INFORMADA).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaSolicitudInscripcion   = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.INSCRIPCION_DIRECTA))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.INSCRIPCION_SOLICITADA).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaInscripcionDirecta     = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.INSCRIPCION_DIRECTA))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.INSCRITO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaSolicitudCambioAlmacen = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.CAMBIO_ALMACEN))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.CAMBIO_ALMACEN_SOLICITADO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaCambioAlmacen          = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.CAMBIO_ALMACEN))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.CAMBIO_ALMACEN_REALIZADO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaSolicitudCanje         = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.CANJE_BL))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.CANJE_SOLICITADO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaCanje                  = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.CANJE_BL))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.CANJEADO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaSolicitudGarantia      = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.GARANTIA_BL))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.GARANTIA_SOLICITADA).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaGarantia               = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.GARANTIA_BL))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.GARANTIZADO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaPapeletaSolicitada     = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.ENVIO_PAPELETA))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.ENVIO_PAPELETA_SOLICTADA).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaPapeletaEnviada        = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.ENVIO_PAPELETA))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.PAPELETA_ENVIADA).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL)
                            };

                return query.OrderBy(f => f.FechaETA).ToList();
            }
        }

        public List<ReporteContenedorBO> ObtenerListaReporteContenedor(List<int> listaEstadosId, string tipoOperacion, DateTime fechaDesde, DateTime fechaHasta)
        {
            using (var db = new JCSDBEntities())
            {
                var query = from cc in db.CONTROL_PUERTO_CONTENEDOR
                            join c in db.CONTROL_EJECUTIVO on cc.id_despacho equals c.ID_DESPACHO
                            //join tb in db.TIPO_BULTO on cc.tipo_bulto_id equals tb.id
                            join a in db.ADUANA on c.ADUANA equals a.id_sigad
                            where cc.esta_activo && c.ESTADO_REGISTRO 
                            && c.CONTROL_PUERTO_ESTADO_ID.HasValue 
                            && listaEstadosId.Contains(c.CONTROL_PUERTO_ESTADO_ID.Value)
                            && !Constantes.ESTADOS_DESPACHOS_EXCLUIR.Contains(c.ESTADO)
                            && c.OPERACION == tipoOperacion
                            && DbFunctions.TruncateTime(c.FECHA_CREACION) >= DbFunctions.TruncateTime(fechaDesde)
                            && DbFunctions.TruncateTime(c.FECHA_CREACION) <= DbFunctions.TruncateTime(fechaHasta)
                            select new ReporteContenedorBO
                            {
                                Despacho                    = c.ID_DESPACHO,
                                Razon_Social                = c.RAZON_SOCIAL,
                                Aduana                      = a.nombre,
                                Nave                        = (c.NAVE != null)? c.NAVE : string.Empty,
                                Fecha_ETA                   = c.FECHA_ETA,
                                SAG                         = (c.SAG != null)? c.SAG : string.Empty,
                                EsImo                       = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.esta_activo))? 
                                                            c.CONTROL_PUERTO_SOLICITUD.Where(s => s.esta_activo).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().carga_imo : false,
                                Codigo_Agente               = (c.AGENTE != null)? c.AGENTE : string.Empty,
                                Tipo_Bulto                  = (cc.tipo_bulto_id.HasValue)? cc.TIPO_BULTO.nombre : string.Empty,
                                Contenedor                  = cc.numero_contenedor,
                                Operador                    = (cc.control_puerto_contenedor_operador_id.HasValue)? cc.CONTROL_PUERTO_CONTENEDOR_OPERADOR.nombre : string.Empty,
                                Deposito                    = (cc.control_puerto_contenedor_deposito_id.HasValue)? cc.CONTROL_PUERTO_CONTENEDOR_DEPOSITO.nombre : string.Empty,
                                Almacen                     = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.esta_activo && b.CONTROL_PUERTO_ALMACEN != null))? 
                                                            c.CONTROL_PUERTO_SOLICITUD.Where(s => s.esta_activo && s.CONTROL_PUERTO_ALMACEN != null).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().CONTROL_PUERTO_ALMACEN.nombre : string.Empty,
                                Tipo_Carga                  = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.esta_activo))? 
                                                            c.CONTROL_PUERTO_SOLICITUD.Where(s => s.esta_activo).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().CONTROL_PUERTO_SOLICITUD_CARGA_TIPO.nombre : string.Empty,
                                Transporte                  = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.esta_activo && !string.IsNullOrEmpty(b.transporte)))? 
                                                            c.CONTROL_PUERTO_SOLICITUD.Where(s => s.esta_activo && !string.IsNullOrEmpty(s.transporte)).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().transporte : string.Empty,
                                Fecha_Retiro_Programada     = (cc.fecha_retiro_programada.HasValue)? cc.fecha_retiro_programada.Value : (DateTime?)null,
                                Fecha_Rastreo_Parcial       = (cc.fecha_retiro.HasValue)? cc.fecha_retiro.Value : (DateTime?)null,
                                Observacion                 = (cc.observacion != null)? cc.observacion : string.Empty,
                                Fecha_Solicitud_Inscripcion_Directa = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.INSCRIPCION_DIRECTA))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.INSCRIPCION_SOLICITADA).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : (DateTime?)null),
                                Fecha_Notificacion_Carga    = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.NOTIFICACION_CARGA))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.NOTIFICACION_CARGA_INFORMADA).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : (DateTime?)null)
                            };

                return query.OrderBy(f => f.Contenedor).ToList();
            }
        }

        public List<ReporteDespachosBO> ObtenerListaReporteSolicitudesSinRealizar(List<int> listaEstadosId, string tipoOperacion)
        {
            using (var db = new JCSDBEntities())
            {
                List<CONTROL_PUERTO_SLA> sla    = ObtenerSla().Where(s => s.control_puerto_bandeja_tipo_id == (int)Enumerables.BandejaTipo.OPERATIVO).ToList();
                List<int> horas                 = sla.Select(x => x.horas_rojo.Value).ToList();
                DateTime fechaLimite            = DateTime.Now.AddHours(horas[0]);

                var query = from c in db.CONTROL_EJECUTIVO
                            join a in db.ADUANA on c.ADUANA equals a.id_sigad
                            where c.ESTADO_REGISTRO && c.CONTROL_PUERTO_ESTADO_ID.HasValue 
                            && listaEstadosId.Contains(c.CONTROL_PUERTO_ESTADO_ID.Value)
                            && !Constantes.ESTADOS_DESPACHOS_EXCLUIR.Contains(c.ESTADO)
                            && c.OPERACION == tipoOperacion
                            && (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.esta_activo && b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => 
                                !t.realizado && t.esta_activo
                                && t.CONTROL_PUERTO_CORREO_TIPO.visible_operativo 
                                && (!c.FECHA_ETA.HasValue || c.FECHA_ETA.HasValue && c.FECHA_ETA.Value <= fechaLimite)
                                //&& c.FECHA_ETA.Value < DbFunctions.AddHours(DateTime.Now, (sla.Where(s => s.control_puerto_correo_tipo_id == t.control_puerto_correo_tipo_id).Select(s => s.horas_rojo.Value).FirstOrDefault()))
                                //&& c.FECHA_ETA.Value < EntityFunctions.AddHours(DateTime.Now, horas[t.control_puerto_correo_tipo_id - 1])
                                )))
                            select new ReporteDespachosBO
                            {
                                Despacho                    = c.ID_DESPACHO,
                                Tipo_Operacion              = c.OPERACION,
                                Razon_Social                = c.RAZON_SOCIAL,
                                Referencia                  = (c.REFERENCIA != null)? c.REFERENCIA : string.Empty,
                                Ejecutivo                   = c.EJECUTIVO,
                                Aduana                      = a.nombre,
                                AduanaId                    = a.id,
                                Nave                        = (c.NAVE != null)? c.NAVE : string.Empty,
                                Destino                     = (c.DESTINO != null)? c.DESTINO : string.Empty,
                                FechaETA                    = c.FECHA_ETA,
                                FechaCreacion               = c.FECHA_CREACION.Value,
                                FechaAceptacion             = c.FECHA_ACEPTACION,
                                FechaPago                   = c.FECHA_PAGO,
                                CDA                         = (c.CDA != null)? c.CDA : string.Empty,
                                SAG                         = (c.SAG != null)? c.SAG : string.Empty,
                                Almacen                     = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.esta_activo && b.CONTROL_PUERTO_ALMACEN != null))? 
                                                            c.CONTROL_PUERTO_SOLICITUD.Where(s => s.esta_activo && s.CONTROL_PUERTO_ALMACEN != null).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().CONTROL_PUERTO_ALMACEN.nombre : string.Empty,
                                Cantidad_Bultos_DIN         = (c.CANTIDAD_BULTOS.HasValue)? c.CANTIDAD_BULTOS.Value : 0,
                                Cantidad_Contenedores       = (c.CONTROL_PUERTO_CONTENEDOR.Any())? c.CONTROL_PUERTO_CONTENEDOR.Where(d => d.esta_activo).Count() : 0,
                                Nombre_Estado               = (c.CONTROL_PUERTO_ESTADO != null)? c.CONTROL_PUERTO_ESTADO.nombre : string.Empty,
                                Tipo_Carga                  = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.esta_activo))? 
                                                            c.CONTROL_PUERTO_SOLICITUD.Where(s => s.esta_activo).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().CONTROL_PUERTO_SOLICITUD_CARGA_TIPO.nombre : string.Empty,
                                Transporte                  = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.esta_activo && !string.IsNullOrEmpty(b.transporte)))? 
                                                            c.CONTROL_PUERTO_SOLICITUD.Where(s => s.esta_activo && !string.IsNullOrEmpty(s.transporte)).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().transporte : string.Empty,
                                FechaNotificacionCarga      = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.NOTIFICACION_CARGA))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.NOTIFICACION_CARGA_INFORMADA).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaSolicitudInscripcion   = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.INSCRIPCION_DIRECTA))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.INSCRIPCION_SOLICITADA).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaInscripcionDirecta     = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.INSCRIPCION_DIRECTA))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.INSCRITO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaSolicitudCambioAlmacen = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.CAMBIO_ALMACEN))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.CAMBIO_ALMACEN_SOLICITADO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaCambioAlmacen          = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.CAMBIO_ALMACEN))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.CAMBIO_ALMACEN_REALIZADO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaSolicitudCanje         = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.CANJE_BL))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.CANJE_SOLICITADO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaCanje                  = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.CANJE_BL))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.CANJEADO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaSolicitudGarantia      = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.GARANTIA_BL))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.GARANTIA_SOLICITADA).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaGarantia               = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.GARANTIA_BL))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.GARANTIZADO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaPapeletaSolicitada     = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.ENVIO_PAPELETA))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.ENVIO_PAPELETA_SOLICTADA).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaPapeletaEnviada        = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.ENVIO_PAPELETA))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.PAPELETA_ENVIADA).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL)
                            };

                return query.OrderBy(f => f.FechaETA).ToList();
            }
        }

        public List<ReporteDespachosBO> ObtenerListaReporteDepachosSinSolicitar(List<int> listaEstadosId, string tipoOperacion, List<string> listaAduanasExcluidasId)
        {
            using (var db = new JCSDBEntities())
            {
                CONTROL_PUERTO_SLA sla  = ObtenerSla()
                                        .Where(s => s.control_puerto_bandeja_tipo_id == (int)Enumerables.BandejaTipo.EJECUTIVO && 
                                        s.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.INSCRIPCION_DIRECTA)
                                        .FirstOrDefault();

                int horas               = sla.horas_rojo.Value;
                DateTime fechaLimite    = DateTime.Now.AddHours(horas);

                var query = from c in db.CONTROL_EJECUTIVO
                            join a in db.ADUANA on c.ADUANA equals a.id_sigad
                            where c.ESTADO_REGISTRO && c.CONTROL_PUERTO_ESTADO_ID.HasValue 
                            && listaEstadosId.Contains(c.CONTROL_PUERTO_ESTADO_ID.Value)
                            && !Constantes.ESTADOS_DESPACHOS_EXCLUIR.Contains(c.ESTADO)
                            && c.OPERACION == tipoOperacion
                            && !listaAduanasExcluidasId.Contains(a.id_sigad)
                            && !c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(
                                t => t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.INSCRIPCION_DIRECTA || 
                                t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.NOTIFICACION_CARGA))
                            && c.FECHA_CREACION >= fechaLimite
                            select new ReporteDespachosBO
                            {
                                Despacho                    = c.ID_DESPACHO,
                                Tipo_Operacion              = c.OPERACION,
                                Razon_Social                = c.RAZON_SOCIAL,
                                Referencia                  = (c.REFERENCIA != null)? c.REFERENCIA : string.Empty,
                                Ejecutivo                   = c.EJECUTIVO,
                                Aduana                      = a.nombre,
                                Nave                        = (c.NAVE != null)? c.NAVE : string.Empty,
                                Destino                     = (c.DESTINO != null)? c.DESTINO : string.Empty,
                                FechaETA                    = c.FECHA_ETA,
                                FechaCreacion               = c.FECHA_CREACION.Value,
                                FechaAceptacion             = c.FECHA_ACEPTACION,
                                FechaPago                   = c.FECHA_PAGO,
                                CDA                         = (c.CDA != null)? c.CDA : string.Empty,
                                SAG                         = (c.SAG != null)? c.SAG : string.Empty,
                                Almacen                     = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.esta_activo && b.CONTROL_PUERTO_ALMACEN != null))? 
                                                            c.CONTROL_PUERTO_SOLICITUD.Where(s => s.esta_activo && s.CONTROL_PUERTO_ALMACEN != null).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().CONTROL_PUERTO_ALMACEN.nombre : string.Empty,
                                //Cantidad_Bultos_DIN         = (c.CANTIDAD_BULTOS.HasValue)? c.CANTIDAD_BULTOS.Value : 0,
                                //Cantidad_Contenedores       = (c.CONTROL_PUERTO_CONTENEDOR != null)? c.CONTROL_PUERTO_CONTENEDOR.Where(d => d.esta_activo).Count() : 0,
                                Nombre_Estado               = (c.CONTROL_PUERTO_ESTADO != null)? c.CONTROL_PUERTO_ESTADO.nombre : string.Empty,
                                Tipo_Carga                  = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.esta_activo))? 
                                                            c.CONTROL_PUERTO_SOLICITUD.Where(s => s.esta_activo).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().CONTROL_PUERTO_SOLICITUD_CARGA_TIPO.nombre : string.Empty,
                                Transporte                  = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.esta_activo && !string.IsNullOrEmpty(b.transporte)))? 
                                                            c.CONTROL_PUERTO_SOLICITUD.Where(s => s.esta_activo && !string.IsNullOrEmpty(s.transporte)).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().transporte : string.Empty,
                                FechaNotificacionCarga      = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.NOTIFICACION_CARGA))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.NOTIFICACION_CARGA_INFORMADA).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaSolicitudInscripcion   = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.INSCRIPCION_DIRECTA))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.INSCRIPCION_SOLICITADA).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaInscripcionDirecta     = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.INSCRIPCION_DIRECTA))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.INSCRITO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaSolicitudCambioAlmacen = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.CAMBIO_ALMACEN))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.CAMBIO_ALMACEN_SOLICITADO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaCambioAlmacen          = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.CAMBIO_ALMACEN))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.CAMBIO_ALMACEN_REALIZADO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaSolicitudCanje         = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.CANJE_BL))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.CANJE_SOLICITADO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaCanje                  = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.CANJE_BL))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.CANJEADO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaSolicitudGarantia      = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.GARANTIA_BL))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.GARANTIA_SOLICITADA).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaGarantia               = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.GARANTIA_BL))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.GARANTIZADO).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaPapeletaSolicitada     = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.ENVIO_PAPELETA))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.ENVIO_PAPELETA_SOLICTADA).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL),
                                FechaPapeletaEnviada        = (c.CONTROL_PUERTO_SOLICITUD.Any(b => b.CONTROL_PUERTO_SOLICITUD_TIPO_CORREO.Any(t => t.esta_activo && t.control_puerto_correo_tipo_id == (int)Enumerables.TipoCorreo.ENVIO_PAPELETA))? 
                                                            c.CONTROL_PUERTO_BITACORA.Where(b => b.control_puerto_estado_id == (int)Enumerables.ControlPuertoEstado.PAPELETA_ENVIADA).OrderByDescending(o => o.fecha_creacion).FirstOrDefault().fecha_creacion : Constantes.FECHA_NULL)
                            };

                return query.OrderBy(f => f.FechaETA).ToList();
            }
        }

        public List<CONTROL_PUERTO_CORREO_TIPO> ObtenerListaTipoCorreo()
        {
            using (var db = new JCSDBEntities())
            {
                var query = from c in db.CONTROL_PUERTO_CORREO_TIPO
                            where c.esta_activo
                            select c;

                return query.ToList();
            }
        }

        public List<CONTROL_PUERTO_SLA> ObtenerSla()
        {
            using (var db = new JCSDBEntities())
            {
                var query = from c in db.CONTROL_PUERTO_SLA
                            where c.esta_activo
                            select c;

                return query.ToList();
            }
        }

        public List<CONTROL_PUERTO_USUARIO_CORREO_TIPO> ObtenerUsuariosOperativos()
        {
            using (var db = new JCSDBEntities())
            {
                var query = from c in db.CONTROL_PUERTO_USUARIO_CORREO_TIPO.Include(u => u.USUARIO)
                            where c.USUARIO.desvinculado == false && c.USUARIO.vigente &&
                            c.id_aduana.HasValue
                            select c;

                return query.ToList();
            }
        }

        public List<ADUANA> ObtenerAduanas()
        {
            using (var db = new JCSDBEntities())
            {
                var query = from c in db.ADUANA
                            select c;

                return query.ToList();
            }
        }

        public List<USUARIO> ObtenerListaUsuarios()
        {
            using (var db = new JCSDBEntities())
            {
                var query = from c in db.USUARIO
                            where c.vigente && c.desvinculado == false
                            select c;

                return query.ToList();
            }
        }

        public List<CONTROL_PUERTO_REPORTE_DESTINATARIO> ObtenerDestinatariosPorReporteYAduana(int aduanaId, int ReporteId)
        {
            using (var db = new JCSDBEntities())
            {
                var query = from c in db.CONTROL_PUERTO_REPORTE_DESTINATARIO.Include(u => u.USUARIO)
                            where c.esta_activo && c.aduana_id == aduanaId &&
                            c.control_puerto_reporte_id == ReporteId
                            select c;

                return query.ToList();
            }
        }
    }
}
