//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace JCSControlPuertoWService
{
    using System;
    using System.Collections.Generic;
    
    public partial class CONTROL_PUERTO_CONTENEDOR
    {
        public int control_puerto_contenedor_id { get; set; }
        public int usuario_creacion_id { get; set; }
        public int id_despacho { get; set; }
        public string numero_contenedor { get; set; }
        public System.DateTime fecha_creacion { get; set; }
        public bool esta_activo { get; set; }
        public Nullable<System.DateTime> fecha_retiro { get; set; }
        public Nullable<int> usuario_retiro_id { get; set; }
        public Nullable<int> control_puerto_contenedor_operador_id { get; set; }
        public Nullable<int> tipo_bulto_id { get; set; }
        public string observacion { get; set; }
        public Nullable<int> usuario_modificacion_id { get; set; }
        public Nullable<System.DateTime> fecha_modificacion { get; set; }
        public Nullable<System.DateTime> fecha_retiro_programada { get; set; }
        public Nullable<int> control_puerto_contenedor_deposito_id { get; set; }
    
        public virtual CONTROL_EJECUTIVO CONTROL_EJECUTIVO { get; set; }
        public virtual USUARIO USUARIO { get; set; }
        public virtual USUARIO USUARIO1 { get; set; }
        public virtual USUARIO USUARIO11 { get; set; }
        public virtual CONTROL_PUERTO_CONTENEDOR_OPERADOR CONTROL_PUERTO_CONTENEDOR_OPERADOR { get; set; }
        public virtual TIPO_BULTO TIPO_BULTO { get; set; }
        public virtual CONTROL_PUERTO_CONTENEDOR_DEPOSITO CONTROL_PUERTO_CONTENEDOR_DEPOSITO { get; set; }
    }
}
