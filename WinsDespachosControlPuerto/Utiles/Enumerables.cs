﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSControlPuertoWService.Utiles
{
    public class Enumerables
    {
        public enum ControlPuertoEstado : int
        {
            APERTURADO_COMPLETO = 1,
            ENVIADO_PARA_CONFIRMAR,
            CONFIRMADO,
            INSCRIPCION_SOLICITADA,
            INSCRITO,
            RETIRADO,
            APERTURADO_INCOMPLETO,
            CAMBIO_ALMACEN_SOLICITADO,
            CAMBIO_ALMACEN_REALIZADO,
            GARANTIA_SOLICITADA,
            GARANTIZADO,
            CANJE_SOLICITADO,
            CANJEADO,
            DEVUELTO_EJECUTIVO,
            RETIRADO_PARCIAL,
            NOTIFICACION_CARGA_INFORMADA,
            ENVIO_PAPELETA_SOLICTADA,
            PAPELETA_ENVIADA,
            NOTIFICACION_RECIBIDA
        }

        public enum TipoCorreo : int
        {
            INSCRIPCION_DIRECTA = 1,
            CAMBIO_ALMACEN,
            CANJE_BL,
            GARANTIA_BL,
            NOTIFICACION_CARGA,
            ENVIO_PAPELETA
        }

        public enum BandejaTipo : int
        {
            EJECUTIVO = 1,
            ENCARGADO,
            OPERATIVO
        }

        public enum Reportes : int
        {
            DESPACHOS_SIN_SOLICITAR = 1,
            OPERACIONES_SIN_REALIZAR = 2
        }
    }
}
