﻿using JCSControlPuertoWService.BLL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSControlPuertoWService.Utiles
{
    public class MailTo
    {
        public static Boolean ReporteDespachos(string pathReporte)
        {
            VariableConfiguracionBLL configBLL  = new VariableConfiguracionBLL();
            string asunto                       = "Reporte de despachos Control Puerto";
            string mensaje                      = string.Format(@"Buenos tardes<br><br>
                                                Se adjunta reporte con el detalle de despachos
                                                <br>
                                                <br>
                                                Les saluda atte.<br>
                                                Departamento de Informática<br>
                                                Agencia de Aduana Juan Carlos Stephens");

            try
            {
                string para = string.Empty;
                string copia = string.Empty;

                if (Constantes.ModoDesarrollo)
                {
                    para = configBLL.ObtenerVariablePorNombre("control.puerto.email.desarrollo");
                    copia = configBLL.ObtenerVariablePorNombre("control.puerto.email.desarrollo");
                }
                else
                {
                    para += configBLL.ObtenerVariablePorNombre("control.puerto.email.reporte.para");
                    copia += configBLL.ObtenerVariablePorNombre("control.puerto.email.reporte.cc");
                }

                string respuesta = new Email.MailClient().EnviarEmail(asunto,
                                    mensaje,
                                    configBLL.ObtenerVariablePorNombre("control.puerto.email.reporte.desde"),
                                    para,
                                    copia,
                                    configBLL.ObtenerVariablePorNombre("control.puerto.email.reporte.cco"),
                                    pathReporte + ";",
                                    true,
                                    "NORMAL",
                                    true,
                                    "WINS CONTROL PUERTO: REPORTE DESPACHOS");

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static Boolean ReporteDespachosSinSolicitar(string pathReporte, string correoEjecutivo, string nombreEjecutivo)
        {
            VariableConfiguracionBLL configBLL  = new VariableConfiguracionBLL();
            string asunto                       = "Reporte sin solicitar inscripción de retiro directo " + nombreEjecutivo;
            string mensaje                      = string.Format(@"Buenos dias<br><br>
                                                Se adjunta reporte con el detalle de operaciones sin solicitar inscripción de retiro directo
                                                <br>
                                                <br>
                                                Les saluda atte.<br>
                                                Departamento de Informática<br>
                                                Agencia de Aduana Juan Carlos Stephens");

            try
            {
                string para = string.Empty;
                string copia = string.Empty;

                if (Constantes.ModoDesarrollo)
                {
                    para = configBLL.ObtenerVariablePorNombre("control.puerto.email.desarrollo");
                }
                else
                {
                    para = correoEjecutivo;
                }

                string respuesta = new Email.MailClient().EnviarEmailSinCC(asunto,
                                    mensaje,
                                    configBLL.ObtenerVariablePorNombre("control.puerto.email.reporte.desde"),
                                    para,
                                    configBLL.ObtenerVariablePorNombre("control.puerto.email.reporte.sin.solicitar.cco"),
                                    pathReporte + ";",
                                    true,
                                    "NORMAL",
                                    true,
                                    "WINS CONTROL PUERTO: REPORTE DESPACHOS SIN SOLICITAR");

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static Boolean ReporteSolicitudesSinRealizar(string pathReporte, List<string> listaCorreosOperativos, List<string> listaCorreosCopia, string nombreAduana)
        {
            VariableConfiguracionBLL configBLL  = new VariableConfiguracionBLL();
            string asunto                       = "Reporte de operaciones sin realizar " + nombreAduana;
            string mensaje                      = string.Format(@"Buenos dias<br><br>
                                                Se adjunta reporte con el detalle de operaciones sin realizar y con su nave pronta de arribar
                                                <br>
                                                <br>
                                                Les saluda atte.<br>
                                                Departamento de Informática<br>
                                                Agencia de Aduana Juan Carlos Stephens");

            try
            {
                string para = string.Empty;
                string copia = string.Empty;

                if (Constantes.ModoDesarrollo)
                {
                    para = configBLL.ObtenerVariablePorNombre("control.puerto.email.desarrollo");
                    copia = configBLL.ObtenerVariablePorNombre("control.puerto.email.desarrollo");
                }
                else
                {
                    foreach(string email in listaCorreosOperativos)
                    {
                        para += (para != string.Empty)? string.Format(";{0}", email) : email;
                    }

                    foreach(string email in listaCorreosCopia)
                    {
                        copia += (copia != string.Empty)? string.Format(";{0}", email) : email;
                    }

                    copia += configBLL.ObtenerVariablePorNombre("control.puerto.email.reporte.sin.realizar.cc");
                }

                string respuesta = new Email.MailClient().EnviarEmail(asunto,
                                    mensaje,
                                    configBLL.ObtenerVariablePorNombre("control.puerto.email.reporte.desde"),
                                    para,
                                    copia,
                                    configBLL.ObtenerVariablePorNombre("control.puerto.email.reporte.sin.realizar.cco"),
                                    pathReporte + ";",
                                    true,
                                    "NORMAL",
                                    true,
                                    "WINS CONTROL PUERTO: REPORTE OPERACIONES SIN REALIZAR");

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool EnviarCorreoControlPuerto(int despachoId, int estadoId, bool enviarCorreoPorAduana, string almacen_viejo, CONTROL_PUERTO_CORREO_TIPO tipoCorreo)
        {
            UsuarioBLL usuarioBLL               = new UsuarioBLL();
            ControlPuertoBLL controlPuertoBLL   = new ControlPuertoBLL();
            VariableConfiguracionBLL configBLL  = new VariableConfiguracionBLL();
            string nombreEstado                 = string.Empty;
            string asunto                       = string.Empty;

            DataSet dsDetalle                           = controlPuertoBLL.ObtenerDetalleControlPuertoDespacho(despachoId);
            DataTable dtDetalle                         = dsDetalle.Tables[0];
            List<CONTROL_PUERTO_ESTADO> listaEstados    = controlPuertoBLL.ObtenerEstadosControlPuerto();

            nombreEstado = listaEstados.Where(e => e.control_puerto_estado_id == estadoId).FirstOrDefault().nombre;

            DateTime  hoy                       = DateTime.Now;

            string almacen                      = (dtDetalle.Rows[0]["ALMACEN"] != DBNull.Value && !string.IsNullOrEmpty(dtDetalle.Rows[0]["ALMACEN"].ToString()))? "<b>Almacén: </b>" + dtDetalle.Rows[0]["ALMACEN"].ToString() + "<br>" : string.Empty;
            string sag                          = (dtDetalle.Rows[0]["SAG"] != DBNull.Value && !string.IsNullOrEmpty(dtDetalle.Rows[0]["SAG"].ToString()))? "<b>SAG: </b>" + dtDetalle.Rows[0]["SAG"].ToString() + "<br>" : string.Empty;
            string cda                          = (dtDetalle.Rows[0]["CDA"] != DBNull.Value && !string.IsNullOrEmpty(dtDetalle.Rows[0]["CDA"].ToString()))? "<b>CDA: </b>" + dtDetalle.Rows[0]["CDA"].ToString() + "<br>" : string.Empty;
            string fecha_eta                    = (dtDetalle.Rows[0]["FECHA_ETA"] != null && dtDetalle.Rows[0]["FECHA_ETA"].ToString() != "01-01-1900 0:00:00")? 
                                                Convert.ToDateTime(dtDetalle.Rows[0]["FECHA_ETA"].ToString()).ToString("dd/MM/yyyy").Replace("-","/") : "";

            if (estadoId == (int)Enumerables.ControlPuertoEstado.CAMBIO_ALMACEN_SOLICITADO)
            {
                asunto  = "AVISO: D-" + despachoId + " ha sido SOLICITADO EL CAMBIO DE ALMACÉN";
            }
            else
            {
                asunto  = "AVISO: D-" + despachoId + " ha sido SOLICITADA SU INSCRIPCIÓN";
            }
            
            string body                         = "<p>Estimado(a):</p>"
                                                + "<p>Informamos que se ha registrado en el sistema una modificación a un despacho. A continuación los detalles:</p>"
                                                + "<b>Despacho: </b>" + dtDetalle.Rows[0]["ID_DESPACHO"].ToString() + "<br>"
                                                + "<b>Cliente: </b>" + dtDetalle.Rows[0]["RAZON_SOCIAL"].ToString() + "<br>"
                                                + "<b>Referencia: </b>" + dtDetalle.Rows[0]["REFERENCIA"].ToString() + "<br>"
                                                + "<b>Destino: </b>" + dtDetalle.Rows[0]["DESTINO"].ToString() + "<br>"
                                                + "<b>Nave: </b>" + dtDetalle.Rows[0]["NAVE"].ToString() + "<br>"
                                                + "<b>Aduana: </b>" + dtDetalle.Rows[0]["ADUANA"].ToString() + "<br>"
                                                + "<b>Fecha ETA: </b>" + fecha_eta + "<br>"
                                                + sag
                                                + cda
                                                + almacen
                                                + ((!string.IsNullOrEmpty(almacen_viejo))? "<b>Almacén Antiguo: </b>" + almacen_viejo + "<br>" : string.Empty)
                                                + "<b>Ejecutivo: </b>" + dtDetalle.Rows[0]["NOMBRE_EJECUTIVO"].ToString() + "<br>"
                                                + "<b>Fecha: </b>" + hoy.ToString("dd/MM/yyyy") + "<br>"
                                                + "<b>Hora: </b>" + hoy.ToShortTimeString() + " hrs.<br>"
                                                + "<b>Estado: </b>" + nombreEstado + "<br><br>"
                                                + "<p>Atte.</p>"
                                                + "<p>Informática JCS</p>";

            int idUsuarioEncargado              = 0;
            int idUsuarioEjecutivo              = 0;

            int.TryParse(dtDetalle.Rows[0]["USUARIO_ENCARGADO_ID"].ToString(), out idUsuarioEncargado);
            int.TryParse(dtDetalle.Rows[0]["USUARIO_EJECUTIVO_ID"].ToString(), out idUsuarioEjecutivo);

            string emailEncargado              = (idUsuarioEncargado > 0)? usuarioBLL.ObtenerEmailUsuario(idUsuarioEncargado) : string.Empty;
            string emailEjecutivo              = (idUsuarioEjecutivo > 0)? usuarioBLL.ObtenerEmailUsuario(idUsuarioEjecutivo) : string.Empty;

            string para = string.Empty;
            string copia = string.Empty;
            string cc = string.Empty;

            if (Constantes.ModoDesarrollo)
            {
                para = configBLL.ObtenerVariablePorNombre("control.puerto.email.desarrollo");
                copia = configBLL.ObtenerVariablePorNombre("control.puerto.email.desarrollo");
            }
            else
            {
                para += emailEncargado;
                copia += emailEjecutivo;

                if (enviarCorreoPorAduana)
                {
                    int aduanaId = int.Parse(dtDetalle.Rows[0]["ADUANA_ID"].ToString());
                    List<string> listaCorreosDestinatarios = controlPuertoBLL.ObtenerCorreosControlPuertoPorAduana(aduanaId);

                    if (listaCorreosDestinatarios.Count > 0)
                    {
                        foreach(string email in listaCorreosDestinatarios)
                        {
                           copia += (copia != string.Empty)? string.Format(";{0}", email) : email;
                        }
                    }
                }

                List<string> listaParaTipoCorreo = (!string.IsNullOrEmpty(tipoCorreo.para))? tipoCorreo.para.Split(';').ToList() : new List<string>(); 
                if (listaParaTipoCorreo.Count > 0)
                {
                    foreach(string email in listaParaTipoCorreo)
                    {
                        para += (para != string.Empty)? string.Format(";{0}", email) : email;
                    }
                }

                List<string> listaCopiaTipoCorreo = (!string.IsNullOrEmpty(tipoCorreo.copia))? tipoCorreo.copia.Split(';').ToList() : new List<string>(); 
                if (listaCopiaTipoCorreo.Count > 0)
                {
                    foreach(string email in listaCopiaTipoCorreo)
                    {
                        copia += (copia != string.Empty)? string.Format(";{0}", email) : email;
                    }
                }
            }

            try
            {
                string respuesta    = new Email.MailClient().EnviarEmail(
                                    asunto,
                                    body,
                                    tipoCorreo.desde,
                                    para,
                                    copia,
                                    tipoCorreo.copia_oculta,
                                    string.Empty,
                                    true,
                                    "NORMAL",
                                    true,
                                    "WINS CONTROL PUERTO: " + tipoCorreo.nombre);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static Boolean ReporteDespachosSinSolicitarResumen
        (
            string rutaReporte
        )
        {
            VariableConfiguracionBLL configBLL  = new VariableConfiguracionBLL();
            string asunto                       = "Reporte despachos por ejecutivo sin solicitar Inscripción de retiro Directo " + DateTime.Now.ToString("dd/MM/yyyy");
            string mensaje                      = string.Format(@"Buenos dias
                                                <br>
                                                <br>
                                                Adjunto se envía reporte con detalle de importaciones sin solicitar inscripción de retiro directo ó notificación de carga.
                                                <br>
                                                <br>
                                                Muchas gracias
                                                <br>
                                                <br>
                                                Les saluda atte.<br>
                                                Departamento de Informática<br>
                                                Agencia de Aduana Juan Carlos Stephens");
            try
            {
                string respuesta    = new Email.MailClient().EnviarEmail(asunto,
                                    mensaje.ToString(),
                                    configBLL.ObtenerVariablePorNombre("control.puerto.email.reporte.desde"),
                                    configBLL.ObtenerVariablePorNombre("control.puerto.email.reporte.sin.solicitar.resumen.para"),
                                    configBLL.ObtenerVariablePorNombre("control.puerto.email.reporte.sin.solicitar.resumen.cc"),
                                    configBLL.ObtenerVariablePorNombre("control.puerto.email.reporte.sin.solicitar.resumen.cco"),
                                    rutaReporte,
                                    true,
                                    "NORMAL",
                                    true,
                                    "WINS CONTROL PUERTO: REPORTE RESUMEN DESPACHOS SIN SOLICITAR");
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public static Boolean aviso_error(Exception ex, string nombreEjecutivo)
        {
            string mensaje_corto = ex.Message;
            string mensaje_largo = ex.ToString();
            string mensaje_causa = Convert.ToString(ex.InnerException);
            string source = ex.Source;
            string targetsite = Convert.ToString(ex.TargetSite);
            string traza = ex.StackTrace;

            StringBuilder estilo = new StringBuilder();

            estilo.Append("<style type=\"text/css\">.grislinea {background-color: #C1C1C1;}");
            estilo.Append(".titulotablaleft {font-family: Arial, Helvetica, sans-serif;font-size: 11px;font-weight: bold;color: #4975A0;text-decoration: none;background-color: #D9E3ED;text-align: left;vertical-align: middle;height: 20px;padding-left: 10px;}");
            estilo.Append(".celdamantenedor {font-family: Arial, Helvetica, sans-serif;font-size: 11px;font-weight: normal;color: #000000;text-decoration: none;background-color: #EFEFEF;height: 25px;text-align: left;vertical-align: middle;padding-left: 10px;}");
            estilo.Append(".celda1mantenedorleft {font-family: Arial, Helvetica, sans-serif;font-size: 11px;font-weight: normal;color: #000000;	text-decoration: none;background-color: #FFFFFF;height: 25px;text-align: left;vertical-align: middle;padding-left: 10px;}");
            estilo.Append("table {border-collapse:collapse};td {border:1px solid #C1C1C1};");
            estilo.Append("</style>");

            StringBuilder mensaje = new StringBuilder();
            mensaje.Append("<head>" + estilo + "</head>");
            mensaje.Append("<h4>Sr Administrador:</h4></br>");

            mensaje.Append("<p>Se ha generado un log de error con fecha " + DateTime.Now.ToString("dd/MM/yyyy HH:mm") + ".</p>");
            mensaje.Append("<p> A continuación se presenta un detalle del log de error: </p>");
            mensaje.Append("<table width=\"80%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" align=\"center\">");
            mensaje.Append("<tr><td class=\"grislinea\"><table width=\"100%\" border=\"1\" cellspacing=\"1\" cellpadding=\"0\" bordercolor=\"#C1C1C1\">");
            mensaje.Append("<tr><td colspan=\"2\" class=\"titulotablaleft\">Encabezado</td></tr>");

            mensaje.Append("<tr><td class=\"celdamantenedor\" width=\"10%\">Causa:</td>");
            mensaje.Append("<td class=\"celda1mantenedorleft\" width=\"20%\">" + mensaje_causa + "</td></tr>");
            mensaje.Append("<tr><td class=\"celdamantenedor\" width=\"10%\">Mensaje:</td>");
            mensaje.Append("<td class=\"celda1mantenedorleft\" width=\"20%\">" + mensaje_corto + "</td></tr>");
            mensaje.Append("<tr><td class=\"celdamantenedor\" width=\"10%\">Mensaje Completo:</td>");
            mensaje.Append("<td class=\"celda1mantenedorleft\" width=\"20%\">" + mensaje_largo + "</td></tr>");

            if (traza != string.Empty)
            {
                mensaje.Append("<tr><td class=\"celdamantenedor\" >Traza:</td>");
                mensaje.Append("<td class=\"celda1mantenedorleft\" >" + traza.Replace(",", "</BR>") + "</td></tr>");
            }

            mensaje.Append("</table></td></tr></table></br>");
            mensaje.Append("<p>Atte.</p></br>");
            mensaje.Append("Departamento de Informática<br>");
            mensaje.Append("Agencia de Aduana Juan Carlos Stephens");

            string asunto = string.Format("{0} {1} {2}", "Error WINS CONTROL PUERTO: ", nombreEjecutivo, DateTime.Now.ToString("dd/MM/yyyy"));

            try
            {
                VariableConfiguracionBLL variableGlobalBLL = new VariableConfiguracionBLL();
                string respuesta = new Email.MailClient().EnviarEmailSinCC(asunto,
                                    mensaje.ToString(),
                                    variableGlobalBLL.ObtenerVariablePorNombre("control.puerto.email.error.desde"),
                                    variableGlobalBLL.ObtenerVariablePorNombre("control.puerto.email.error.para"),
                                    variableGlobalBLL.ObtenerVariablePorNombre("control.puerto.email.error.cco"),
                                    string.Empty,
                                    true,
                                    "NORMAL",
                                    true,
                                    "WINS CONTROL PUERTO: ERROR");
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
