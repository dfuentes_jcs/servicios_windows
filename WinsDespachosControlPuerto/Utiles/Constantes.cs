﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JCSControlPuertoWService.Utiles
{
    public static class Constantes
    {
        public const bool ModoDesarrollo = true;
        public const int USUARIO_TAREA_PROGRAMADA_ID = 637;   
        public const string TIPO_OPERACION_IMPO = "IMPO";
        public const string TIPO_OPERACION_EXPO = "EXPO";
        public static DateTime FECHA_NULL = new DateTime(1900, 01, 01, 00, 00, 00);

        public static List<string> ESTADOS_DESPACHOS_EXCLUIR = new List<string> { "ANULADO" };

        public const string RESPUESTA_OK = "OK";
        public const string RESPUETA_ERROR = "ERROR";
    }
}
