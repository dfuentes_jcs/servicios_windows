﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using Quartz;
using Quartz.Impl;
using System.Configuration;

using JCSControlPuertoWService.ServicioWindow;

namespace JCSControlPuertoWService
{
    public partial class WService : ServiceBase
    {
        private IScheduler sched;

        public WService()
        {
            InitializeComponent();
        }

        public void StartFromDebugger(string[] args)
        {
            OnStart(args);
        }

        protected override void OnStart(string[] args)
        {
            StdSchedulerFactory sf = new StdSchedulerFactory();
            sched = sf.GetScheduler();

            #region CORREO INSCRIPCION

            //string tiempoEjecucionProcesoSolicitudCorreo = ConfigurationManager.AppSettings["tiempoEjecucionProcesoSolicitudCorreo"].ToString();
            //IJobDetail job1         = JobBuilder.Create<ProcesoSolicitudCorreo>()
            //                        .WithIdentity("job1", "group1")
            //                        .Build();
            //ICronTrigger trigger    = (ICronTrigger)TriggerBuilder.Create()
            //                        .WithIdentity("trigger1", "group1")
            //                        .WithCronSchedule(tiempoEjecucionProcesoSolicitudCorreo)
            //                        .Build();
            //sched.ScheduleJob(job1, trigger);
            #endregion

            #region REPORTE DESPACHOS

            string tiempoEjecucionReporteDespachos = ConfigurationManager.AppSettings["tiempoEjecucionReporteDespachos"].ToString();
            IJobDetail job2         = JobBuilder.Create<ProcesoReporteDespachos>()
                                    .WithIdentity("job2", "group2")
                                    .Build();
            ICronTrigger trigger2   = (ICronTrigger)TriggerBuilder.Create()
                                    .WithIdentity("trigger2", "group2")
                                    .WithCronSchedule(tiempoEjecucionReporteDespachos)
                                    .Build();
            sched.ScheduleJob(job2, trigger2);
            #endregion

            #region REPORTE ALERTA DESPACHOS SIN SOLICITAR

            //string tiempoEjecucionReporteSinSolicitar = ConfigurationManager.AppSettings["tiempoEjecucionReporteSinSolicitar"].ToString();
            //IJobDetail job3         = JobBuilder.Create<ProcesoReporteDespachosSinSolicitar>()
            //                        .WithIdentity("job3", "group3")
            //                        .Build();
            //ICronTrigger trigger3   = (ICronTrigger)TriggerBuilder.Create()
            //                        .WithIdentity("trigger3", "group3")
            //                        .WithCronSchedule(tiempoEjecucionReporteSinSolicitar)
            //                        .Build();
            //sched.ScheduleJob(job3, trigger3);
            #endregion

            #region REPORTE ALERTA SOLICITUDES SIN REALIZAR

            string tiempoEjecucionReporteSolicitudesSinRealizar = ConfigurationManager.AppSettings["tiempoEjecucionReporteSinRealizar"].ToString();
            IJobDetail job4         = JobBuilder.Create<ProcesoAlertaSolicitudesSinRealizar>()
                                    .WithIdentity("job4", "group4")
                                    .Build();
            ICronTrigger trigger4   = (ICronTrigger)TriggerBuilder.Create()
                                    .WithIdentity("trigger4", "group4")
                                    .WithCronSchedule(tiempoEjecucionReporteSolicitudesSinRealizar)
                                    .Build();
            sched.ScheduleJob(job4, trigger4);
            #endregion

            sched.Start();
        }

        protected override void OnStop()
        {
            sched.Shutdown(true);
        }
    }
}
