﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Quartz;
using System.IO;
using System.Configuration;
using HQB.HQBDOC.HQBDOCWService.Utiles;
using HQB.HQBDOC.HQBDOCWService;
using NLog;
using System.Collections;
using System.Data;
using System.Threading;
using HQB.HQBDOC.HQBDOCWService.BLL;

namespace HQB.HQBDOC.HQBDOCWService
{
    public class CronJob : IJob
    {
      
        Logger log = LogManager.GetCurrentClassLogger();
        public Semaforo semaforo = Semaforo.getInstance;

        public void Execute(IJobExecutionContext context)
        {
            if (semaforo.EstaProcesando == false)
            {
                semaforo.EstaProcesando = true;

                try
                {
                    ProcesoBLL procesoBLL = new ProcesoBLL();
                    EmailBLL managerEmail = new EmailBLL();

                    log.Debug("Inicio WS Reporte");
                    procesoBLL.GenerarReportes();
                    log.Debug("Fin WS Reporte");
                    
                }
                catch (System.IO.IOException e)
                {
                    Logger log = LogManager.GetLogger("Error");
                    log.Error(e.Message.ToString());
                    
                    EmailBLL managerEmail = new EmailBLL();
                    managerEmail.EnviarMailAdmin("ERROR: "+ e.Message.ToString());
                }
                finally
                {
                    if (semaforo.EstaProcesando == true)
                    {
                        Semaforo.setInstance = null;
                        if (semaforo.CantidadArchivosConError > 0)

                        Semaforo.setInstance = null;
                    }

                }
            }

        }
        public DateTime UltimoViernesMes() {
 
            DateTime fechaActual = DateTime.Now;
            int anyo = fechaActual.Year;
            int mes = fechaActual.Month;
            int contador = 0;
            int dias = DateTime.DaysInMonth(anyo, mes);
            List<DateTime> diasViernes = new List<DateTime>();
            for (int i = 1; i <= dias; i++ )
            {
                DateTime dateValue = new DateTime(anyo, mes, i);
                

                if ((int)dateValue.DayOfWeek == 5) {
                    diasViernes.Add(dateValue);
                    contador++;
                } 
            }

            return diasViernes[contador-1];
        
        }
    }
}
