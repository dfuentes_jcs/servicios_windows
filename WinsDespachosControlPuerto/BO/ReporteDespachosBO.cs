﻿using JCSControlPuertoWService.Utiles;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSControlPuertoWService.BO
{
    public class ReporteDespachosBO
    {
        public DateTime FechaCreacion { get; set; }
        public string Fecha_Apertura { get { return FechaCreacion.ToString("dd/MM/yyyy"); } }
        public int Despacho { get; set; }
        public string Contenedor { get; set; }
        public int Cantidad_Bultos_DIN { get; set; }
        public int Cantidad_Contenedores { get; set; }
        public string Tipo_Operacion { get; set; }
        public string Razon_Social { get; set; }
        public string Referencia { get; set; }
        public string Ejecutivo { get; set; }
        public string Pedidor { get; set; }
        public string Aduana { get; set; }
        public int AduanaId { get; set; }
        public string Nave { get; set; }
        public DateTime? FechaETA { get; set; }
        public string Fecha_ETA { get { return ((FechaETA.HasValue && FechaETA.Value != Constantes.FECHA_NULL) ? FechaETA.Value.ToString("dd/MM/yyyy") : string.Empty); } }
        public DateTime? FechaSolicitudInscripcion { get; set; }
        public string Fecha_Solicitud_Inscripcion_Directo { get { return ((FechaSolicitudInscripcion.HasValue && FechaSolicitudInscripcion.Value != Constantes.FECHA_NULL) ? FechaSolicitudInscripcion.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty); } }
        public DateTime? FechaNotificacionCarga { get; set; }
        public string Fecha_Notificacion_Carga { get { return ((FechaNotificacionCarga.HasValue && FechaNotificacionCarga.Value != Constantes.FECHA_NULL) ? FechaNotificacionCarga.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty); } }
        public DateTime? FechaInscripcionDirecta { get; set; }
        public string Fecha_Inscripcion_Directo { get { return ((FechaInscripcionDirecta.HasValue && FechaInscripcionDirecta.Value != Constantes.FECHA_NULL) ? FechaInscripcionDirecta.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty); } }
        public DateTime? FechaSolicitudCambioAlmacen { get; set; }
        public string Fecha_Solicitud_Cambio_Almacen { get { return ((FechaSolicitudCambioAlmacen.HasValue && FechaSolicitudCambioAlmacen.Value != Constantes.FECHA_NULL) ? FechaSolicitudCambioAlmacen.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty); } }
        public DateTime? FechaCambioAlmacen { get; set; }
        public string Fecha_Cambio_Almacen { get { return ((FechaCambioAlmacen.HasValue && FechaCambioAlmacen.Value != Constantes.FECHA_NULL) ? FechaCambioAlmacen.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty); } }
        public DateTime? FechaSolicitudCanje { get; set; }
        public string Fecha_Solicitud_Canje { get { return ((FechaSolicitudCanje.HasValue && FechaSolicitudCanje.Value != Constantes.FECHA_NULL) ? FechaSolicitudCanje.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty); } }
        public DateTime? FechaCanje { get; set; }
        public string Fecha_Canje { get { return ((FechaCanje.HasValue && FechaCanje.Value != Constantes.FECHA_NULL) ? FechaCanje.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty); } }
        public DateTime? FechaSolicitudGarantia { get; set; }
        public string Fecha_Solicitud_Garantia { get { return ((FechaSolicitudGarantia.HasValue && FechaSolicitudGarantia.Value != Constantes.FECHA_NULL) ? FechaSolicitudGarantia.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty); } }
        public DateTime? FechaGarantia { get; set; }
        public string Fecha_Garantia { get { return ((FechaGarantia.HasValue && FechaGarantia.Value != Constantes.FECHA_NULL) ? FechaGarantia.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty); } }
        public DateTime? FechaPapeletaSolicitada { get; set; }
        public string Fecha_Papeleta_Solicitada { get { return ((FechaPapeletaSolicitada.HasValue && FechaPapeletaSolicitada.Value != Constantes.FECHA_NULL) ? FechaPapeletaSolicitada.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty); } }
        public DateTime? FechaPapeletaEnviada { get; set; }
        public string Fecha_Papeleta_Enviada { get { return ((FechaPapeletaEnviada.HasValue && FechaPapeletaEnviada.Value != Constantes.FECHA_NULL) ? FechaPapeletaEnviada.Value.ToString("dd/MM/yyyy HH:mm:ss") : string.Empty); } }
        public string SAG { get; set; }
        public string CDA { get; set; }
        public string Tipo_Carga { get; set; }
        public string Transporte { get; set; }
        public DateTime? FechaAceptacion { get; set; }
        public string Fecha_Aceptacion { get { return ((FechaAceptacion.HasValue && FechaAceptacion.Value != Constantes.FECHA_NULL) ? FechaAceptacion.Value.ToString("dd/MM/yyyy") : string.Empty); } }
        public DateTime? FechaPago { get; set; }
        public string Fecha_Pago { get { return ((FechaPago.HasValue && FechaPago.Value != Constantes.FECHA_NULL) ? FechaPago.Value.ToString("dd/MM/yyyy") : string.Empty); } }
        public string Almacen { get; set; } 
        //public string Cambio_Almacen { get; set; }
        public string Destino { get; set; }
        public string Nombre_Estado { get; set; }
        //public int EstadoId { get; set; }
        public DateTime? FechaRetiroParcial { get; set; }
        public string Fecha_Retiro_Parcial { get { return ((FechaRetiroParcial.HasValue && FechaRetiroParcial.Value != Constantes.FECHA_NULL) ? FechaRetiroParcial.Value.ToString("dd/MM/yyyy HH:mm") : string.Empty); } }
    }
}
