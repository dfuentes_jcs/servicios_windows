﻿using JCSControlPuertoWService.Utiles;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSControlPuertoWService.BO
{
    public class ReporteContenedorBO
    {
        public string Contenedor { get; set; }
        public string Tipo_Bulto { get; set; }
        public string Razon_Social { get; set; }
        public string Operador { get; set; }
        public string Codigo_Agente { get; set; }
        public string Aduana { get; set; }
        public string Nave { get; set; }
        public DateTime? Fecha_ETA { get; set; }
        //public string Fecha_ETA { get { return ((FechaETA.HasValue && FechaETA.Value != Constantes.FECHA_NULL) ? FechaETA.Value.ToString("dd/MM/yyyy") : string.Empty); } }
        public string Tipo_Carga { get; set; }
        public int Despacho { get; set; }
        public string Almacen { get; set; } 
        public string SAG { get; set; }
        public bool? EsImo { get; set; }
        public string IMO { get { return (EsImo.HasValue && EsImo.Value) ? "SI" : "NO"; } }
        public string Transporte { get; set; }
        public string Deposito { get; set; }
        public DateTime? Fecha_Retiro_Programada { get; set; }
        //public string Fecha_Retiro_Programada { get { return ((FechaRetiroProgramada.HasValue && FechaRetiroProgramada.Value != Constantes.FECHA_NULL) ? FechaRetiroProgramada.Value.ToString("dd/MM/yyyy HH:mm") : string.Empty); } }
        public string Observacion { get; set; }
        public DateTime? Fecha_Solicitud_Inscripcion_Directa { get; set; }
        //public string Fecha_Solicitud_Inscripcion_Directa { get { return ((FechaSolicitudInscripcion.HasValue && FechaSolicitudInscripcion.Value != Constantes.FECHA_NULL) ? FechaSolicitudInscripcion.Value.ToString("dd/MM/yyyy HH:mm") : string.Empty); } }
        public DateTime? Fecha_Notificacion_Carga { get; set; }
        //public string Fecha_Notificacion_Carga { get { return ((FechaNotificacionCarga.HasValue && FechaNotificacionCarga.Value != Constantes.FECHA_NULL) ? FechaNotificacionCarga.Value.ToString("dd/MM/yyyy HH:mm") : string.Empty); } }
        public DateTime? Fecha_Rastreo_Parcial { get; set; }
        //public string Fecha_Rastreo_Parcial { get { return ((FechaRastreoParcial.HasValue && FechaRastreoParcial.Value != Constantes.FECHA_NULL) ? FechaRastreoParcial.Value.ToString("dd/MM/yyyy HH:mm") : string.Empty); } }
    }
}
