﻿using JCSControlPuertoWService.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSControlPuertoWService.BLL
{
    class VariableConfiguracionBLL
    {
        public string ObtenerVariablePorNombre(string nombre)
        {
            VARIABLE_CONFIGURACION variable = new VariableConfiguracionDAL().ObtenerVariablePorNombre(nombre);
            return (variable != null)? variable.valor : string.Empty;
        }
    }
}
