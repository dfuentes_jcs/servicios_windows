﻿using JCSControlPuertoWService.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSControlPuertoWService.BLL
{
    public class UsuarioBLL
    {
        public string ObtenerEmailUsuario(int usuarioId)
        {
            return new UsuarioDAL().ObtenerEmailUsuario(usuarioId);
        }
    }
}
