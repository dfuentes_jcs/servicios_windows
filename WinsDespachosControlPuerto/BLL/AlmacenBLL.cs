﻿using JCSControlPuertoWService.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSControlPuertoWService.BLL
{
    public class AlmacenBLL
    {
        public List<CONTROL_PUERTO_ALMACEN> ObtenerListaAlmacenes()
        {
            return new AlmacenDAL().ObtenerListaAlmacenes();
        }
    }
}
