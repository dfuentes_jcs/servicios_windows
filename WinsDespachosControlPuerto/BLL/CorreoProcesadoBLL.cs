﻿using JCSControlPuertoWService.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSControlPuertoWService.BLL
{
    public class CorreoProcesadoBLL
    {
        public List<string> ListaCorreoProcesado(DateTime fechaProceso)
        {
            List<string> lista = new CorreoProcesadoDAL().ListaCorreoProcesado(fechaProceso);
            return lista;
        }

        public bool CrearCorreoProcesado(CONTROL_PUERTO_CORREO_PROCESADO correoProcesado) {

            return new CorreoProcesadoDAL().CrearCorreoProcesado(correoProcesado);
        }
    }
}
