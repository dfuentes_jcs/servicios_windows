﻿using JCSControlPuertoWService.BO;
using JCSControlPuertoWService.DAL;
using JCSControlPuertoWService.Utiles;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JCSControlPuertoWService.BLL
{
    public class ControlPuertoBLL
    {
        public CONTROL_EJECUTIVO ObtenerControlEjecutivoPorDespacho(int despachoId)
        {
            return new ControlPuertoDAL().ObtenerControlEjecutivoPorDespacho(despachoId);
        }

        public List<string> ObtenerListaNaves()
        {
            return new ControlPuertoDAL().ObtenerListaNaves();
        }

        public bool ControlPuertoBitacoraCrear(CONTROL_EJECUTIVO controlEjecutivo, int almacenId, int almacenNuevoId)
        {
            CONTROL_PUERTO_BITACORA bitacora            = new CONTROL_PUERTO_BITACORA();
            bitacora.control_puerto_estado_id           = controlEjecutivo.CONTROL_PUERTO_ESTADO_ID.Value;
            bitacora.id_despacho                        = controlEjecutivo.ID_DESPACHO;
            bitacora.esta_activo                        = true;
            bitacora.fecha_creacion                     = DateTime.Now;
            bitacora.usuario_creacion                   = Constantes.USUARIO_TAREA_PROGRAMADA_ID;
            bitacora.control_puerto_almacen_id_antiguo  = (almacenId > 0)? almacenId : (int?)null;
            bitacora.control_puerto_almacen_id_nuevo    = (almacenNuevoId > 0)? almacenNuevoId : (int?)null;
            return new ControlPuertoBitacoraDAL().ControlPuertoBitacoraCrear(bitacora);
        }

        public bool EditarDespachoControlEjecutivo(int despachoId, int estadoId, string almacen)
        {
            return new ControlPuertoDAL().EditarDespachoControlEjecutivo(despachoId, estadoId, almacen);
        }

        public List<int> ObtenerListaDespachosExistentes()
        {
            return new ControlPuertoDAL().ObtenerListaDespachosExistentes();
        }

        public bool InsertarAdjuntosControlPuerto(List<CONTROL_PUERTO_CORREO_PROCESADO_ADJUNTO> listaAdjuntos)
        {
            return new ControlPuertoDAL().InsertarAdjuntosControlPuerto(listaAdjuntos);
        }

        public bool InsertarDocumentosControlPuerto(List<CONTROL_PUERTO_DOCUMENTO> listaDocumentos)
        {
            return new ControlPuertoDAL().InsertarDocumentosControlPuerto(listaDocumentos);
        }

        public bool InsertarContenedoresControlPuerto(List<CONTROL_PUERTO_CONTENEDOR> listaContenedores)
        {
            return new ControlPuertoDAL().InsertarContenedoresControlPuerto(listaContenedores);
        }

        public bool InsertarDespachoCorreoProcesadoControlPuerto(CONTROL_PUERTO_CORREO_PROCESADO_DESPACHO despachoCorreoProcesado)
        {
            return new ControlPuertoDAL().InsertarDespachoCorreoProcesadoControlPuerto(despachoCorreoProcesado);
        }

        public List<CONTROL_PUERTO_ESTADO> ObtenerEstadosControlPuerto()
        {
            return new ControlPuertoDAL().ObtenerEstadosControlPuerto();
        }

        public DataSet ObtenerDetalleControlPuertoDespacho(int despachoId)
        {
            return new ControlPuertoDAL().ObtenerDetalleDespachoSP(despachoId);
        }

        public List<string> ObtenerCorreosControlPuertoPorAduana(int aduanaId)
        {
            DataTable dtCorreos = new ControlPuertoDAL().ObtenerCorreosPorAduana(aduanaId);
            List<string> listaCorreos = new List<string>();
            string email = string.Empty;

            if (dtCorreos != null && dtCorreos.Rows.Count > 0)
            {
                foreach(DataRow row in dtCorreos.Rows)
                {
                    email = row["email"].ToString();
                    if (!listaCorreos.Contains(email))
                        listaCorreos.Add(email);
                }
            }

            return listaCorreos;
        }

        public List<ReporteDespachosBO> ObtenerListaReporteDepachos(DateTime fechaDesde, DateTime fechaHasta)
        {
            ControlPuertoDAL controlPuertoDAL = new ControlPuertoDAL();
            List<ReporteDespachosBO> listaDespachos = controlPuertoDAL.ObtenerListaReporteDepachos(
                new List<int> 
                {
                    (int)Enumerables.ControlPuertoEstado.APERTURADO_COMPLETO, 
                    (int)Enumerables.ControlPuertoEstado.APERTURADO_INCOMPLETO, 
                    (int)Enumerables.ControlPuertoEstado.ENVIADO_PARA_CONFIRMAR,
                    (int)Enumerables.ControlPuertoEstado.CONFIRMADO, 
                    (int)Enumerables.ControlPuertoEstado.INSCRIPCION_SOLICITADA,
                    (int)Enumerables.ControlPuertoEstado.INSCRITO,
                    (int)Enumerables.ControlPuertoEstado.CAMBIO_ALMACEN_SOLICITADO,
                    (int)Enumerables.ControlPuertoEstado.CAMBIO_ALMACEN_REALIZADO,
                    (int)Enumerables.ControlPuertoEstado.GARANTIA_SOLICITADA,
                    (int)Enumerables.ControlPuertoEstado.GARANTIZADO,
                    (int)Enumerables.ControlPuertoEstado.CANJE_SOLICITADO,
                    (int)Enumerables.ControlPuertoEstado.CANJEADO,
                    (int)Enumerables.ControlPuertoEstado.DEVUELTO_EJECUTIVO,
                    (int)Enumerables.ControlPuertoEstado.RETIRADO_PARCIAL,
                    (int)Enumerables.ControlPuertoEstado.NOTIFICACION_CARGA_INFORMADA,
                    (int)Enumerables.ControlPuertoEstado.ENVIO_PAPELETA_SOLICTADA,
                    (int)Enumerables.ControlPuertoEstado.PAPELETA_ENVIADA,
                    (int)Enumerables.ControlPuertoEstado.NOTIFICACION_RECIBIDA
                },
                Constantes.TIPO_OPERACION_IMPO,
                fechaDesde,
                fechaHasta
            );

            Parallel.ForEach(listaDespachos, despacho =>
            {
                DataSet dsDetalle = controlPuertoDAL.ObtenerDetalleDespachoSP(despacho.Despacho);
                if (dsDetalle != null && dsDetalle.Tables.Count > 0 && dsDetalle.Tables[0].Rows.Count > 0)
                {
                    despacho.Pedidor = dsDetalle.Tables[0].Rows[0]["PEDIDOR"].ToString();
                }
            });

            return listaDespachos;
        }

        public List<ReporteContenedorBO> ObtenerListaReporteContenedor(DateTime fechaDesde, DateTime fechaHasta)
        {
            ControlPuertoDAL controlPuertoDAL = new ControlPuertoDAL();
            List<ReporteContenedorBO> listaContenedores = controlPuertoDAL.ObtenerListaReporteContenedor(
                new List<int> 
                {
                    (int)Enumerables.ControlPuertoEstado.APERTURADO_COMPLETO, 
                    (int)Enumerables.ControlPuertoEstado.APERTURADO_INCOMPLETO, 
                    (int)Enumerables.ControlPuertoEstado.ENVIADO_PARA_CONFIRMAR,
                    (int)Enumerables.ControlPuertoEstado.CONFIRMADO, 
                    (int)Enumerables.ControlPuertoEstado.INSCRIPCION_SOLICITADA,
                    (int)Enumerables.ControlPuertoEstado.INSCRITO,
                    (int)Enumerables.ControlPuertoEstado.CAMBIO_ALMACEN_SOLICITADO,
                    (int)Enumerables.ControlPuertoEstado.CAMBIO_ALMACEN_REALIZADO,
                    (int)Enumerables.ControlPuertoEstado.GARANTIA_SOLICITADA,
                    (int)Enumerables.ControlPuertoEstado.GARANTIZADO,
                    (int)Enumerables.ControlPuertoEstado.CANJE_SOLICITADO,
                    (int)Enumerables.ControlPuertoEstado.CANJEADO,
                    (int)Enumerables.ControlPuertoEstado.DEVUELTO_EJECUTIVO,
                    (int)Enumerables.ControlPuertoEstado.RETIRADO_PARCIAL,
                    (int)Enumerables.ControlPuertoEstado.NOTIFICACION_CARGA_INFORMADA,
                    (int)Enumerables.ControlPuertoEstado.ENVIO_PAPELETA_SOLICTADA,
                    (int)Enumerables.ControlPuertoEstado.PAPELETA_ENVIADA,
                    (int)Enumerables.ControlPuertoEstado.NOTIFICACION_RECIBIDA
                },
                Constantes.TIPO_OPERACION_IMPO,
                fechaDesde,
                fechaHasta
            );

            return listaContenedores;
        }

        public DataTable ObtenerReporteContenedor(DateTime fechaDesde, DateTime fechaHasta)
        {
            ControlPuertoDAL controlPuertoDAL = new ControlPuertoDAL();
            return controlPuertoDAL.ObtenerReporteContenedorSP(fechaDesde, fechaHasta);
        }

        public List<ReporteDespachosBO> ObtenerListaReporteDespachosSinSolicitar(List<string> listaAduanasExcluidasId)
        {
            ControlPuertoDAL controlPuertoDAL = new ControlPuertoDAL();
            List<ReporteDespachosBO> listaDespachos = controlPuertoDAL.ObtenerListaReporteDepachosSinSolicitar(
                new List<int> 
                {
                    (int)Enumerables.ControlPuertoEstado.APERTURADO_COMPLETO, 
                    (int)Enumerables.ControlPuertoEstado.APERTURADO_INCOMPLETO, 
                    (int)Enumerables.ControlPuertoEstado.ENVIADO_PARA_CONFIRMAR,
                    (int)Enumerables.ControlPuertoEstado.CONFIRMADO, 
                    (int)Enumerables.ControlPuertoEstado.INSCRIPCION_SOLICITADA,
                    (int)Enumerables.ControlPuertoEstado.INSCRITO,
                    (int)Enumerables.ControlPuertoEstado.CAMBIO_ALMACEN_SOLICITADO,
                    (int)Enumerables.ControlPuertoEstado.CAMBIO_ALMACEN_REALIZADO,
                    (int)Enumerables.ControlPuertoEstado.GARANTIA_SOLICITADA,
                    (int)Enumerables.ControlPuertoEstado.GARANTIZADO,
                    (int)Enumerables.ControlPuertoEstado.CANJE_SOLICITADO,
                    (int)Enumerables.ControlPuertoEstado.CANJEADO,
                    (int)Enumerables.ControlPuertoEstado.DEVUELTO_EJECUTIVO,
                    (int)Enumerables.ControlPuertoEstado.RETIRADO_PARCIAL,
                    (int)Enumerables.ControlPuertoEstado.NOTIFICACION_CARGA_INFORMADA,
                    (int)Enumerables.ControlPuertoEstado.ENVIO_PAPELETA_SOLICTADA,
                    (int)Enumerables.ControlPuertoEstado.PAPELETA_ENVIADA,
                    (int)Enumerables.ControlPuertoEstado.NOTIFICACION_RECIBIDA
                },
                Constantes.TIPO_OPERACION_IMPO,
                listaAduanasExcluidasId
            );

            //DataSet dsDetalle = new DataSet();
            //foreach (ReporteDespachosBO despacho in listaDespachos)
            //{
            //    dsDetalle = controlPuertoDAL.ObtenerDetalleDespachoSP(despacho.Despacho);
            //    if (dsDetalle != null && dsDetalle.Tables.Count > 0 && dsDetalle.Tables[0].Rows.Count > 0)
            //    {
            //        despacho.Pedidor = dsDetalle.Tables[0].Rows[0]["PEDIDOR"].ToString();
            //    }
            //}

            Parallel.ForEach(listaDespachos, despacho =>
            {
                DataSet dsDetalle = controlPuertoDAL.ObtenerDetalleDespachoSP(despacho.Despacho);
                if (dsDetalle != null && dsDetalle.Tables.Count > 0 && dsDetalle.Tables[0].Rows.Count > 0)
                {
                    despacho.Pedidor = dsDetalle.Tables[0].Rows[0]["PEDIDOR"].ToString();
                }
            });

            return listaDespachos;
        }

        public List<ReporteDespachosBO> ObtenerListaReporteSolicitudesSinRealizar()
        {
            ControlPuertoDAL controlPuertoDAL = new ControlPuertoDAL();
            List<ReporteDespachosBO> listaDespachos = controlPuertoDAL.ObtenerListaReporteSolicitudesSinRealizar(
                new List<int> 
                {
                    (int)Enumerables.ControlPuertoEstado.APERTURADO_COMPLETO, 
                    (int)Enumerables.ControlPuertoEstado.APERTURADO_INCOMPLETO, 
                    (int)Enumerables.ControlPuertoEstado.ENVIADO_PARA_CONFIRMAR,
                    (int)Enumerables.ControlPuertoEstado.CONFIRMADO, 
                    (int)Enumerables.ControlPuertoEstado.INSCRIPCION_SOLICITADA,
                    (int)Enumerables.ControlPuertoEstado.INSCRITO,
                    (int)Enumerables.ControlPuertoEstado.CAMBIO_ALMACEN_SOLICITADO,
                    (int)Enumerables.ControlPuertoEstado.CAMBIO_ALMACEN_REALIZADO,
                    (int)Enumerables.ControlPuertoEstado.GARANTIA_SOLICITADA,
                    (int)Enumerables.ControlPuertoEstado.GARANTIZADO,
                    (int)Enumerables.ControlPuertoEstado.CANJE_SOLICITADO,
                    (int)Enumerables.ControlPuertoEstado.CANJEADO,
                    (int)Enumerables.ControlPuertoEstado.DEVUELTO_EJECUTIVO,
                    (int)Enumerables.ControlPuertoEstado.RETIRADO_PARCIAL,
                    (int)Enumerables.ControlPuertoEstado.NOTIFICACION_CARGA_INFORMADA,
                    (int)Enumerables.ControlPuertoEstado.ENVIO_PAPELETA_SOLICTADA,
                    (int)Enumerables.ControlPuertoEstado.PAPELETA_ENVIADA,
                    (int)Enumerables.ControlPuertoEstado.NOTIFICACION_RECIBIDA
                },
                Constantes.TIPO_OPERACION_IMPO
            );

            Parallel.ForEach(listaDespachos, despacho =>
            {
                DataSet dsDetalle = controlPuertoDAL.ObtenerDetalleDespachoSP(despacho.Despacho);
                if (dsDetalle != null && dsDetalle.Tables.Count > 0 && dsDetalle.Tables[0].Rows.Count > 0)
                {
                    despacho.Pedidor = dsDetalle.Tables[0].Rows[0]["PEDIDOR"].ToString();
                }
            });

            return listaDespachos;
        }

        public List<CONTROL_PUERTO_CORREO_TIPO> ObtenerListaTipoCorreo()
        {
            return new ControlPuertoDAL().ObtenerListaTipoCorreo();
        }

        public List<CONTROL_PUERTO_USUARIO_CORREO_TIPO> ObtenerUsuariosOperativos()
        {
            return new ControlPuertoDAL().ObtenerUsuariosOperativos();
        }

        public List<ADUANA> ObtenerAduanas()
        {
            return new ControlPuertoDAL().ObtenerAduanas();
        }

        public List<USUARIO> ObtenerListaUsuarios()
        {
            return new ControlPuertoDAL().ObtenerListaUsuarios();
        }

        public List<string> ObtenerDestinatariosPorReporteYAduana(int aduanaId, int ReporteId)
        {
            List<CONTROL_PUERTO_REPORTE_DESTINATARIO> listaDestinatarios    = new ControlPuertoDAL().ObtenerDestinatariosPorReporteYAduana(aduanaId, ReporteId);
            List<string> listaCorreos                                       = new List<string>();

            foreach(CONTROL_PUERTO_REPORTE_DESTINATARIO destinatario in listaDestinatarios)
            {
                listaCorreos.Add((destinatario.USUARIO != null)? destinatario.USUARIO.email : destinatario.email);
            }

            return listaCorreos.Distinct().ToList();
        }
    }
}
